package com.themedzone.medzoneapp

import androidx.annotation.NonNull;
import com.example.flutterapp.PlatformBridge
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugins.GeneratedPluginRegistrant

class MainActivity: FlutterActivity() {
    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        GeneratedPluginRegistrant.registerWith(flutterEngine);
        PlatformBridge(flutterEngine.dartExecutor.binaryMessenger, this).create()

    }
}
