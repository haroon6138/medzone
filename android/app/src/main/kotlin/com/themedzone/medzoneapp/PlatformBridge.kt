package com.example.flutterapp
import android.content.Context
import android.content.Intent
import android.content.Intent.getIntent
import android.net.Uri
import android.net.wifi.WifiManager
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.widget.Toast
import androidx.core.app.ActivityCompat.startActivityForResult
import com.cloud.diplomaticquarterapp.utils.Utils
import com.themedzone.medzoneapp.MainActivity
import io.flutter.plugin.common.BinaryMessenger
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel


class PlatformBridge(binaryMessenger: BinaryMessenger, flutterMainActivity: MainActivity) {
    private var binaryMessenger = binaryMessenger
    private var mainActivity = flutterMainActivity

    private lateinit var channel: MethodChannel

    companion object {
        private const val CHANNEL = "Platform-Bridge"
        private const val IS_DRAW_OVER_APPS_PERMISSION_ALLOWED = "isDrawOverAppsPermissionAllowed"
        private const val ASK_DRAW_OVER_APPS_PERMISSION = "askDrawOverAppsPermission"
        private const val GET_INTENT = "getIntent"
    }

    fun create(){
        channel = MethodChannel(binaryMessenger, CHANNEL)
        Utils.setPlatformChannel(channel)
        channel.setMethodCallHandler { methodCall: MethodCall, result: MethodChannel.Result ->

           if (methodCall.method == IS_DRAW_OVER_APPS_PERMISSION_ALLOWED) {
                isDrawOverAppsPermissionAllowed(methodCall,result)
            }else if (methodCall.method == ASK_DRAW_OVER_APPS_PERMISSION) {
                askDrawOverAppsPermission(methodCall,result)
            }else if (methodCall.method == GET_INTENT) {
                getIntentData(methodCall,result)
            }else{
                result.notImplemented()
            }

        }

        val res = channel.invokeMethod("localizedValue","errorConnectingHmgNetwork")
        print(res)
    }

    private fun isDrawOverAppsPermissionAllowed(methodCall: MethodCall, result: MethodChannel.Result) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (
                    Settings.canDrawOverlays(mainActivity)
            ) {
                result.success(true)
            } else {
                result.success(false)
            }
        } else {
            result.success(false)
        }
    }

    private fun askDrawOverAppsPermission(methodCall: MethodCall, result: MethodChannel.Result) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val intent = Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION)
            val uri = Uri.parse("package:" + mainActivity.getPackageName())
            intent.setData(uri)
            startActivityForResult(mainActivity, intent,102,null)
            result.success(true)
        } else {
            result.success(false)
        }
    }

    private fun getIntentData(methodCall: MethodCall, result: MethodChannel.Result) {

        val bundle :Bundle ?= getIntent("").extras
        if (bundle!=null){
            val message = bundle.getString("notification") // 1
            System.out.println("BundleExtra:"+message)
            Toast.makeText(this.mainActivity, message+"", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this.mainActivity,"Bundle Null",Toast.LENGTH_SHORT).show();
        }
        result.success(true);
    }

}