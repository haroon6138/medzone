package com.themedzone.medzoneapp;
import io.flutter.plugins.firebasemessaging.FirebaseMessagingPlugin;
import io.flutter.app.FlutterApplication;
import io.flutter.plugin.common.PluginRegistry;
import io.flutter.plugin.common.PluginRegistry.PluginRegistrantCallback;

import static io.flutter.plugins.firebasemessaging.FlutterFirebaseMessagingService.setPluginRegistrant;

public class Application extends FlutterApplication implements PluginRegistrantCallback {
    @Override
    public void onCreate() {
        super.onCreate();
        setPluginRegistrant(this);
    }

    @Override
    public void registerWith(PluginRegistry registry) {
        FirebaseMessagingPlugin.registerWith(registry.registrarFor("io.flutter.plugins.firebasemessaging.FirebaseMessagingPlugin"));
        //GeneratedPluginRegistrant.registerWith(registry);
    }
}