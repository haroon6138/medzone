package com.cloud.diplomaticquarterapp.utils

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.os.Build
import android.widget.Toast
import androidx.annotation.Nullable
import androidx.core.app.NotificationCompat
import androidx.core.app.TaskStackBuilder
//import com.example.flutterapp.MainActivity
import com.themedzone.medzoneapp.MainActivity
import creativecreatorormaybenot.wakelock.BuildConfig
import io.flutter.plugin.common.MethodChannel
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.concurrent.timerTask


class Utils {

    companion object{
        private lateinit var platformChannel: MethodChannel
        fun getPlatformChannel():MethodChannel{
            return platformChannel
        }
        fun setPlatformChannel(channel: MethodChannel){
            platformChannel = channel
        }

        fun timer(delay: Long, repeat: Boolean, tick: (Timer) -> Unit) : Timer{
            val timer = Timer()
            if(repeat)
                timer.schedule(timerTask {
                    tick(timer)
                }, delay, delay)
            else
                timer.schedule(timerTask {
                    tick(timer)
                }, delay)

            return timer
        }

        fun popMessage(context: MainActivity, message: String){
            context.runOnUiThread {
                Toast.makeText(context, message, Toast.LENGTH_LONG).show()
            }
        }
    }

}


private const val NOTIFICATION_CHANNEL_ID = BuildConfig.APPLICATION_ID + ".channel"

//-------------------------
// Open Helper Methods
//-------------------------
fun getUniqueId() = ((System.currentTimeMillis() % 10000).toInt())

object DateUtils {
    @JvmStatic
    fun dateTimeNow() : String {
        val format = SimpleDateFormat("dd-MMM-yyy hh:mm:ss")
        return format.format(Date())
    }
}

fun isJSONValid(jsonString: String?): Boolean {
    try { JSONObject(jsonString) } catch (ex: JSONException) {
        try {  JSONArray(jsonString) } catch (ex1: JSONException) {
            return false
        }
    }
    return true
}


class HTTPResponse<T>(data: T){
    final var data:T = data
}
