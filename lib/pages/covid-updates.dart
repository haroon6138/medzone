import 'package:MedZone/models/response/CountyCovidDataResponse.dart';
import 'package:MedZone/models/response/StateCovidDataResponse.dart';
import 'package:MedZone/services/CommonServices.dart';
import 'package:MedZone/services/Covid-Services.dart';
import 'package:MedZone/utils/date_uitl.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:geolocator/geolocator.dart';

class CovidUpdates extends StatefulWidget {
  Placemark placeObject;

  CountyCovidDataResponse countyCovidDataResponse;
  StateCovidDataResponse stateCovidDataResponse;
  APIService service;

  String countyConfirmed = "0";
  String countyDeaths = "0";

  String stateConfirmed = "0";
  String stateDeaths = "0";

  String stateDailyConfirmed = "0";
  String stateDailyDeaths = "0";

  DateTime updatedAt;

  String lastUpdateDateTime = "As of Sat, Jul 11, 2020";

  CovidUpdates({@required this.placeObject});

  @override
  _CovidUpdatesState createState() => _CovidUpdatesState();
}

class _CovidUpdatesState extends State<CovidUpdates> {
  @override
  void initState() {
    widget.countyCovidDataResponse = new CountyCovidDataResponse();
    widget.stateCovidDataResponse = new StateCovidDataResponse();
    widget.service = new APIService();
    var countyName = "";

    if (widget.placeObject.subAdministrativeArea
        .toLowerCase()
        .contains("county")) {
      countyName = widget.placeObject.subAdministrativeArea
          .toLowerCase()
          .replaceFirst(" county", "");
    }

    getCovidStatsByCounty(context, countyName);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        title: Text("Covid-19",
            style: TextStyle(
              fontSize: 16.0,
              fontWeight: FontWeight.bold,
              letterSpacing: 0.5,
              fontFamily: 'Open-Sans',
            )),
        backgroundColor: new Color(0xff17AF6B).withOpacity(1.0),
      ),
      body: Container(
        padding: EdgeInsets.fromLTRB(20, 10, 16, 16),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              new Color(0xff17AF6B).withOpacity(1.0),
              new Color(0xffF1F2F2).withOpacity(1.0),
            ],
            stops: [0.2, 0.5],
          ),
        ),
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Card(
                shape: RoundedRectangleBorder(
                    side: BorderSide(color: Colors.white70, width: 1),
                    borderRadius: BorderRadius.circular(10)),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(left: 20.0, top: 10.0),
                      child: Text(
                          widget.placeObject.locality +
                              ", " +
                              widget.placeObject.administrativeArea,
                          style: TextStyle(
                            fontSize: 14.0,
                            fontWeight: FontWeight.bold,
                            fontFamily: "Open-Sans-Bold",
                          )),
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 20.0, bottom: 3.0),
                      child: Text(widget.placeObject.subAdministrativeArea,
                          style: TextStyle(
                            fontSize: 18.0,
                            fontWeight: FontWeight.bold,
                            fontFamily: "Open-Sans-Bold",
                          )),
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 20.0, bottom: 5.0),
                      child: Text(widget.lastUpdateDateTime,
                          style: TextStyle(
                              fontSize: 12.0,
                              letterSpacing: 0.5,
                              color: Colors.grey[600])),
                    ),
                    Container(
                      padding:
                          EdgeInsets.only(left: 20.0, bottom: 5.0, top: 10.0),
                      child: Table(
                        children: [
                          TableRow(children: [
                            TableCell(
                                child: Text("Confirmed Cases",
                                    style: TextStyle(
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.grey[800],
                                        fontFamily: 'Open-Sans-Bold'))),
                            TableCell(
                                child: Text("Deaths",
                                    style: TextStyle(
                                        fontSize: 16.0,
                                        color: Colors.grey[800],
                                        fontWeight: FontWeight.bold,
                                        fontFamily: 'Open-Sans-Bold'))),
                          ]),
                          TableRow(children: [
                            TableCell(
                                child: Container(
                                    child: Text(widget.countyConfirmed,
                                        style: TextStyle(
                                            fontSize: 32.0,
                                            color: Colors.red[600],
                                            fontWeight: FontWeight.bold,
                                            fontFamily: 'Open-Sans-Bold')))),
                            TableCell(
                                child: Text(widget.countyDeaths,
                                    style: TextStyle(
                                        fontSize: 32.0,
//                            color: Colors.red[600],
                                        fontWeight: FontWeight.bold,
                                        fontFamily: 'Open-Sans-Bold'))),
                          ]),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                      child: Divider(
                        color: Colors.grey[800],
                      ),
                    ),
                    Container(
                      padding:
                          EdgeInsets.only(left: 20.0, top: 10.0, bottom: 15.0),
                      child: Text(widget.placeObject.administrativeArea,
                          style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                            fontFamily: "Open-Sans-Bold",
                          )),
                    ),
                    Container(
                      padding:
                          EdgeInsets.only(left: 20.0, bottom: 30.0, top: 0.0),
                      child: Table(
                        children: [
                          TableRow(children: [
                            TableCell(
                                child: Text("Confirmed Cases",
                                    style: TextStyle(
                                        fontSize: 14.0,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.grey[800],
                                        fontFamily: 'Open-Sans-Bold'))),
                            TableCell(
                                child: Text("Deaths",
                                    style: TextStyle(
                                        fontSize: 14.0,
                                        color: Colors.grey[800],
                                        fontWeight: FontWeight.bold,
                                        fontFamily: 'Open-Sans-Bold'))),
                          ]),
                          TableRow(children: [
                            TableCell(
                                child: Container(
                                    child: Text(widget.stateConfirmed,
                                        style: TextStyle(
                                            fontSize: 28.0,
                                            color: Colors.red[600],
                                            fontWeight: FontWeight.bold,
                                            fontFamily: 'Open-Sans-Bold')))),
                            TableCell(
                                child: Text(widget.stateDeaths,
                                    style: TextStyle(
                                        fontSize: 28.0,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: 'Open-Sans-Bold'))),
                          ]),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                child: Card(
                  shape: RoundedRectangleBorder(
                      side: BorderSide(color: Colors.white70, width: 1),
                      borderRadius: BorderRadius.circular(10)),
                  child: Column(
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(
                              left: 20.0, top: 10.0, bottom: 10.0),
                          child: Text("Daily Trends",
                              style: TextStyle(
                                fontSize: 14.0,
                                fontWeight: FontWeight.bold,
                                fontFamily: "Open-Sans-Bold",
                              )),
                        ),
                        Container(
                          padding: EdgeInsets.only(
                              left: 20.0, bottom: 20.0, top: 10.0),
                          alignment: Alignment.center,
                          child: Table(
                            children: [
                              TableRow(children: [
                                TableCell(
                                    child: Text("Confirmed Cases",
                                        style: TextStyle(
                                            fontSize: 14.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.grey[800],
                                            fontFamily: 'Open-Sans-Bold'))),
                                TableCell(
                                    child: Text("Deaths",
                                        style: TextStyle(
                                            fontSize: 14.0,
                                            color: Colors.grey[800],
                                            fontWeight: FontWeight.bold,
                                            fontFamily: 'Open-Sans-Bold'))),
                              ]),
                              TableRow(children: [
                                TableCell(
                                    child: Container(
                                        child: Text(widget.stateDailyConfirmed,
                                            style: TextStyle(
                                                fontSize: 28.0,
                                                color: Colors.red[600],
                                                fontWeight: FontWeight.bold,
                                                fontFamily:
                                                    'Open-Sans-Bold')))),
                                TableCell(
                                    child: Text(widget.stateDailyDeaths,
                                        style: TextStyle(
                                            fontSize: 28.0,
                                            fontWeight: FontWeight.bold,
                                            fontFamily: 'Open-Sans-Bold'))),
                              ]),
                            ],
                          ),
                        ),
                      ]),
                ),
              ),
//              Column(
//                crossAxisAlignment: CrossAxisAlignment.start,
//                children: <Widget>[
//                  Container(
//                    margin: EdgeInsets.only(top: 20.0),
//                    child: LineGraph(
//                      features: widget.features,
//                      size: Size(350, 250),
//                      labelX: ['Day 1', 'Day 2', 'Day 3', 'Day 4', 'Day 5'],
//                      labelY: ['20%', '40%', '60%', '80%', '100%'],
//                      showDescription: false,
//                      graphColor: Colors.grey[600],
//                    ),
//                  ),
//                  SizedBox(
//                    height: 50,
//                  )
//                ],
//              ),
            ],
          ),
        ),
      ),
    );
  }

  getCovidStatsByState(context, String stateName) {
    EasyLoading.show(status: CommonService.getLoadingMessage(), maskType: EasyLoadingMaskType.black);
    widget.service.getCovidStatsByState(stateName, context).then((res) {
      setState(() {
        widget.stateCovidDataResponse = StateCovidDataResponse.fromJson(res);
        widget.stateDeaths = widget.stateCovidDataResponse.deaths.toString();
        widget.stateConfirmed = widget.stateCovidDataResponse.cases.toString();
        widget.stateDailyConfirmed =
            widget.stateCovidDataResponse.todayCases.toString();
        widget.stateDailyDeaths =
            widget.stateCovidDataResponse.todayDeaths.toString();
      });
      EasyLoading.dismiss();
    }).catchError((err) {
      print(err);
      EasyLoading.dismiss();
    });
  }

  getCovidStatsByCounty(context, String countyName) {
    String updatedAtDate;
    String updatedAtTime;
    dynamic updatedDateTime;

    EasyLoading.show(status: CommonService.getLoadingMessage(), maskType: EasyLoadingMaskType.black);

    widget.service.getCovidStatsByCounty(countyName, context).then((res) {
      setState(() {
        widget.countyCovidDataResponse = CountyCovidDataResponse.fromJson(res);
        widget.countyConfirmed =
            widget.countyCovidDataResponse.stats.confirmed.toString();
        widget.countyDeaths =
            widget.countyCovidDataResponse.stats.deaths.toString();

        updatedAtDate = widget.countyCovidDataResponse.updatedAt.split(" ")[0];
        updatedAtTime = widget.countyCovidDataResponse.updatedAt.split(" ")[1];
        updatedDateTime = DateTime(
            int.parse(updatedAtDate.split("-")[0]),
            int.parse(updatedAtDate.split("-")[1]),
            int.parse(updatedAtDate.split("-")[2]),
            int.parse(updatedAtTime.split(":")[0]),
            int.parse(updatedAtTime.split(":")[1]),
            int.parse(updatedAtTime.split(":")[2]));
        widget.lastUpdateDateTime = getFormattedDateTime(updatedDateTime);

        EasyLoading.dismiss();

        Future.delayed(new Duration(milliseconds: 1500)).then((value) {
          getCovidStatsByState(context, widget.placeObject.administrativeArea);
        });
      });
    }).catchError((err) {
      EasyLoading.dismiss();
      print(err);
    });
  }

  String getFormattedDateTime(DateTime dateTime) {
    return "As of " +
        DateUtil.getWeekDay(dateTime.weekday) +
        ", " +
        DateUtil.getMonth(dateTime.month) +
        " " +
        dateTime.day.toString() +
        ", " +
        dateTime.year.toString() +
        ", " +
        dateTime.hour.toString() +
        ":" +
        dateTime.minute.toString();
  }
}
