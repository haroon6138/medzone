import 'package:MedZone/custom_widget/medicine_card.dart';
import 'package:MedZone/models/response/SearchMedicineResponse.dart';
import 'package:MedZone/models/response/TopMedicinesResponse.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';

class MedicineSearchList extends StatefulWidget {
  SearchMedicineResponse searchMedicinesResponse;

  MedicineSearchList({@required this.searchMedicinesResponse});

  @override
  _MedicineSearchListState createState() => _MedicineSearchListState();
}

class _MedicineSearchListState extends State<MedicineSearchList> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: new Color(0xffF1F2F2).withOpacity(1.0),
      appBar: AppBar(
        title: Text("searchMedHeading",
            style: TextStyle(
              fontSize: 16.0,
              fontWeight: FontWeight.bold,
              letterSpacing: 0.5,
              fontFamily: 'Open-Sans',
            )).tr(),
        backgroundColor: new Color(0xff17AF6B).withOpacity(1.0),
      ),
      body: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            margin: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
            child: Text("searchResults",
                style: TextStyle(
                    fontSize: 18,
                    fontFamily: 'Open-Sans',
                    fontWeight: FontWeight.bold,
                    letterSpacing: 1.0,
                    color: Colors.black))
                .tr(),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
            height: MediaQuery.of(context).size.height * 0.78,
            child: ListView.builder(
              itemCount: widget.searchMedicinesResponse.data.length,
              itemBuilder: (context, index) {
                return getMedicineCard(
                    index, widget.searchMedicinesResponse.data[index]);
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget getMedicineCard(int index, Data medListData) {
    medData data = new medData();
    data.name = medListData.name;
    data.formulastrength = medListData.formulastrength;
    data.id = medListData.id;
    data.supplier = medListData.supplier;

    return MedicineCard(data, 'assets/images/link_Arrow.svg', 'yes');
  }
}
