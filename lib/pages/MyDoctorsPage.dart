import 'package:MedZone/models/AuthUserProvider.dart';
import 'package:MedZone/models/MyDoctorsProvider.dart';
import 'package:MedZone/pages/appointments/SearchByClinic.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:MedZone/pages/appointments/widgets/DoctorView.dart';
import 'package:MedZone/services/CustomTextStyles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:MedZone/utils/PlatformBridge.dart';

class MyDoctors extends StatefulWidget {
  final Function removeFunction;

  MyDoctors({@required this.removeFunction});

  @override
  _State createState() => _State();
}

class _State extends State<MyDoctors> {
  bool hasDoctors = false;

  AuthUserProvider authUserProvider;
  MyDoctorsProvider myDoctorsProvider;

  @override
  Widget build(BuildContext context) {
    authUserProvider = Provider.of<AuthUserProvider>(context);
    myDoctorsProvider = Provider.of<MyDoctorsProvider>(context);
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("My Doctors", style: CustomTextStyle.display5(context)),
        backgroundColor: new Color(0xff17AF6B).withOpacity(1.0),
      ),
      body: authUserProvider.getIsLogin() ? ((myDoctorsProvider.getMyDoctors() != null && myDoctorsProvider.getMyDoctors().data.length != 0) ? _getDoctorsUI() : _getNoDoctorsUI()) : _getNoDoctorsUI(),

      // hasDoctors ? _getDoctorsUI() : _getNoDoctorsUI(),
    );
  }

  _getNoDoctorsUI() {
    return SingleChildScrollView(
      child: Container(
        margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.2),
        child: Center(
          child: Column(
            children: <Widget>[
              Container(child: SvgPicture.asset('assets/images/my_doctor.svg', width: 130.0, height: 130.0)),
              Container(
                margin: EdgeInsets.only(left: 80.0, right: 80.0, top: 20.0),
                child: Text("Always stay informed about your doctors",
                    textAlign: TextAlign.center, style: TextStyle(fontSize: 22.0, letterSpacing: 0.5, fontWeight: FontWeight.bold, fontFamily: 'Open-Sans-Bold', color: Colors.black)),
              ),
              Container(
                  margin: EdgeInsets.only(left: 80.0, right: 80.0, top: 20.0),
                  child: Text("Conveniently book again with your favoured doctors",
                      textAlign: TextAlign.center, style: TextStyle(fontSize: 18.0, letterSpacing: 0.5, fontFamily: 'Open-Sans', color: Colors.black))),
              Container(
                margin: EdgeInsets.only(left: 60.0, right: 60.0, top: 20.0),
                child: RaisedButton(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0), side: BorderSide(color: new Color(0xff17AF6B).withOpacity(1.0))),
                  color: new Color(0xff17AF6B).withOpacity(1.0),
                  onPressed: () async {
                    // if (await askVideoCallPermission()) {
                    //   print("You can proceed.");
                    // } else {
                    //   print("You must give permissions for live care.");
                    // }
                    openBookAppointment();
                  },
                  child: Column(
                    children: <Widget>[
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(10.0),
                        margin: EdgeInsets.only(left: 5.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Text('Book an appointment', textAlign: TextAlign.center, style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 18.0, letterSpacing: 0.5)),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<bool> askVideoCallPermission() async {
    if (!(await Permission.camera.request().isGranted) || !(await Permission.microphone.request().isGranted)) {
      return false;
    }
    if (!(await PlatformBridge.shared().isDrawOverAppsPermissionAllowed())) {
      await drawOverAppsMessageDialog(context);
      return false;
    }
    return true;
  }

  Future drawOverAppsMessageDialog(BuildContext context) async {
    await showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: Text("Please select 'MedZone' from the list and allow draw over app permission to use live care."),
          contentPadding: EdgeInsets.fromLTRB(24.0, 20.0, 24.0, 0.0),
          actions: [
            FlatButton(
              child: Text("Cancel"),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            FlatButton(
              child: Text("Go to Settings"),
              onPressed: () async {
                await PlatformBridge.shared().askDrawOverAppsPermission();
                Navigator.pop(context);
              },
            )
          ],
        );
      },
    );
  }

  openBookAppointment() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => SearchByClinic()));
  }

  _getDoctorsUI() {
    return Container(
      height: MediaQuery.of(context).size.height,
      padding: EdgeInsets.all(10.0),
      child: ListView.builder(
        itemCount: myDoctorsProvider.getMyDoctors() != null ? myDoctorsProvider.getMyDoctors().data.length : 0,
        itemBuilder: (context, index) {
          return DoctorView(
            isClickable: true,
            isShowAddress: false,
            isMyDoctor: true,
            removeFunction: widget.removeFunction,
            doc: myDoctorsProvider.getMyDoctors().data[index],
          );
        },
      ),
    );
  }
}
