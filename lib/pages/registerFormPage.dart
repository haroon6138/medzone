import 'dart:convert';

import 'package:MedZone/config/config.dart';
import 'package:MedZone/models/AuthUserProvider.dart';
import 'package:MedZone/models/response/AuthenticatedUser.dart';
import 'package:MedZone/routes.dart';
import 'package:MedZone/services/CommonServices.dart';
import 'package:MedZone/services/Covid-Services.dart';
import 'package:MedZone/utils/sms-popup.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';

class RegisterFormPage extends StatefulWidget {
  @override
  _RegisterFormPageState createState() => _RegisterFormPageState();
}

class _RegisterFormPageState extends State<RegisterFormPage> {
  final myFirstNameController = TextEditingController();
  final myLastNameController = TextEditingController();
  final TextEditingController myPhoneNumberController = TextEditingController();
  final myEmailController = TextEditingController();
  final myPasswordController = TextEditingController();

  AuthUserProvider authUserProvider;

  @override
  Widget build(BuildContext context) {
    authUserProvider = Provider.of<AuthUserProvider>(context);
    final node = FocusScope.of(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
          title: Text("Sign Up",
              style: TextStyle(
                fontSize: 16.0,
                fontWeight: FontWeight.bold,
                letterSpacing: 0.5,
                fontFamily: 'Open-Sans',
              )),
          backgroundColor: new Color(0xff17AF6B).withOpacity(1.0)),
      body: Container(
        margin: EdgeInsets.all(15.0),
        child: SingleChildScrollView(
          child: Container(
            height: MediaQuery.of(context).size.height,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: [
                Text("Welcome to MedZone", style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.bold, fontFamily: "Open-Sans-Bold", letterSpacing: 0.6)),
                Container(
                  margin: EdgeInsets.only(top: 10.0),
                  child: Text("Sign up to see and manage your appointments, favourite doctors & much more", style: TextStyle(fontSize: 16.0, letterSpacing: 0.4)),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10.0),
                  child: Text("Enter your details below to sign up", style: TextStyle(fontSize: 16.0, letterSpacing: 0.4)),
                ),
                //First Name TextField
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
                      width: MediaQuery.of(context).size.width * 0.9,
                      height: 50.0,
                      child: Row(
                        children: <Widget>[
                          new Flexible(
                            child: new TextField(
                              textInputAction: TextInputAction.next,
                              onEditingComplete: () => node.nextFocus(),
                              controller: myFirstNameController,
                              decoration: const InputDecoration(
                                border: OutlineInputBorder(),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green, width: 2.0),
                                ),
                                labelText: "First Name",
                                labelStyle: TextStyle(color: Colors.grey),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                //Last Name TextField
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
                      width: MediaQuery.of(context).size.width * 0.9,
                      height: 50.0,
                      child: Row(
                        children: <Widget>[
                          new Flexible(
                            child: new TextField(
                              keyboardType: TextInputType.text,
                              textInputAction: TextInputAction.next,
                              onEditingComplete: () => node.nextFocus(),
                              controller: myLastNameController,
                              decoration: const InputDecoration(
                                border: OutlineInputBorder(),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green, width: 2.0),
                                ),
                                labelText: "Last Name",
                                labelStyle: TextStyle(color: Colors.grey),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                //Email TextField
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
                      width: MediaQuery.of(context).size.width * 0.9,
                      height: 50.0,
                      child: Row(
                        children: <Widget>[
                          new Flexible(
                            child: new TextField(
                              keyboardType: TextInputType.emailAddress,
                              textInputAction: TextInputAction.next,
                              onEditingComplete: () => node.nextFocus(),
                              controller: myEmailController,
                              decoration: const InputDecoration(
                                border: OutlineInputBorder(),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green, width: 2.0),
                                ),
                                labelText: "Email Address",
                                labelStyle: TextStyle(color: Colors.grey),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                //Password TextField
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
                      width: MediaQuery.of(context).size.width * 0.9,
                      height: 50.0,
                      child: Row(
                        children: <Widget>[
                          new Flexible(
                            child: new TextField(
                              keyboardType: TextInputType.visiblePassword,
                              obscureText: true,
                              textInputAction: TextInputAction.done,
                              onSubmitted: (_) {
                                registerUser();
                              },
                              controller: myPasswordController,
                              decoration: const InputDecoration(
                                border: OutlineInputBorder(),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green, width: 2.0),
                                ),
                                labelText: "Password",
                                labelStyle: TextStyle(color: Colors.grey),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Container(
                  margin: EdgeInsets.only(top: 10.0),
                  child: Text("• Has at least 8 characters (no spaces)", style: TextStyle(fontSize: 14.0, letterSpacing: 0.4, fontWeight: FontWeight.bold)),
                ),
                Container(
                  margin: EdgeInsets.only(top: 0.0),
                  child: Text("• Must be a combination of letters & numbers", style: TextStyle(fontSize: 14.0, letterSpacing: 0.4, fontWeight: FontWeight.bold)),
                ),
                Container(
                  margin: EdgeInsets.only(top: 0.0),
                  child: Text("• Must contain at least 1 uppercase letter", style: TextStyle(fontSize: 14.0, letterSpacing: 0.4, fontWeight: FontWeight.bold)),
                ),
                Container(
                  margin: EdgeInsets.only(top: 0.0),
                  child: Text("• Must contain at least 1 special character", style: TextStyle(fontSize: 14.0, letterSpacing: 0.4, fontWeight: FontWeight.bold)),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(0.0, 40.0, 0.0, 0.0),
                  height: 50.0,
                  width: MediaQuery.of(context).size.width * 0.9,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0), side: BorderSide(color: new Color(0xffFF2B56).withOpacity(1.0))),
                    color: new Color(0xffFF2B56).withOpacity(1.0),
                    onPressed: () {
                      registerUser();
                      // showSMSDialog();
                    },
                    child: Column(
                      children: <Widget>[
                        Container(
                          transform: Matrix4.translationValues(0.0, 5.0, 0.0),
                          margin: EdgeInsets.only(top: 7.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text('Sign up', overflow: TextOverflow.fade, style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 16.5, letterSpacing: 0.8)),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  registerUser() {
    if (myFirstNameController.text.isEmpty || myLastNameController.text.isEmpty || myEmailController.text.isEmpty || myPasswordController.text.isEmpty) {
      CommonService.showToast(context, "Please enter all details");
    } else if (!isEmail(myEmailController.text)) {
      CommonService.showToast(context, "Please enter correct email address");
    } else if (!isPasswordCompliant(myPasswordController.text)) {
      CommonService.showToast(context, "Please enter valid password");
    } else {
      callRegisterAPI();
    }
  }

  void showSMSDialog() {
    new SMSOTP(
      context,
      (value) {
        // submit(model, value);
      },
      () => {
        // Navigator.pop(context),
      },
    ).displayDialog(context);
  }

  callRegisterAPI() async {
    bool isRegistered = false;
    APIService service = new APIService();
    String fcmToken = await CommonService.getPrefs(FIREBASE_TOKEN, false);
    AuthenticatedUser authenticatedUser = new AuthenticatedUser();
    EasyLoading.show(status: CommonService.getLoadingMessage(), maskType: EasyLoadingMaskType.black);
    service.registerUser(myFirstNameController.text, myLastNameController.text, myEmailController.text, myPasswordController.text, fcmToken, context).then((res) {
      setState(() {
        EasyLoading.dismiss();
        if (res.containsKey("response") && res['response'] == 'false') {
          CommonService.showToast(context, res['data']);
        } else {
          authenticatedUser = AuthenticatedUser.fromJson(res);
          CommonService.setPrefs(AUTHENTICATED_USER, json.encode(res));
          CommonService.setPrefs(JWT_TOKEN, authenticatedUser.token);
          authUserProvider.setAuthUser(authenticatedUser);
          authUserProvider.setIsLogin(true);

          print(authUserProvider.getIsLogin());
          print(authUserProvider.getAuthUser().token);
          print(authUserProvider.getAuthUser().user.email);

          CommonService.showToast(context, "Registration Successful");

          Navigator.popAndPushNamed(context, MY_ACCOUNT);
        }
      });
    }).catchError((err) {
      EasyLoading.dismiss();
      print(err);
    });
  }

  callLoginAPI(String email, String password, String fcmToken) {
    APIService service = new APIService();
    EasyLoading.show(status: CommonService.getLoadingMessage(), maskType: EasyLoadingMaskType.black);
    service.loginUser(email, password, fcmToken, context).then((res) {
      CommonService.showToast(context, "Successfully Logged In");
      EasyLoading.dismiss();
    }).catchError((err) {
      print(err);
      EasyLoading.dismiss();
    });
  }

  bool isEmail(String string) {
    if (string == null || string.isEmpty) {
      return true;
    }
    const pattern = r'^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$';
    final regExp = RegExp(pattern);
    if (!regExp.hasMatch(string)) {
      return false;
    }
    return true;
  }

  bool isPasswordCompliant(String password, [int minLength = 8]) {
    if (password == null || password.isEmpty) {
      return false;
    }

    bool hasUppercase = password.contains(new RegExp(r'[A-Z]'));
    bool hasDigits = password.contains(new RegExp(r'[0-9]'));
    bool hasLowercase = password.contains(new RegExp(r'[a-z]'));
    bool hasSpecialCharacters = password.contains(new RegExp(r'[!@#$%^&*(),.?":{}|<>]'));
    bool hasMinLength = password.length > minLength;

    return hasDigits & hasUppercase & hasLowercase & hasSpecialCharacters & hasMinLength;
  }
}
