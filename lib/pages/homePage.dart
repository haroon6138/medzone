import 'dart:convert';
import 'dart:io';

import 'package:MedZone/config/config.dart';
import 'package:MedZone/custom_widget/bottom_button.dart';
import 'package:MedZone/custom_widget/card_home.dart';
import 'package:MedZone/custom_widget/select_language.dart';
import 'package:MedZone/custom_widget/top_findings_card.dart';
import 'package:MedZone/models/AuthUserProvider.dart';
import 'package:MedZone/models/IncomingCallData.dart';
import 'package:MedZone/models/TopFindingsModel.dart';
import 'package:MedZone/models/currentLocation.dart';
import 'package:MedZone/models/response/AuthenticatedUser.dart';
import 'package:MedZone/models/response/HomeDataResponse.dart';
import 'package:MedZone/pages/All_Providers.dart';
import 'package:MedZone/pages/No_Internet.dart';
import 'package:MedZone/pages/SearchMedicine.dart';
import 'package:MedZone/pages/conference/incoming_call.dart';
import 'package:MedZone/pages/noLocation.dart';
import 'package:MedZone/pages/selectLocation.dart';
import 'package:MedZone/pages/teleconsultation/Teleconsultation.dart';
import 'package:MedZone/services/CommonServices.dart';
import 'package:MedZone/services/Covid-Services.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:geolocator/geolocator.dart';
import 'package:provider/provider.dart';

import 'appointments/SearchByClinic.dart';

class HomePageWidget extends StatefulWidget {
  static bool isOpenCallPage = false;
  static IncomingCallData incomingCallData = new IncomingCallData();

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePageWidget>
    with SingleTickerProviderStateMixin, WidgetsBindingObserver {
  List<String> catList = [
    "pharmacy",
    "hospital",
    "urgentCare",
    "harmReduction"
  ];

  List<String> catListName = [
    "PHARMACY",
    "HOSPITAL",
    "URGENT CARE",
    "HARM REDUCTION"
  ];

  var currentSelectedText = "PHARMACY";

  bool isDataLoaded = false;

  Position _location = Position(latitude: 0.0, longitude: 0.0);
  var currentLat = 0.0;
  var currentLong = 0.0;
  String _currentAddress = "";
  dynamic placeObject;

  String pharmacyCount = "0";
  String urgentCareCount = "0";
  String hospitalCount = "0";
  String harmReductionCount = "0";

  HomeData homeDataResponse;

  bool isShowFabButton = true;

  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  var locationProvider;

  AnimationController _animationController;
  Animation _animation;

  ScrollController _controller;

  // FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;

  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  AuthUserProvider authUserProvider;

  bool isPageNavigated = false;

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    var route = ModalRoute.of(context);
    if (route != null) {
      print(route.settings.name);
    }

    print("didChangeAppLifecycleState");
    print('state = $state');
    AppGlobal.context = context;
    if (state == AppLifecycleState.resumed) {
      if (HomePageWidget.isOpenCallPage) {
        if (Platform.isAndroid) {
          return;
        }
        if (!isPageNavigated) {
          isPageNavigated = true;
          Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => IncomingCall(
                          incomingCallData: HomePageWidget.incomingCallData)))
              .then((value) {
            if (Platform.isAndroid) {
              Future.delayed(Duration(seconds: 5), () {
                isPageNavigated = false;
              });
            } else
              isPageNavigated = false;
          });
        }
      }
    }

    if (state == AppLifecycleState.paused) {
      isPageNavigated = false;
    }

    if (state == AppLifecycleState.inactive) {
      isPageNavigated = false;
    }
  }

  @override
  void initState() {
    homeDataResponse = new HomeData();
    _controller = ScrollController();
    _controller.addListener(_scrollListener);

    _firebaseMessaging.setAutoInitEnabled(true);

    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(
            sound: true, badge: true, alert: true, provisional: false));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });


    _firebaseMessaging.getToken().then((String token) {
      assert(token != null);
      print("FirebaseToken: " + token);
      CommonService.setPrefs(FIREBASE_TOKEN, token);
    });

    // if (Platform.isIOS)
    //   _firebaseMessaging.getAPNSToken().then((String token) {
    //     assert(token != null);
    //     print("FirebaseTokenAPNS: " + token);
    //     CommonService.setPrefs(APNS_TOKEN, token);
    //   });

    // FirebaseMessaging.onMessage.listen((RemoteMessage message) {
    //   print('Got a message whilst in the foreground!');
    //   print('Message data: ${message.data}');
    //
    //   if (message.notification != null) {
    //     print('Message also contained a notification: ${message.notification}');
    //   }
    //     print("onMessage: $message");
    //     print(message);
    //
    //     if (Platform.isIOS) {
    //       if (message.data['is_call'] == "true") {
    //         var route = ModalRoute.of(context);
    //
    //         if (route != null) {
    //           print(route.settings.name);
    //         }
    //
    //         if (message.data["notification_type"].toString() == "10") {
    //           Map<String, dynamic> myMap =
    //           new Map<String, dynamic>.from(message.data);
    //           print(myMap);
    //           HomePageWidget.isOpenCallPage = true;
    //           HomePageWidget.incomingCallData =
    //               IncomingCallData.fromJson(myMap);
    //           if (!isPageNavigated) {
    //             isPageNavigated = true;
    //             Navigator.push(
    //                 context,
    //                 MaterialPageRoute(
    //                     builder: (context) => IncomingCall(
    //                         incomingCallData: HomePageWidget
    //                             .incomingCallData))).then((value) {
    //               isPageNavigated = false;
    //             });
    //           }
    //         }
    //       } else {
    //         print("Is Call Not Found iOS");
    //       }
    //     } else {
    //       print("Is Call Not Found iOS");
    //     }
    //
    //     if (Platform.isAndroid) {
    //       if (message.data["is_call"] == "true") {
    //         var route = ModalRoute.of(context);
    //
    //         if (route != null) {
    //           print(route.settings.name);
    //         }
    //
    //         if (message.data["notification_type"].toString() == "10") {
    //           Map<String, dynamic> myMap =
    //           new Map<String, dynamic>.from(message.data);
    //           print(myMap);
    //           if (HomePageWidget.isOpenCallPage) {
    //             return;
    //           }
    //           HomePageWidget.isOpenCallPage = true;
    //           HomePageWidget.incomingCallData =
    //               IncomingCallData.fromJson(myMap);
    //           if (!isPageNavigated) {
    //             isPageNavigated = true;
    //             Navigator.push(
    //                 context,
    //                 MaterialPageRoute(
    //                     builder: (context) => IncomingCall(
    //                         incomingCallData: HomePageWidget
    //                             .incomingCallData))).then((value) {
    //               Future.delayed(Duration(seconds: 3), () {
    //                 isPageNavigated = false;
    //               });
    //             });
    //           }
    //         }
    //       } else {
    //         print("Is Call Not Found Android");
    //       }
    //     } else {
    //       print("Is Call Not Found Android");
    //     }
    //
    // });

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        print(message);

        if (Platform.isIOS) {
          if (message['is_call'] == "true") {
            var route = ModalRoute.of(context);

            if (route != null) {
              print(route.settings.name);
            }

            if (message["notification_type"].toString() == "10") {
              Map<String, dynamic> myMap =
                  new Map<String, dynamic>.from(message);
              print(myMap);
              HomePageWidget.isOpenCallPage = true;
              HomePageWidget.incomingCallData =
                  IncomingCallData.fromJson(myMap);
              if (!isPageNavigated) {
                isPageNavigated = true;
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => IncomingCall(
                            incomingCallData: HomePageWidget
                                .incomingCallData))).then((value) {
                  isPageNavigated = false;
                });
              }
            }
          } else {
            print("Is Call Not Found iOS");
          }
        } else {
          print("Is Call Not Found iOS");
        }

        if (Platform.isAndroid) {
          if (message['data'].containsKey("is_call")) {
            var route = ModalRoute.of(context);

            if (route != null) {
              print(route.settings.name);
            }

            if (message["data"]["notification_type"].toString() == "10") {
              Map<String, dynamic> myMap =
                  new Map<String, dynamic>.from(message['data']);
              print(myMap);
              if (HomePageWidget.isOpenCallPage) {
                return;
              }
              HomePageWidget.isOpenCallPage = true;
              HomePageWidget.incomingCallData =
                  IncomingCallData.fromJson(myMap);
              if (!isPageNavigated) {
                isPageNavigated = true;
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => IncomingCall(
                            incomingCallData: HomePageWidget
                                .incomingCallData))).then((value) {
                  Future.delayed(Duration(seconds: 3), () {
                    isPageNavigated = false;
                  });
                });
              }
            }
            // if (message["data"]["notification_type"].toString() == "30") {
            //   HomePageWidget.isOpenCallPage = false;
            //   Navigator.pop(context);
            // }
          } else {
            print("Is Call Not Found Android");
          }
        } else {
          print("Is Call Not Found Android");
        }
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
      },
    );

    WidgetsBinding.instance.addPostFrameCallback((_) {
      _displayCurrentLocation(locationProvider);
    });

    _animationController =
        AnimationController(vsync: this, duration: Duration(seconds: 1));
    _animationController.repeat(reverse: true);
    _animation = Tween(begin: 2.0, end: 15.0).animate(_animationController)
      ..addListener(() {
        setState(() {});
      });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    AppGlobal.context = context;
    locationProvider = Provider.of<CurrentLocation>(context);
    authUserProvider = Provider.of<AuthUserProvider>(context);
    return isDataLoaded ? _getHomePage(locationProvider) : Container();
  }

  _getHomePage(locationProvider) {
    if (Provider.of<DataConnectionStatus>(context) ==
        DataConnectionStatus.disconnected) {
      return NoInternet();
    } else {
      return Scaffold(
        floatingActionButton: isShowFabButton
            ? InkWell(
                onTap: () {
                  openAppointments();
                },
                child: Container(
                  width: 100,
                  height: 100,
                  child: SvgPicture.asset("assets/images/fab_btn.svg"),
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: new Color(0xfff57e7c).withOpacity(1.0),
                      boxShadow: [
                        BoxShadow(
                            color: new Color(0xfff57e7c).withOpacity(1.0),
                            blurRadius: _animation.value,
                            spreadRadius: _animation.value)
                      ]),
                ),
              )
            : Container(),
        body: Container(
          padding: EdgeInsets.fromLTRB(20, 50, 16, 16),
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                new Color(0xff17AF6B).withOpacity(1.0),
                new Color(0xffF1F2F2).withOpacity(1.0),
              ],
              stops: [0.2, 0.5],
            ),
          ),
          child: SingleChildScrollView(
            controller: _controller,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Column(
                            children: <Widget>[
                              Container(
                                  padding: EdgeInsets.only(left: 8),
                                  child: SvgPicture.asset(
                                      'assets/images/map_pin_white_icon.svg')),
                            ],
                          ),
                          GestureDetector(
                            onTap: () {
                              navigateToSelectLocation(context);
                            },
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Container(
                                      padding: EdgeInsets.only(left: 8),
                                      child: new Text(
                                        "currentLocation",
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                            fontSize: 13,
                                            color: Colors.white,
                                            letterSpacing: 0.5,
                                            fontFamily: 'Open-Sans',
                                            fontWeight: FontWeight.bold),
                                      ).tr(),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: <Widget>[
                                    Container(
                                      width: MediaQuery.of(context).size.width *
                                          0.75,
                                      padding: EdgeInsets.only(left: 8),
                                      child: Text(
                                          locationProvider.getCurrentAddress(),
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                              fontSize: 13,
                                              fontFamily: 'Open-Sans',
                                              letterSpacing: 0.5,
                                              color: new Color(0xffF1F2F2)
                                                  .withOpacity(1.0))),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Column(
                            children: <Widget>[
                              InkWell(
                                onTap: () {
                                  openLanguageDialog();
                                },
                                child: Container(
                                    width: 30.0,
                                    height: 30.0,
                                    child: SvgPicture.asset(
                                        'assets/images/translate_icon.svg')),
                              ),
                              InkWell(
                                onTap: () {
                                  openLanguageDialog();
                                },
                                child: Container(
                                  transform:
                                      Matrix4.translationValues(0.0, -3.0, 0.0),
                                  alignment: Alignment.bottomRight,
                                  child: Text(
                                      context.locale.toString().toUpperCase(),
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontFamily: 'Open-Sans-bold',
                                          fontWeight: FontWeight.bold,
                                          letterSpacing: 0.5,
                                          color: new Color(0xffF1F2F2)
                                              .withOpacity(1.0))),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Row(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(top: 15),
                      margin: EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0),
                      child: Text("welcome",
                              style: TextStyle(
                                  fontSize: 16,
                                  fontFamily: 'Open-Sans',
                                  letterSpacing: 0.5,
                                  color:
                                      new Color(0xffF1F2F2).withOpacity(1.0)))
                          .tr(),
                    ),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(top: 5, left: 10.0),
                      width: MediaQuery.of(context).size.width * 0.88,
                      child: Text("findNearest",
                              overflow: TextOverflow.fade,
                              style: TextStyle(
                                  fontSize: 22,
                                  fontFamily: 'Open-Sans',
                                  fontWeight: FontWeight.bold,
                                  letterSpacing: 1.0,
                                  color:
                                      new Color(0xffF1F2F2).withOpacity(1.0)))
                          .tr(),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Expanded(
                      child: InkWell(
                        onTap: () {
                          openTeleconsultation();
                        },
                        child: Card(
                          margin: EdgeInsets.fromLTRB(8.0, 16.0, 8.0, 0.0),
                          shape: RoundedRectangleBorder(
                            side: BorderSide(color: Colors.white70, width: 1),
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Container(
                            child: Column(
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Container(
                                      margin: EdgeInsets.fromLTRB(11.0, 5.0, 0.0, 0.0),
                                      child: Column(
                                        children: <Widget>[
                                          Text("Teleconsultation",
                                              style: TextStyle(
                                                  fontSize: 22,
                                                  fontFamily: 'Open-Sans',
                                                  fontWeight: FontWeight.w900,
                                                  letterSpacing: 0.8,
                                                  color: new Color(0xff17AF6B).withOpacity(1.0))),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(top: 5.0),
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: <Widget>[
                                          SvgPicture.asset("assets/images/mobile.svg", width: 60.0, height: 60.0),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: <Widget>[
                                    Container(
                                      // width: MediaQuery.of(context).size.width * 0.34,
                                      margin: EdgeInsets.fromLTRB(11.0, 0.0, 10.0, 0.0),
                                      child: Text("With pharmacies & veterinarians",
                                          overflow: TextOverflow.fade,
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontFamily: 'Open-Sans',
                                              fontWeight: FontWeight.bold,
                                              letterSpacing: 0.5,
                                              color: Colors.black)),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: <Widget>[
                                    Container(
                                      margin: EdgeInsets.fromLTRB(11.0, 0.0, 10.0, 0.0),
                                      child: Text("nearYou",
                                          style: TextStyle(
                                              fontSize: 12,
                                              fontFamily: 'Open-Sans',
                                              fontWeight: FontWeight.bold,
                                              letterSpacing: 0.5,
                                              color: Colors.grey[600]))
                                          .tr(),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: <Widget>[
                                    Container(
                                      margin: EdgeInsets.fromLTRB(11.0, 3.0, 10.0, 12.0),
                                      child: Text("clickToSee",
                                          style: TextStyle(
                                              fontSize: 11,
                                              fontFamily: 'Open-Sans',
                                              fontWeight: FontWeight.bold,
                                              letterSpacing: 0.5,
                                              color: Colors.redAccent[200]))
                                          .tr(),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: <Widget>[],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        child: CardHome(
                            count: pharmacyCount.toString(),
                            image: "assets/images/pharmacy_icon.svg",
                            name: "pharmacy",
                            currentLat: currentLat,
                            currentLong: currentLong,
                            type: "pharmacies"),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        child: CardHome(
                            count: urgentCareCount.toString(),
                            image: "assets/images/urgent_care_icon.svg",
                            name: "urgentCare",
                            currentLat: currentLat,
                            currentLong: currentLong,
                            type: "urgent_cares"),
                      ),
                    ),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        child: CardHome(
                            count: hospitalCount,
                            image: "assets/images/hospital_icon.svg",
                            name: "hospital",
                            currentLat: currentLat,
                            currentLong: currentLong,
                            type: "hospitals"),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        child: CardHome(
                            count: harmReductionCount.toString(),
                            image: "assets/images/harm_reduction_icon.svg",
                            name: "harmReduction",
                            currentLat: currentLat,
                            currentLong: currentLong,
                            type: "harm_reductions"),
                      ),
                    ),
                  ],
                ),
                Row(children: <Widget>[
                  Expanded(
                    flex: 2,
                    child: Container(
                      margin: EdgeInsets.fromLTRB(10.0, 10.0, 0.0, 0.0),
                      height: 60.0,
                      width: MediaQuery.of(context).size.width * 0.9,
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            side: BorderSide(
                                color: new Color(0xffFF2B56).withOpacity(1.0))),
                        color: new Color(0xffFF2B56).withOpacity(1.0),
                        onPressed: () {
                          navigateToSearchMedicine(context);
                        },
                        child: Column(
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(top: 7.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text('searchMed',
                                          overflow: TextOverflow.fade,
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 16.5,
                                              letterSpacing: 0.8))
                                      .tr(),
                                  Container(
                                    transform: Matrix4.translationValues(
                                        0.0, 7.0, 0.0),
                                    child: SvgPicture.asset(
                                        "assets/images/search_medicine.svg",
                                        width: 25.0,
                                        height: 25.0,
                                        fit: BoxFit.contain),
                                  ),
                                ],
                              ),
                            ),
                            Row(
                              children: <Widget>[
                                Container(
                                  child: Text('inStoreNearYou',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 14.0,
                                              letterSpacing: 0.1))
                                      .tr(),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(
                      margin: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 0.0),
                      height: 60.0,
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            side: BorderSide(
                                color: new Color(0xff17AF6B).withOpacity(1.0))),
                        color: new Color(0xff17AF6B).withOpacity(1.0),
                        onPressed: () {
                          navigateToViewAll(context);
                        },
                        child: Column(
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
                              child: SvgPicture.asset(
                                  "assets/images/more_icon.svg",
                                  width: 14.0,
                                  height: 14.0,
                                  fit: BoxFit.contain),
                            ),
                            Container(
                              margin: context.locale.toString() == 'es'
                                  ? EdgeInsets.only(top: 5.0)
                                  : EdgeInsets.only(top: 5.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text('more',
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 15.0,
                                              letterSpacing: 0.8))
                                      .tr(),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ]),
                Container(
                  padding: EdgeInsets.only(top: 5),
                  margin: EdgeInsets.fromLTRB(10.0, 10.0, 0.0, 0.0),
                  child: Text("topNearYou",
                          style: TextStyle(
                              fontSize: 22,
                              fontFamily: 'Open-Sans',
                              fontWeight: FontWeight.bold,
                              letterSpacing: 0.6,
                              color: Colors.black54))
                      .tr(),
                ),
                Container(
                    height: 60.0,
                    margin: EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0),
                    child: ListView(
                        scrollDirection: Axis.horizontal,
                        children: _getListData())),
                Column(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    getCurrentListCount() == 0
                        ? Container(
                            width: MediaQuery.of(context).size.width,
                            margin: EdgeInsets.only(top: 40.0),
                            height: 100,
                            child: Text(
                                "No " +
                                    currentSelectedText.toLowerCase() +
                                    " found near you.",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color:
                                        new Color(0xffFF2B56).withOpacity(1.0),
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.bold)),
                          )
                        : Container(
                            height: 490,
                            child: ListView.builder(
                              physics: const NeverScrollableScrollPhysics(),
                              padding: EdgeInsets.all(0.0),
                              itemCount: getCurrentListCount(),
                              itemBuilder: (context, index) {
                                return _getTopFindingsCurrent(index);
                              },
                            ),
                          ),
                  ],
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 20.0),
                  child: Flex(
                    direction: Axis.horizontal,
                    children: <Widget>[
                      Expanded(
                          child: BottomButton(
                              text: "Contact",
                              callBack: "contact",
                              image: "assets/images/contact_icon.svg")),
                      Expanded(
                        child: BottomButton(
                            text: "Privacy",
                            callBack: "privacy",
                            image: "assets/images/privacy_policy_icon.svg"),
                      ),
                      Expanded(
                        child: BottomButton(
                          text: "Covid-19",
                          callBack: "covid",
                          image: "assets/images/covid-19-icon.svg",
                          placeObject: placeObject,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    }
  }

  Future navigateToSelectLocation(context) async {
    await Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => SelectLocation(
              _location.latitude, _location.longitude, _currentAddress)),
    ).then((value) {
      getHomeData(context, locationProvider.getCurrentLat().toString(),
          locationProvider.getCurrentLong().toString());
    });
  }

  void _displayCurrentLocation(locationProvider) async {
    if (currentLat == 0.0 && currentLong == 0.0) {
      await Geolocator().isLocationServiceEnabled().then((enabled) {
        if (enabled) {
          Geolocator()
              .getLastKnownPosition(
                  desiredAccuracy: LocationAccuracy.bestForNavigation)
              .then((Position position) {
            setState(() {
              _location = position;
              print(_location);
              currentLat = _location.latitude;
              currentLong = _location.longitude;

              locationProvider.setCurrentLat(currentLat);
              locationProvider.setCurrentLong(currentLong);

              CommonService.setPrefsDouble(
                  CommonService.currentLat, currentLat);
              CommonService.setPrefsDouble(
                  CommonService.currentLong, currentLong);
              getHomeData(
                  context, currentLat.toString(), currentLong.toString());
            });
            _getAddressFromLatLng(locationProvider);
          }).catchError((e) {
            print(e);
            _displayCurrentLocation(locationProvider);
          });
        } else {
          navigateToNoLocation(context);
        }
      });
    }
  }

  _getAddressFromLatLng(locationProvider) async {
    try {
      List<Placemark> p = await geolocator.placemarkFromCoordinates(
          _location.latitude, _location.longitude,
          localeIdentifier: context.locale.toString());

      CommonService.setPrefsDouble(
          CommonService.currentLat, _location.latitude);
      CommonService.setPrefsDouble(
          CommonService.currentLong, _location.longitude);

      Placemark place = p[0];

      setState(() {
        _currentAddress =
            "${place.name}, ${place.locality}, ${place.subAdministrativeArea}, ${place.administrativeArea}";

        print(place.administrativeArea);
        print(place.locality);
        print(place.subAdministrativeArea);

        placeObject = place;

        locationProvider.setCurrentAddress(_currentAddress);
      });
    } catch (e) {
      print(e);
    }
  }

  openLanguageDialog() {
    var currentLat;
    var currentLong;

    CommonService.getPrefsDouble(CommonService.currentLat, false).then((value) {
      currentLat = value;
      print(currentLat);
    });

    CommonService.getPrefsDouble(CommonService.currentLong, false)
        .then((value) {
      currentLong = value;
      print(currentLong);
    });

    showGeneralDialog(
            barrierColor: Colors.black.withOpacity(0.5),
            transitionBuilder: (context, a1, a2, widget) {
              final curvedValue =
                  Curves.easeInOutBack.transform(a1.value) - 1.0;
              return Transform(
                transform:
                    Matrix4.translationValues(0.0, curvedValue * 200, 0.0),
                child: Opacity(
                  opacity: a1.value,
                  child: SelectLanguage(),
                ),
              );
            },
            transitionDuration: Duration(milliseconds: 500),
            barrierDismissible: true,
            barrierLabel: '',
            context: context,
            pageBuilder: (context, animation1, animation2) {})
        .then((value) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        getHomeData(context, currentLat.toString(), currentLong.toString());
      });
    });
  }

  Future navigateToSearchMedicine(context) async {
    await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => searchMedicine()),
    );
  }

  Future navigateToViewAll(context) async {
    Count count = new Count();
    count.pharmacy = homeDataResponse.count.pharmacy;
    count.urgentCare = homeDataResponse.count.urgentCare;
    count.hospital = homeDataResponse.count.hospital;
    count.harmReduction = homeDataResponse.count.harmReduction;
    count.laboratory = homeDataResponse.count.laboratory;
    count.radiologyCentre = homeDataResponse.count.radiologyCentre;
    count.chiropractory = homeDataResponse.count.chiropractory;
    count.physicalTherapy = homeDataResponse.count.physicalTherapy;
    count.veterinarian = homeDataResponse.count.veterinarian;
    count.ophthalmologist = homeDataResponse.count.ophthalmologist;

    await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => AllProviders(count: count)),
    );
  }

  Future navigateToNoLocation(context) async {
    await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => NoLocation()),
    );
  }

  _captureTextClick(text) {
    currentSelectedText = text;
  }

  _getListData() {
    List<Widget> widgets = [];
    for (int i = 0; i <= 3; i++) {
      widgets.add(Padding(
          padding: EdgeInsets.fromLTRB(0.0, 0.0, 12.0, 0.0),
          child: Column(
            children: <Widget>[
              Expanded(
                child: Container(),
              ),
              FlatButton(
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  onPressed: () {
                    setState(() {
                      _captureTextClick(catListName[i]);
                    });
                  },
                  color: Colors.transparent,
                  padding: EdgeInsets.all(5.0),
                  child: Column(
                    children: <Widget>[
                      Text(catList[i],
                              style: currentSelectedText == catListName[i]
                                  ? TextStyle(
                                      fontSize: 15,
                                      fontFamily: 'Open-Sans',
                                      fontWeight: FontWeight.bold,
                                      color: new Color(0xff17AF6B)
                                          .withOpacity(1.0))
                                  : TextStyle(
                                      fontSize: 15,
                                      fontFamily: 'Open-Sans',
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black54))
                          .tr(),
                    ],
                  )),
            ],
          )));
    }
    return widgets;
  }

  int getCurrentListCount() {
    if (currentSelectedText == "PHARMACY") {
      return homeDataResponse != null && homeDataResponse.topResult != null
          ? homeDataResponse.topResult.pharmacy.length
          : 0;
    } else if (currentSelectedText == "HOSPITAL") {
      return homeDataResponse != null && homeDataResponse.topResult != null
          ? homeDataResponse.topResult.hospital.length
          : 0;
    } else if (currentSelectedText == "URGENT CARE") {
      return homeDataResponse != null && homeDataResponse.topResult != null
          ? homeDataResponse.topResult.urgentCare.length
          : 0;
    } else if (currentSelectedText == "HARM REDUCTION") {
      return homeDataResponse != null && homeDataResponse.topResult != null
          ? homeDataResponse.topResult.harmReduction.length
          : 0;
    } else {
      return 2;
    }
  }

  _getTopFindingsCurrent(index) {
    var obj = new topFindings();
    if (currentSelectedText == "PHARMACY") {
      return TopFindingsCard(
          id: homeDataResponse.topResult.pharmacy[index].id,
          type: "pharmacies",
          address: homeDataResponse.topResult.pharmacy[index].address1,
          image: homeDataResponse.topResult.pharmacy[index].logo == null
              ? "assets/images/medzone_logo.png"
              : homeDataResponse.topResult.pharmacy[index].logo,
          name: homeDataResponse.topResult.pharmacy[index].name,
          distance:
              double.parse(homeDataResponse.topResult.pharmacy[index].distance)
                  .toStringAsFixed(2),
          days: CommonService.getDays(
              homeDataResponse.topResult.pharmacy[index].days),
          timing: CommonService.getTimings(
              homeDataResponse.topResult.pharmacy[index].hours),
          deliveryTiming: obj.getTopFindingsListPhar()[index].deliveryTiming,
          contactNo: homeDataResponse.topResult.pharmacy[index].phoneNumber,
          webURL: obj.getTopFindingsListPhar()[index].webURL,
          latitude:
              homeDataResponse.topResult.pharmacy[index].latlong.split(",")[0],
          longitude: homeDataResponse.topResult.pharmacy[index].latlong
              .split(",")[1]
              .trim(),
          rating: homeDataResponse.topResult.pharmacy[index].rating.toDouble(),
          currentLatitude: locationProvider.getCurrentLat(),
          currentLongitude: locationProvider.getCurrentLong());
    }
    if (currentSelectedText == "HOSPITAL") {
      return TopFindingsCard(
          waitingTime: homeDataResponse.topResult.hospital[index].waitingTime,
          id: homeDataResponse.topResult.hospital[index].id,
          type: "hospitals",
          address: homeDataResponse.topResult.hospital[index].address1,
          image: homeDataResponse.topResult.hospital[index].logo == null
              ? "assets/images/medzone_logo.png"
              : homeDataResponse.topResult.hospital[index].logo,
          name: homeDataResponse.topResult.hospital[index].name,
          distance:
              double.parse(homeDataResponse.topResult.hospital[index].distance)
                  .toStringAsFixed(2),
          days: CommonService.getDays(
              homeDataResponse.topResult.hospital[index].days),
          timing: CommonService.getTimings(
              homeDataResponse.topResult.hospital[index].hours),
          deliveryTiming: obj.getTopFindingsListHosp()[index].deliveryTiming,
          contactNo: homeDataResponse.topResult.hospital[index].phoneNumber,
          webURL: obj.getTopFindingsListHosp()[index].webURL,
          latitude:
              homeDataResponse.topResult.hospital[index].latlong.split(",")[0],
          longitude: homeDataResponse.topResult.hospital[index].latlong
              .split(",")[1]
              .trim(),
          rating: homeDataResponse.topResult.hospital[index].rating.toDouble(),
          currentLatitude: locationProvider.getCurrentLat(),
          currentLongitude: locationProvider.getCurrentLong());
    }
    if (currentSelectedText == "URGENT CARE") {
      return TopFindingsCard(
          waitingTime: homeDataResponse.topResult.urgentCare[index].waitingTime,
          id: homeDataResponse.topResult.urgentCare[index].id,
          type: "urgent_cares",
          address: homeDataResponse.topResult.urgentCare[index].address1,
          image: homeDataResponse.topResult.urgentCare[index].logo == null
              ? "assets/images/medzone_logo.png"
              : homeDataResponse.topResult.urgentCare[index].logo,
          name: homeDataResponse.topResult.urgentCare[index].name,
          distance: double.parse(
                  homeDataResponse.topResult.urgentCare[index].distance)
              .toStringAsFixed(2),
          days: CommonService.getDays(
              homeDataResponse.topResult.urgentCare[index].days),
          timing: CommonService.getTimings(
              homeDataResponse.topResult.urgentCare[index].hours),
          deliveryTiming: obj.getTopFindingsListHosp()[index].deliveryTiming,
          contactNo: homeDataResponse.topResult.urgentCare[index].phoneNumber,
          webURL: obj.getTopFindingsListHosp()[index].webURL,
          latitude: homeDataResponse.topResult.urgentCare[index].latlong
              .split(",")[0],
          longitude: homeDataResponse.topResult.urgentCare[index].latlong
              .split(",")[1]
              .trim(),
          rating:
              homeDataResponse.topResult.urgentCare[index].rating.toDouble(),
          currentLatitude: locationProvider.getCurrentLat(),
          currentLongitude: locationProvider.getCurrentLong());
    }
    if (currentSelectedText == "HARM REDUCTION") {
      return TopFindingsCard(
          id: homeDataResponse.topResult.harmReduction[index].id,
          type: "harm_reductions",
          address: homeDataResponse.topResult.harmReduction[index].address1,
          image: homeDataResponse.topResult.harmReduction[index].logo == null
              ? "assets/images/medzone_logo.png"
              : homeDataResponse.topResult.harmReduction[index].logo,
          name: homeDataResponse.topResult.harmReduction[index].name,
          distance: double.parse(
                  homeDataResponse.topResult.harmReduction[index].distance)
              .toStringAsFixed(2),
          days: CommonService.getDays(
              homeDataResponse.topResult.harmReduction[index].days),
          timing: CommonService.getTimings(
              homeDataResponse.topResult.harmReduction[index].hours),
          deliveryTiming: obj.getTopFindingsListHosp()[index].deliveryTiming,
          contactNo:
              homeDataResponse.topResult.harmReduction[index].phoneNumber,
          webURL: obj.getTopFindingsListHosp()[index].webURL,
          latitude: homeDataResponse.topResult.harmReduction[index].latlong
              .split(",")[0],
          longitude: homeDataResponse.topResult.harmReduction[index].latlong
              .split(",")[1]
              .trim(),
          rating:
              homeDataResponse.topResult.harmReduction[index].rating.toDouble(),
          currentLatitude: locationProvider.getCurrentLat(),
          currentLongitude: locationProvider.getCurrentLong());
    }
  }

  _scrollListener() {
    if (_controller.offset >= _controller.position.maxScrollExtent &&
        !_controller.position.outOfRange) {
      setState(() {
        isShowFabButton = false;
      });
    }
    if (_controller.offset <= _controller.position.minScrollExtent &&
        !_controller.position.outOfRange) {
      setState(() {
        isShowFabButton = true;
      });
    }
  }

  openAppointments() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => SearchByClinic()));
  }

  openTeleconsultation() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => Teleconsultation(lat: locationProvider.getCurrentLat(), long: locationProvider.getCurrentLong(),)));
  }

  checkAuthUser() async {
    var user = await CommonService.getPrefs(AUTHENTICATED_USER, false);
    AuthenticatedUser authenticatedUser = new AuthenticatedUser();
    if (user != null) {
      print("User LoggedIn");
      authenticatedUser = AuthenticatedUser.fromJson(json.decode(user));
      print(authenticatedUser.user.firstName);
      authUserProvider.setAuthUser(authenticatedUser);
      authUserProvider.setIsLogin(true);
      refreshUserToken();
    } else {
      print("User Not LoggedIn");
    }
  }

  refreshUserToken() {
    APIService service = new APIService();
    service.refreshUserToken(context).then((res) {
      print(res['token']);
      CommonService.setPrefs(JWT_TOKEN, res['token']);
    }).catchError((err) {
      print(err);
      authUserProvider.setIsLogin(false);
      CommonService.removePref(AUTHENTICATED_USER);
      CommonService.removePref(JWT_TOKEN);
    });
  }

  getHomeData(context, String lat, String long) {
    isDataLoaded = false;
    checkAuthUser();
    APIService service = new APIService();
    EasyLoading.show(
        status: CommonService.getLoadingMessage(),
        maskType: EasyLoadingMaskType.black);
    service.getHomeData(lat, long, context).then((res) {
      print(res);
      setState(() {
        homeDataResponse = HomeData.fromJson(res);
        pharmacyCount = homeDataResponse.count.pharmacy.toString();
        hospitalCount = homeDataResponse.count.hospital.toString();
        urgentCareCount = homeDataResponse.count.urgentCare.toString();
        harmReductionCount = homeDataResponse.count.harmReduction.toString();

        isDataLoaded = true;
        EasyLoading.dismiss();
      });
    }).catchError((err) {
      setState(() {
        isDataLoaded = true;
        EasyLoading.dismiss();
      });
      print(err);
    });
  }
}
