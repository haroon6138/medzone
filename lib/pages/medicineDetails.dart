import 'package:MedZone/models/response/MedicineDetailsResponse.dart';
import 'package:flutter/material.dart';

class MedicineDetails extends StatelessWidget {
  MedicineDetailsResponse medicineDetailsResponse;

  MedicineDetails({@required this.medicineDetailsResponse});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
          title: Text(
              medicineDetailsResponse.data[0].name +
                  " - " +
                  medicineDetailsResponse.data[0].formulastrength,
              style: TextStyle(
                fontSize: 16.0,
                fontWeight: FontWeight.bold,
                letterSpacing: 0.5,
                fontFamily: 'Open-Sans',
              )),
          backgroundColor: new Color(0xff17AF6B).withOpacity(1.0)),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Container(
                margin: EdgeInsets.fromLTRB(15.0, 10.0, 0.0, 0.0),
                child: Text(
                    medicineDetailsResponse.data[0].name +
                        " - " +
                        medicineDetailsResponse.data[0].formulastrength,
                    style: TextStyle(
                        fontSize: 18,
                        fontFamily: 'Open-Sans',
                        fontWeight: FontWeight.bold,
                        letterSpacing: 1.0,
                        color: Colors.black)),
              ),
              Card(
                  margin: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
                  shape: RoundedRectangleBorder(
                    side: BorderSide(color: Colors.white70, width: 1),
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 10.0),
                        child: Table(
                          children: [
                            TableRow(children: [
                              TableCell(child: _getHeadingText("Imprint")),
                              TableCell(child: _getHeadingText("Strength")),
                            ]),
                            TableRow(children: [
                              TableCell(
                                  child: Container(
                                      child: _getNormalText(
                                          medicineDetailsResponse
                                              .data[0].imprint))),
                              TableCell(
                                  child: _getNormalText(medicineDetailsResponse
                                      .data[0].formulastrength)),
                            ]),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 10.0),
                        child: Table(
                          children: [
                            TableRow(children: [
                              TableCell(child: _getHeadingText("Color")),
                              TableCell(child: _getHeadingText("Size")),
                            ]),
                            TableRow(children: [
                              TableCell(
                                  child: Container(
                                      child: _getNormalText(
                                          medicineDetailsResponse
                                              .data[0].color))),
                              TableCell(
                                  child: _getNormalText(
                                      medicineDetailsResponse.data[0].size)),
                            ]),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 10.0),
                        child: Table(
                          children: [
                            TableRow(children: [
                              TableCell(child: _getHeadingText("Shape")),
                              TableCell(child: _getHeadingText("Availability")),
                            ]),
                            TableRow(children: [
                              TableCell(
                                  child: Container(
                                      child: _getNormalText(
                                          medicineDetailsResponse
                                              .data[0].shape))),
                              TableCell(
                                  child: _getNormalText(medicineDetailsResponse
                                      .data[0].availability)),
                            ]),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 20.0),
                        child: Table(
                          border: TableBorder(
                              bottom: BorderSide(
                                  color: Colors.grey[400], width: 1.0)),
                          children: [
                            TableRow(children: [
                              TableCell(child: _getHeadingText("Drug Class"))
                            ]),
                            TableRow(children: [
                              TableCell(
                                child: Container(
                                    margin: EdgeInsets.only(bottom: 10.0),
                                    child: _getNormalText(
                                        medicineDetailsResponse
                                            .data[0].drugClass)),
                              ),
                            ]),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 20.0),
                        child: Table(
                          border: TableBorder(
                              bottom: BorderSide(
                                  color: Colors.grey[400], width: 1.0)),
                          children: [
                            TableRow(children: [
                              TableCell(
                                  child: _getHeadingText("Pregnancy Category"))
                            ]),
                            TableRow(children: [
                              TableCell(
                                child: Container(
                                    margin: EdgeInsets.only(bottom: 10.0),
                                    child: _getNormalText(
                                        medicineDetailsResponse
                                            .data[0].pregnancyCategory)),
                              ),
                            ]),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 20.0),
                        child: Table(
                          border: TableBorder(
                              bottom: BorderSide(
                                  color: Colors.grey[400], width: 1.0)),
                          children: [
                            TableRow(children: [
                              TableCell(child: _getHeadingText("CSA Schedule"))
                            ]),
                            TableRow(children: [
                              TableCell(
                                child: Container(
                                    margin: EdgeInsets.only(bottom: 10.0),
                                    child: _getNormalText(
                                        medicineDetailsResponse
                                            .data[0].csaSchedule)),
                              ),
                            ]),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 20.0),
                        child: Table(
                          border: TableBorder(
                              bottom: BorderSide(
                                  color: Colors.grey[400], width: 1.0)),
                          children: [
                            TableRow(children: [
                              TableCell(
                                  child: _getHeadingText("Labeler / Supplier"))
                            ]),
                            TableRow(children: [
                              TableCell(
                                child: Container(
                                    margin: EdgeInsets.only(bottom: 10.0),
                                    child: _getNormalText(
                                        medicineDetailsResponse
                                            .data[0].supplier)),
                              ),
                            ]),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 20.0),
                        child: Table(
                          children: [
                            TableRow(children: [
                              TableCell(
                                  child: _getHeadingText(
                                      "National Drug Code (NDC)"))
                            ]),
                            TableRow(children: [
                              TableCell(
                                child: Container(
                                    child: _getNormalText(
                                        medicineDetailsResponse.data[0].ndc)),
                              ),
                            ]),
                          ],
                        ),
                      ),
                    ],
                  )),
            ],
          ),
        ),
      ),
    );
  }

  _getHeadingText(text) {
    return Text(text,
        style: TextStyle(
            fontSize: 13,
            fontFamily: 'Open-Sans',
            fontWeight: FontWeight.bold,
            letterSpacing: 0.5,
            color: Colors.black));
  }

  _getNormalText(text) {
    return Text(text,
        style: TextStyle(
            fontSize: 13,
            fontFamily: 'Open-Sans',
            letterSpacing: 0.5,
            color: Colors.black));
  }
}
