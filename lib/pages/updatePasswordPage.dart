import 'package:MedZone/services/CommonServices.dart';
import 'package:MedZone/services/Covid-Services.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

class UpdatePassword extends StatefulWidget {
  final String userEmail;

  UpdatePassword({@required this.userEmail});

  @override
  _UpdatePasswordState createState() => _UpdatePasswordState();
}

class _UpdatePasswordState extends State<UpdatePassword> {
  final myPasswordController = TextEditingController();
  final myConfirmPasswordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text("Update Password",
              style: TextStyle(
                fontSize: 16.0,
                fontWeight: FontWeight.bold,
                letterSpacing: 0.5,
                fontFamily: 'Open-Sans',
              )),
          backgroundColor: new Color(0xff17AF6B).withOpacity(1.0)),
      body: Container(
        height: MediaQuery.of(context).size.height,
        margin: EdgeInsets.all(20.0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: [
              Container(
                margin: EdgeInsets.only(top: 10.0),
                child: Text("Enter your new password below:",
                    style: TextStyle(fontSize: 16.0, letterSpacing: 0.4)),
              ),
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
                    width: MediaQuery.of(context).size.width * 0.9,
                    height: 50.0,
                    child: Row(
                      children: <Widget>[
                        new Flexible(
                          child: new TextField(
                            keyboardType: TextInputType.visiblePassword,
                            obscureText: true,
                            textInputAction: TextInputAction.next,
                            controller: myPasswordController,
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              focusedBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.green, width: 2.0),
                              ),
                              labelText: "Password",
                              labelStyle: TextStyle(color: Colors.grey),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
                    width: MediaQuery.of(context).size.width * 0.9,
                    height: 50.0,
                    child: Row(
                      children: <Widget>[
                        new Flexible(
                          child: new TextField(
                            keyboardType: TextInputType.visiblePassword,
                            obscureText: true,
                            textInputAction: TextInputAction.done,
                            onSubmitted: (_) {
                              updatePassword();
                            },
                            controller: myConfirmPasswordController,
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              focusedBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.green, width: 2.0),
                              ),
                              labelText: "Confirm Password",
                              labelStyle: TextStyle(color: Colors.grey),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.only(top: 30.0, bottom: 5.0),
                child: Text("The password must follow below guidelines:",
                    style: TextStyle(
                        fontSize: 14.0,
                        letterSpacing: 0.4,
                        fontWeight: FontWeight.bold)),
              ),
              Container(
                margin: EdgeInsets.only(top: 10.0),
                child: Text("• Has at least 8 characters (no spaces)",
                    style: TextStyle(
                        fontSize: 14.0,
                        letterSpacing: 0.4,
                        fontWeight: FontWeight.bold)),
              ),
              Container(
                margin: EdgeInsets.only(top: 0.0),
                child: Text("• Must be a combination of letters & numbers",
                    style: TextStyle(
                        fontSize: 14.0,
                        letterSpacing: 0.4,
                        fontWeight: FontWeight.bold)),
              ),
              Container(
                margin: EdgeInsets.only(top: 0.0),
                child: Text("• Must contain at least 1 uppercase letter",
                    style: TextStyle(
                        fontSize: 14.0,
                        letterSpacing: 0.4,
                        fontWeight: FontWeight.bold)),
              ),
              Container(
                margin: EdgeInsets.only(top: 0.0),
                child: Text("• Must contain at least 1 special character",
                    style: TextStyle(
                        fontSize: 14.0,
                        letterSpacing: 0.4,
                        fontWeight: FontWeight.bold)),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(0.0, 40.0, 0.0, 0.0),
                height: 50.0,
                width: MediaQuery.of(context).size.width * 0.9,
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0),
                      side: BorderSide(
                          color: new Color(0xffFF2B56).withOpacity(1.0))),
                  color: new Color(0xffFF2B56).withOpacity(1.0),
                  onPressed: () {
                    updatePassword();
                  },
                  child: Column(
                    children: <Widget>[
                      Container(
                        transform: Matrix4.translationValues(0.0, 5.0, 0.0),
                        margin: EdgeInsets.only(top: 7.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text('Submit',
                                overflow: TextOverflow.fade,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16.5,
                                    letterSpacing: 0.8)),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  updatePassword() {
    if (myPasswordController.text != myConfirmPasswordController.text) {
      CommonService.showToast(
          context, "Password & confirm password do not match.");
    } else if (!isPasswordCompliant(myPasswordController.text)) {
      CommonService.showToast(context, "Please enter valid password");
    } else {
      callUpdatePasswordAPI();
    }
  }

  callUpdatePasswordAPI() {
    APIService service = new APIService();
    EasyLoading.show(
        status: CommonService.getLoadingMessage(),
        maskType: EasyLoadingMaskType.black);
    service
        .updatePassword(widget.userEmail, myPasswordController.text, context)
        .then((res) {
      setState(() {
        print(res);
        EasyLoading.dismiss();
        if (res.containsKey("success") && res['success'] == true) {
          CommonService.showToast(context, res['message']);
          Navigator.pop(context);
          Navigator.pop(context);
        } else {
          CommonService.showToast(context, res['message']);
        }
      });
    }).catchError((err) {
      EasyLoading.dismiss();
      print(err);
    });
  }

  bool isPasswordCompliant(String password, [int minLength = 8]) {
    if (password == null || password.isEmpty) {
      return false;
    }

    bool hasUppercase = password.contains(new RegExp(r'[A-Z]'));
    bool hasDigits = password.contains(new RegExp(r'[0-9]'));
    bool hasLowercase = password.contains(new RegExp(r'[a-z]'));
    bool hasSpecialCharacters =
        password.contains(new RegExp(r'[!@#$%^&*(),.?":{}|<>]'));
    bool hasMinLength = password.length > minLength;

    return hasDigits &
        hasUppercase &
        hasLowercase &
        hasSpecialCharacters &
        hasMinLength;
  }
}
