import 'package:MedZone/custom_widget/card_home.dart';
import 'package:MedZone/models/response/HomeDataResponse.dart';
import 'package:MedZone/services/CommonServices.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'SearchMedicine.dart';

class AllProviders extends StatefulWidget {
  Count count;

  AllProviders({@required this.count});

  @override
  _AllProvidersState createState() => _AllProvidersState();
}

class _AllProvidersState extends State<AllProviders> {
  var currentLat;
  var currentLong;

  @override
  void initState() {
    CommonService.getPrefsDouble(CommonService.currentLat, false).then((value) {
      currentLat = value;
    });

    CommonService.getPrefsDouble(CommonService.currentLong, false)
        .then((value) {
      currentLong = value;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: new Color(0xffF1F2F2).withOpacity(1.0),
      appBar: AppBar(
        elevation: 0.0,
        title: Text("allHealthCare",
            style: TextStyle(
              fontSize: 16.0,
              fontWeight: FontWeight.bold,
              letterSpacing: 0.5,
              fontFamily: 'Open-Sans',
            )).tr(),
        backgroundColor: new Color(0xff17AF6B).withOpacity(1.0),
      ),
      body: Container(
        padding: EdgeInsets.fromLTRB(20, 10, 16, 16),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              new Color(0xff17AF6B).withOpacity(1.0),
              new Color(0xffF1F2F2).withOpacity(1.0),
            ],
            stops: [0.2, 0.5],
          ),
        ),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      child: CardHome(
                          count: widget.count.pharmacy.toString(),
                          image: "assets/images/pharmacy_icon.svg",
                          name: "pharmacy",
                          currentLat: currentLat,
                          currentLong: currentLong,
                          type: "pharmacies"),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      child: CardHome(
                          count: widget.count.urgentCare.toString(),
                          image: "assets/images/urgent_care_icon.svg",
                          name: "urgentCare",
                          currentLat: currentLat,
                          currentLong: currentLong,
                          type: "urgent_cares"),
                    ),
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      child: CardHome(
                          count: widget.count.hospital.toString(),
                          image: "assets/images/hospital_icon.svg",
                          name: "hospital",
                          currentLat: currentLat,
                          currentLong: currentLong,
                          type: "hospitals"),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      child: CardHome(
                          count: widget.count.harmReduction.toString(),
                          image: "assets/images/harm_reduction_icon.svg",
                          name: "harmReduction",
                          currentLat: currentLat,
                          currentLong: currentLong,
                          type: "harm_reductions"),
                    ),
                  ),
                ],
              ),
              Row(children: <Widget>[
                Expanded(
                  child: Container(
                    margin: EdgeInsets.fromLTRB(10.0, 10.0, 0.0, 0.0),
                    height: 60.0,
                    width: MediaQuery.of(context).size.width * 0.9,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          side: BorderSide(
                              color: new Color(0xffFF2B56).withOpacity(1.0))),
                      color: new Color(0xffFF2B56).withOpacity(1.0),
                      onPressed: () {
                        navigateToSearchMedicine(context);
                      },
                      child: Column(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(top: 8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text('searchMed',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20.0,
                                            letterSpacing: 0.8))
                                    .tr(),
                                Container(
                                  transform:
                                      Matrix4.translationValues(0.0, 7.0, 0.0),
                                  child: SvgPicture.asset(
                                      "assets/images/search_medicine.svg",
                                      width: 25.0,
                                      height: 25.0,
                                      fit: BoxFit.contain),
                                ),
                              ],
                            ),
                          ),
                          Row(
                            children: <Widget>[
                              Container(
                                child: Text('inStoreNearYou',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 14.0,
                                            letterSpacing: 0.1))
                                    .tr(),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ]),
              Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      child: CardHome(
                          count: widget.count.laboratory.toString(),
                          image: "assets/images/laboratory_icon.svg",
                          name: "lab",
                          currentLat: currentLat,
                          currentLong: currentLong,
                          type: "laboratories"),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      child: CardHome(
                          count: widget.count.radiologyCentre.toString(),
                          image: "assets/images/radiology_icon.svg",
                          name: "rad",
                          currentLat: currentLat,
                          currentLong: currentLong,
                          type: "radiology_centres"),
                    ),
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      child: CardHome(
                          count: widget.count.chiropractory.toString(),
                          image: "assets/images/chiroparactor_icon.svg",
                          name: "chiropractor",
                          currentLat: currentLat,
                          currentLong: currentLong,
                          type: "chiropractors"),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      child: CardHome(
                          count: widget.count.physicalTherapy.toString(),
                          image: "assets/images/physical-therapy_icon.svg",
                          name: "physicalTherapy",
                          currentLat: currentLat,
                          currentLong: currentLong,
                          type: "physical_therapies"),
                    ),
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      child: CardHome(
                          count: widget.count.veterinarian.toString(),
                          image: "assets/images/veterinary_icon.svg",
                          name: "veterinarian",
                          currentLat: currentLat,
                          currentLong: currentLong,
                          type: "veterinarians"),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      child: CardHome(
                          count: widget.count.ophthalmologist.toString(),
                          image: "assets/images/eye_clinic_icon.svg",
                          name: "eyecare",
                          currentLat: currentLat,
                          currentLong: currentLong,
                          type: "ophthalamologists"),
                    ),
                  ),
                ],
              ),
              Container(
                height: 20.0,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future navigateToSearchMedicine(context) async {
    await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => searchMedicine()),
    );
  }
}
