import 'package:MedZone/custom_widget/medicine_card.dart';
import 'package:MedZone/custom_widget/medicine_pharmacies.dart';
import 'package:MedZone/custom_widget/top_findings_card.dart';
import 'package:MedZone/models/TopFindingsModel.dart';
import 'package:MedZone/models/response/HomeDataResponse.dart';
import 'package:MedZone/models/response/TopMedicinesResponse.dart';
import 'package:MedZone/services/CommonServices.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';

class MedicineSearchResults extends StatefulWidget {
  medData data;
  List<Pharmacy> pharmaciesList;
  var currentLat;
  var currentLong;

  MedicineSearchResults({@required this.data,
    this.pharmaciesList,
    this.currentLat,
    this.currentLong});

  @override
  _MedicineSearchResultsState createState() => _MedicineSearchResultsState();
}

class _MedicineSearchResultsState extends State<MedicineSearchResults> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
          title: Text(widget.data.name + ' - ' + widget.data.formulastrength,
              style: TextStyle(
                fontSize: 16.0,
                fontWeight: FontWeight.bold,
                letterSpacing: 0.5,
                fontFamily: 'Open-Sans',
              )),
          backgroundColor: new Color(0xff17AF6B).withOpacity(1.0)),
      body: Container(
        color: new Color(0xffF1F2F2).withOpacity(1.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Container(
              margin: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 0.0),
              child: MedicineCard(
                  widget.data, 'assets/images/info_icon.svg', 'no'),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(25.0, 20.0, 0.0, 0.0),
              child: Text("availableIn",
                  style: TextStyle(
                      fontSize: 18,
                      fontFamily: 'Open-Sans',
                      fontWeight: FontWeight.bold,
                      letterSpacing: 1.0,
                      color: Colors.black))
                  .tr(),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 0.0),
              height: MediaQuery
                  .of(context)
                  .size
                  .height * 0.67,
              child: ListView.builder(
                itemCount: widget.pharmaciesList.length,
                itemBuilder: (context, index) {
                  return _getTopFindingsCurrent(index);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  _getTopFindingsCurrent(index) {
    var obj = new topFindings();

    return MedicinePharmacies(
        id: widget.pharmaciesList[index].id,
        type: "pharmacies",
        address: widget.pharmaciesList[index].address1,
        image: widget.pharmaciesList[index].logo,
        name: widget.pharmaciesList[index].name,
        distance: widget.pharmaciesList[index].distance,
        days: CommonService.getDays(widget.pharmaciesList[index].days),
        timing: CommonService.getTimings(widget.pharmaciesList[index].hours),
        deliveryTiming: obj.getTopFindingsListPhar()[index].deliveryTiming,
        contactNo: widget.pharmaciesList[index].phoneNumber,
        webURL: obj.getTopFindingsListPhar()[index].webURL,
        latitude: widget.pharmaciesList[index].latlong.split(",")[0],
        longitude: widget.pharmaciesList[index].latlong.split(",")[1],
        rating: widget.pharmaciesList[index].rating.toDouble(),
        currentLatitude: widget.currentLat,
        currentLongitude: widget.currentLong,
        packages: widget.pharmaciesList[index].packages);
  }
}