import 'package:MedZone/main.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

class NoInternet extends StatefulWidget {
  @override
  _NoInternetState createState() => _NoInternetState();
}

class _NoInternetState extends State<NoInternet> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/grey_bg.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Center(
              child: SvgPicture.asset('assets/images/no_network_icon.svg',
                  width: 120.0, height: 120.0),
            ),
            Container(
                margin: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 20.0),
                child: Text("noInternet",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: new Color(0xff17AF6B).withOpacity(1.0),
                            fontSize: 20,
                            fontFamily: 'Open-Sans-bold',
                            fontWeight: FontWeight.bold))
                    .tr()),
            Container(
              margin: EdgeInsets.fromLTRB(40.0, 20.0, 40.0, 20.0),
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    side: BorderSide(
                        color: new Color(0xff17AF6B).withOpacity(1.0))),
                color: new Color(0xff17AF6B).withOpacity(1.0),
                onPressed: () {
                  navigateToHomePage(context);
                },
                child: Container(
                  margin: EdgeInsets.only(top: 0.0, left: 25.0),
                  height: 40.0,
                  child: Row(
//                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        child: SvgPicture.asset(
                            "assets/images/refresh_icon.svg",
                            width: 20.0,
                            height: 20.0,
                            fit: BoxFit.contain),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 30.0),
                        child: Text('clickRefresh',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18.0,
                                    letterSpacing: 0.8))
                            .tr(),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future navigateToHomePage(context) async {
    print("fun Called");

    print(Provider.of<DataConnectionStatus>(context));

    if (Provider.of<DataConnectionStatus>(context) !=
        DataConnectionStatus.disconnected) {
      await Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => MainPage()),
      );
    } else {}
  }
}
