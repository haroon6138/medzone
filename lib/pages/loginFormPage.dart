import 'package:MedZone/config/config.dart';
import 'package:MedZone/models/AuthUserProvider.dart';
import 'package:MedZone/models/response/AuthenticatedUser.dart';
import 'package:MedZone/pages/forgotPasswordPage.dart';
import 'package:MedZone/services/CommonServices.dart';
import 'package:MedZone/services/Covid-Services.dart';
import 'package:MedZone/utils/transitions/fade_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';
import 'dart:convert';

class LoginFormPage extends StatefulWidget {
  bool isAppointmentLogin;

  LoginFormPage({@required this.isAppointmentLogin});

  @override
  _LoginFormPageState createState() => _LoginFormPageState();
}

class _LoginFormPageState extends State<LoginFormPage> {
  final myEmailController = TextEditingController();
  final myPasswordController = TextEditingController();

  AuthUserProvider authUserProvider;

  bool checkedValue = false;

  @override
  void initState() {
    setEmailPassword();
    super.initState();
  }

  void setEmailPassword() async {
    if (await CommonService.getPrefs(USER_EMAIL, false) != null) {
      myEmailController.text = await CommonService.getPrefs(USER_EMAIL, false);
      myPasswordController.text = await CommonService.getPrefs(USER_PASSWORD, false);
      checkedValue = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    authUserProvider = Provider.of<AuthUserProvider>(context);
    final node = FocusScope.of(context);
    return Scaffold(
      appBar: AppBar(
          title: Text("Login",
              style: TextStyle(
                fontSize: 16.0,
                fontWeight: FontWeight.bold,
                letterSpacing: 0.5,
                fontFamily: 'Open-Sans',
              )),
          backgroundColor: new Color(0xff17AF6B).withOpacity(1.0)),
      body: Container(
          margin: EdgeInsets.all(20.0),
          child: SingleChildScrollView(
            child: Container(
                height: MediaQuery.of(context).size.height,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Text("Welcome to MedZone", style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.bold, fontFamily: "Open-Sans-Bold", letterSpacing: 0.6)),
                    Container(
                      margin: EdgeInsets.only(top: 20.0),
                      child: Text("Login to see and manage your appointments, favourite doctors & much more", style: TextStyle(fontSize: 16.0, letterSpacing: 0.4)),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 20.0),
                      child: Text("Enter your details below to login", style: TextStyle(fontSize: 16.0, letterSpacing: 0.4)),
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
                          width: MediaQuery.of(context).size.width * 0.89,
                          height: 80.0,
                          child: Row(
                            children: <Widget>[
                              new Flexible(
                                child: new TextField(
                                  textInputAction: TextInputAction.next,
                                  onEditingComplete: () => node.nextFocus(),
                                  controller: myEmailController,
                                  decoration: const InputDecoration(
                                    border: OutlineInputBorder(),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.green, width: 2.0),
                                    ),
                                    labelText: "Email Address",
                                    labelStyle: TextStyle(color: Colors.grey),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
                          width: MediaQuery.of(context).size.width * 0.89,
                          height: 50.0,
                          child: Row(
                            children: <Widget>[
                              new Flexible(
                                child: new TextField(
                                  keyboardType: TextInputType.visiblePassword,
                                  obscureText: true,
                                  textInputAction: TextInputAction.done,
                                  onSubmitted: (_) {
                                    callLoginAPI();
                                  },
                                  controller: myPasswordController,
                                  decoration: const InputDecoration(
                                    border: OutlineInputBorder(),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.green, width: 2.0),
                                    ),
                                    labelText: "Password",
                                    labelStyle: TextStyle(color: Colors.grey),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: Transform.translate(
                        offset: const Offset(-10, 0),
                        child: Row(
                          children: [
                            Checkbox(
                              activeColor: new Color(0xff17AF6B).withOpacity(1.0),
                              value: checkedValue,
                              onChanged: (value) {
                                setState(() {
                                  checkedValue = !checkedValue;
                                });
                              },
                            ),
                            Text('Remember me'),
                          ],
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        navigateToForgotPassword();
                      },
                      child: Container(
                        alignment: Alignment.center,
                        margin: EdgeInsets.only(top: 20.0),
                        child: Text("Forgot Password?", style: TextStyle(fontSize: 16.0, letterSpacing: 0.4, color: new Color(0xffFF2B56).withOpacity(1.0), fontWeight: FontWeight.bold, decoration: TextDecoration.underline)),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(0.0, 40.0, 0.0, 0.0),
                      height: 50.0,
                      width: MediaQuery.of(context).size.width * 0.89,
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0), side: BorderSide(color: new Color(0xffFF2B56).withOpacity(1.0))),
                        color: new Color(0xffFF2B56).withOpacity(1.0),
                        onPressed: () {
                          callLoginAPI();
                        },
                        child: Column(
                          children: <Widget>[
                            Container(
                              transform: Matrix4.translationValues(0.0, 5.0, 0.0),
                              margin: EdgeInsets.only(top: 7.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text('Login', overflow: TextOverflow.fade, style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 16.5, letterSpacing: 0.8)),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                )),
          )),
    );
  }

  navigateToForgotPassword() async {
    await Navigator.push(context, FadePage(page: ForgotPasswordPage()));
  }

  callLoginAPI() async {
    APIService service = new APIService();
    String fcmToken = await CommonService.getPrefs(FIREBASE_TOKEN, false);
    AuthenticatedUser authenticatedUser = new AuthenticatedUser();
    EasyLoading.show(status: CommonService.getLoadingMessage(), maskType: EasyLoadingMaskType.black);
    service.loginUser(myEmailController.text.trim(), myPasswordController.text.trim(), fcmToken, context).then((res) {
      authenticatedUser = AuthenticatedUser.fromJson(res);
      CommonService.setPrefs(AUTHENTICATED_USER, json.encode(res));
      CommonService.setPrefs(JWT_TOKEN, authenticatedUser.token);

      if (checkedValue) {
        CommonService.setPrefs(USER_EMAIL, myEmailController.text.trim());
        CommonService.setPrefs(USER_PASSWORD, myPasswordController.text.trim());
      }

      authUserProvider.setAuthUser(authenticatedUser);
      authUserProvider.setIsLogin(true);
      EasyLoading.dismiss();
      CommonService.showToast(context, "Successfully Logged In");
      if (widget.isAppointmentLogin) {
        Navigator.of(context).pop();
      } else {
        Navigator.popUntil(context, ModalRoute.withName('/'));
      }
    }).catchError((err) {
      EasyLoading.dismiss();
      print(err);
      CommonService.showToast(context, "Invalid email or password");
    });
  }
}
