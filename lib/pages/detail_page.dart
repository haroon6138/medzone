import 'package:MedZone/custom_widget/ReviewWidget.dart';
import 'package:MedZone/custom_widget/circle_image.dart';
import 'package:MedZone/custom_widget/confirm_dialog.dart';
import 'package:MedZone/custom_widget/write_review.dart';
import 'package:MedZone/models/TopFindingsModel.dart';
import 'package:MedZone/services/CommonServices.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_phone_direct_caller/flutter_phone_direct_caller.dart';
import 'package:flutter_svg/svg.dart';
import 'package:rating_bar/rating_bar.dart';
import 'package:url_launcher/url_launcher.dart';

import 'SearchMedicine.dart';

final imageSize = 200.0;

class DetailPage extends StatelessWidget {
  DetailPage(this.topFindingsObj);

  final topFindings topFindingsObj;

  @override
  Widget build(BuildContext context) {
    return detailsPage(topFindingsObj);
  }
}

class detailsPage extends StatefulWidget {
  detailsPage(this.topFindingsObj);

  final topFindings topFindingsObj;
  var currentLat;
  var currentLong;

  List<String> catListName = [
    "SERVICES",
    "ACCEPTED INSURANCES"
  ];

//  List<String> catList = ["types", "services", "photos", "acceptedInsurances"];

  List<String> catList = ["services", "acceptedInsurances"];

//  List<String> typesList = ["Compounding", "No Fault", "Retail"];
  List<String> servicesList = [
    "Blood Pressure Screening",
    "Diabetes Screening"
  ];

//  List<String> photosList = ["Digital Photos", "Print to Print"];
  List<String> acceptedInsuranceList = [
    "Blue Cross Shield",
    "CVS/Caremark",
    "Express Scripts"
  ];

  var currentSelectedText = "SERVICES";

  @override
  _detailsPageState createState() => _detailsPageState();
}

class _detailsPageState extends State<detailsPage> {
  @override
  void initState() {
    CommonService.getPrefsDouble(CommonService.currentLat, false).then((value) {
      widget.currentLat = value;
    });

    CommonService.getPrefsDouble(CommonService.currentLong, false)
        .then((value) {
      widget.currentLong = value;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: new Color(0xffF1F2F2).withOpacity(1.0),
      appBar: AppBar(
        title: Text(widget.topFindingsObj.name,
            style: TextStyle(
              fontSize: 16.0,
              fontWeight: FontWeight.bold,
              letterSpacing: 0.5,
              fontFamily: 'Open-Sans',
            )),
        backgroundColor: new Color(0xff17AF6B).withOpacity(1.0),
      ),
      bottomNavigationBar: Padding(
        padding: EdgeInsets.all(0.0),
        child: Container(
          height: 70.0,
          child: RaisedButton(
            padding: const EdgeInsets.all(0.0),
            textColor: Colors.white,
            color: new Color(0xff17AF6B).withOpacity(1.0),
            onPressed: () {
              var url = 'https://www.google.com/maps/dir/?api=1&destination=' +
                  widget.topFindingsObj.latitude.toString() +
                  ',' +
                  widget.topFindingsObj.longitude.toString() +
                  '&origin=' +
                  widget.currentLat.toString() +
                  ',' +
                  widget.currentLong.toString() +
                  '&travelmode=driving';
              launch(url);
            },
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Image.asset("assets/images/location_pin_white.png",
                    width: 30.0, height: 30.0),
                Container(
                  margin: EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 5.0),
                  child: Text("getDirections",
                          style: TextStyle(
                              fontSize: 18,
                              color: Colors.white,
                              letterSpacing: 0.5,
                              fontFamily: 'Open-Sans-Bold',
                              fontWeight: FontWeight.bold))
                      .tr(),
                ),
              ],
            ),
          ),
        ),
      ),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width,
                            height: MediaQuery.of(context).size.height / 4,
                            child: FittedBox(
                                child: widget.topFindingsObj.featuredImage
                                        .contains("medzone_cover")
                                    ? Image.asset(
                                        widget.topFindingsObj.featuredImage,
                                        fit: BoxFit.fill)
                                    : Image.network(
                                        widget.topFindingsObj.featuredImage,
                                        fit: BoxFit.fill)),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Stack(
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width / 0.5,
//                        height: 200.0,
                            margin: EdgeInsets.fromLTRB(10.0, 160.0, 10.0, 0.0),
                            child: Card(
                              shape: RoundedRectangleBorder(
                                side:
                                    BorderSide(color: Colors.white70, width: 1),
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: Container(
                                margin: EdgeInsets.only(top: 70.0),
                                child: Column(
                                  children: <Widget>[
                                    Row(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Text(
                                          widget.topFindingsObj.name,
                                          style: TextStyle(
                                            fontSize: 18.0,
                                            fontWeight: FontWeight.bold,
                                            fontFamily: 'Open-Sans-Bold',
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Container(
                                          margin: EdgeInsets.only(top: 5.0),
                                          child: Text(
                                            widget.topFindingsObj.address,
                                            style: TextStyle(
                                              fontSize: 13.0,
                                              color: Colors.grey[600],
                                              fontFamily: 'Open-Sans-Light',
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Container(
                                          margin: EdgeInsets.only(top: 5.0),
                                          child: RatingBar.readOnly(
                                            size: 28.0,
                                            filledColor: Colors.amber,
                                            emptyColor: Colors.amber,
                                            halfFilledColor: Colors.amber,
                                            initialRating:
                                                widget.topFindingsObj.rating,
                                            isHalfAllowed: true,
                                            halfFilledIcon: Icons.star_half,
                                            filledIcon: Icons.star,
                                            emptyIcon: Icons.star_border,
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Container(
                                          margin: EdgeInsets.only(
                                              top: 10.0, bottom: 10.0),
                                          child: SvgPicture.asset(
                                              'assets/images/map_pin_icon_green.svg',
                                              height: 20.0,
                                              width: 20.0),
                                        ),
                                        Container(
                                          margin: EdgeInsets.fromLTRB(
                                              20.0, 5.0, 0.0, 0.0),
                                          child: Text(
                                            "milesAway",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                                fontSize: 14,
                                                color: new Color(0xff17AF6B)
                                                    .withOpacity(1.0),
                                                letterSpacing: 0.1,
                                                fontFamily: 'Open-Sans-Black',
                                                fontWeight: FontWeight.bold),
                                          ).tr(args: [
                                            widget.topFindingsObj.distance
                                          ]),
                                        ),
                                      ],
                                    ),
                                    (widget.topFindingsObj.type ==
                                                "hospitals" ||
                                            widget.topFindingsObj.type ==
                                                "urgent_cares")
                                        ? Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisSize: MainAxisSize.min,
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: <Widget>[
                                              Column(
                                                children: <Widget>[
                                                  Container(
                                                      padding:
                                                          EdgeInsets.fromLTRB(
                                                              10.0,
                                                              10.0,
                                                              0.0,
                                                              0.0),
                                                      child: Text(
                                                          "Waiting Time: ",
                                                          style: TextStyle(
                                                              fontSize: 14,
                                                              color: new Color(
                                                                      0xff17AF6B)
                                                                  .withOpacity(
                                                                      1.0),
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                              letterSpacing:
                                                                  0.1,
                                                              fontFamily:
                                                                  'Open-Sans-bold'))),
                                                ],
                                              ),
                                              Column(
                                                children: <Widget>[
                                                  Container(
                                                    padding:
                                                        EdgeInsets.fromLTRB(
                                                            0, 10.0, 0.0, 0.0),
                                                    child: Text(
                                                      widget.topFindingsObj
                                                                  .waitingTime !=
                                                              null
                                                          ? widget.topFindingsObj
                                                                  .waitingTime +
                                                              " mins"
                                                          : "N/A",
                                                      textAlign:
                                                          TextAlign.start,
                                                      style: TextStyle(
                                                          fontSize: 14,
                                                          color: new Color(
                                                                  0xff17AF6B)
                                                              .withOpacity(1.0),
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          letterSpacing: 0.1,
                                                          fontFamily:
                                                              'Open-Sans-bold'),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          )
                                        : Container(),
                                    Divider(
                                      color: Colors.grey[300],
                                    ),
                                    Row(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: <Widget>[
                                                Container(
                                                  margin: EdgeInsets.only(
                                                      top: 10.0,
                                                      bottom: 9.0,
                                                      left: 11.0),
                                                  child: Column(
                                                    children: <Widget>[
                                                      SvgPicture.asset(
                                                          'assets/images/globe.svg',
                                                          height: 20.0,
                                                          width: 20.0),
                                                    ],
                                                  ),
                                                ),
                                                GestureDetector(
                                                  onTap: () {
                                                    launch(widget
                                                        .topFindingsObj.webURL);
                                                  },
                                                  child: Container(
                                                    width:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .width *
                                                            0.45,
                                                    margin: EdgeInsets.fromLTRB(
                                                        10.0, 0.0, 0.0, 0.0),
                                                    child: Text(
                                                      widget.topFindingsObj
                                                          .webURL,
                                                      textAlign:
                                                          TextAlign.start,
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                      style: TextStyle(
                                                          fontSize: 14,
                                                          color: new Color(
                                                                  0xff17AF6B)
                                                              .withOpacity(1.0),
                                                          letterSpacing: 0.1,
                                                          fontFamily:
                                                              'Open-Sans-Light',
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                        Row(
                                          children: [
                                            GestureDetector(
                                              onTap: () {
                                                _callNumber(widget
                                                    .topFindingsObj.contactNo);
                                              },
                                              child: Container(
                                                margin: EdgeInsets.fromLTRB(
                                                    0.0, 10.0, 20.0, 10.0),
                                                child: SvgPicture.asset(
                                                    'assets/images/telephone-call.svg',
                                                    height: 30.0,
                                                    width: 20.0),
                                              ),
                                            ),
                                            GestureDetector(
                                              onTap: () {
                                                _requestVideoCall(widget
                                                    .topFindingsObj.name);
                                              },
                                              child: Container(
                                                margin: EdgeInsets.fromLTRB(
                                                    0.0, 10.0, 20.0, 10.0),
                                                child: SvgPicture.asset(
                                                    'assets/images/video-camera.svg',
                                                    height: 30.0,
                                                    width: 20.0),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Container(
                                width: 150.0,
                                height: 160.0,
                                padding: EdgeInsets.only(bottom: 10.0),
                                margin: EdgeInsets.only(top: 50.0),
                                child: Card(
                                  margin:
                                      EdgeInsets.fromLTRB(8.0, 16.0, 8.0, 0.0),
                                  shape: RoundedRectangleBorder(
                                    side: BorderSide(
                                        color: Colors.white70, width: 1),
                                    borderRadius: BorderRadius.circular(90),
                                  ),
                                  child: CircleImage(
                                      image: widget.topFindingsObj.image,
                                      width: 100.0,
                                      height: 100.0),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
              widget.topFindingsObj.type == "pharmacies"
                  ? Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                          Container(
                            margin: EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                            height: 50.0,
                            width: MediaQuery.of(context).size.width * 0.9,
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                  side: BorderSide(
                                      color: new Color(0xffFF2B56)
                                          .withOpacity(1.0))),
                              color: new Color(0xffFF2B56).withOpacity(1.0),
                              onPressed: () {
                                navigateToSearchMedicine(context);
                              },
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  SvgPicture.asset(
                                      "assets/images/search_medicine.svg",
                                      width: 25.0,
                                      height: 25.0,
                                      fit: BoxFit.contain),
                                  Container(
                                    margin: EdgeInsets.fromLTRB(
                                        15.0, 0.0, 0.0, 0.0),
                                    child: Text('medLook',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 16.0,
                                                letterSpacing: 0.8))
                                        .tr(),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ])
                  : Container(),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.fromLTRB(10.0, 20.0, 10.0, 0.0),
                    width: MediaQuery.of(context).size.width * 0.9,
                    child: Card(
                      shape: RoundedRectangleBorder(
                          side: BorderSide(color: Colors.white70, width: 1),
                          borderRadius: BorderRadius.circular(10)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 0.0),
                            child: Text(
                              widget.topFindingsObj.type == "pharmacies" ? "pharmacyHours" : "pharmHours",
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                  fontSize: 12,
                                  color: new Color(0xff17AF6B).withOpacity(1.0),
                                  letterSpacing: 0.1,
                                  fontFamily: 'Open-Sans-Bold',
                                  fontWeight: FontWeight.bold),
                            ).tr(),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 0.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Column(
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        SvgPicture.asset(
                                            'assets/images/calender_green_icon.svg',
                                            height: 20.0,
                                            width: 20.0),
                                        Container(
                                          margin: EdgeInsets.all(10.0),
                                          child: Text(
                                            widget.topFindingsObj.days,
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                                fontSize: 12,
                                                color: Colors.black87,
                                                letterSpacing: 0.1,
                                                fontFamily: 'Open-Sans'),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                Column(
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        SvgPicture.asset(
                                            'assets/images/clock_green_icon.svg',
                                            height: 20.0,
                                            width: 20.0),
                                        Container(
                                          margin: EdgeInsets.all(10.0),
                                          child: Text(
                                            widget.topFindingsObj.timing,
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                                fontSize: 12,
                                                color: Colors.black87,
                                                letterSpacing: 0.1,
                                                fontFamily: 'Open-Sans'),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          widget.topFindingsObj.type == "pharmacies"
                              ? Divider(
                                  color: Colors.grey[300],
                                )
                              : Container(),
                          widget.topFindingsObj.type == "pharmacies"
                              ? Container(
                                  margin:
                                      EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
                                  child: Text(
                                    "storeHours",
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                        fontSize: 12,
                                        color: new Color(0xff17AF6B)
                                            .withOpacity(1.0),
                                        letterSpacing: 0.1,
                                        fontFamily: 'Open-Sans-Bold',
                                        fontWeight: FontWeight.bold),
                                  ).tr(),
                                )
                              : Container(),
                          widget.topFindingsObj.type == "pharmacies"
                              ? Container(
                                  margin: EdgeInsets.fromLTRB(
                                      10.0, 10.0, 10.0, 0.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Column(
                                        children: <Widget>[
                                          Row(
                                            children: <Widget>[
                                              SvgPicture.asset(
                                                  'assets/images/calender_green_icon.svg',
                                                  height: 20.0,
                                                  width: 20.0),
                                              Container(
                                                margin: EdgeInsets.fromLTRB(
                                                    10.0, 00.0, 10.0, 0.0),
                                                child: Text(
                                                  widget.topFindingsObj.days,
                                                  textAlign: TextAlign.start,
                                                  style: TextStyle(
                                                      fontSize: 12,
                                                      color: Colors.black87,
                                                      letterSpacing: 0.1,
                                                      fontFamily: 'Open-Sans'),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                      Column(
                                        children: <Widget>[
                                          Row(
                                            children: <Widget>[
                                              SvgPicture.asset(
                                                  'assets/images/clock_green_icon.svg',
                                                  height: 20.0,
                                                  width: 20.0),
                                              Container(
                                                margin: EdgeInsets.all(10.0),
                                                child: Text(
                                                  widget.topFindingsObj.timing,
                                                  textAlign: TextAlign.start,
                                                  style: TextStyle(
                                                      fontSize: 12,
                                                      color: Colors.black87,
                                                      letterSpacing: 0.1,
                                                      fontFamily: 'Open-Sans'),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                )
                              : Container(),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Container(
                          height: 50.0,
                          width: MediaQuery.of(context).size.width * 0.75,
                          padding: EdgeInsets.all(0.0),
//                          margin: EdgeInsets.only(left: 100.0),
                          child: ListView(
                              scrollDirection: Axis.horizontal,
                              children: _getListData())),
                    ],
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
//                  Column(
//                    children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width * 0.9,
                    margin: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 20.0),
                    child: _getCurrentTypesList(),
                  ),
                ],
              ),
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Container(
//                          height: 60.0,
                        margin: EdgeInsets.fromLTRB(20.0, 5.0, 0.0, 10.0),
                        padding: EdgeInsets.all(0.0),
                        child: widget.topFindingsObj.reviewsList.length != 0
                            ? Text(
                                "reviews",
                                style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black87,
                                    letterSpacing: 0.1,
                                    fontFamily: 'Open-Sans-Bold'),
                              ).tr()
                            : Container(),
                      ),
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Container(
//                        height: 60.0,
                        margin: EdgeInsets.fromLTRB(0.0, 0.0, 20.0, 10.0),
                        padding: EdgeInsets.all(0.0),
                        child: RaisedButton(
                          padding: const EdgeInsets.all(8.0),
                          elevation: 2.0,
                          textColor: Colors.white,
                          color: Colors.grey[600],
                          onPressed: () {},
                          child: GestureDetector(
                            onTap: () {
                              showGeneralDialog(
                                  barrierColor: Colors.black.withOpacity(0.5),
                                  transitionBuilder: (context, a1, a2, widget) {
                                    final curvedValue = Curves.easeInOutBack
                                            .transform(a1.value) -
                                        1.0;
                                    return Transform(
                                      transform: Matrix4.translationValues(
                                          0.0, curvedValue * 200, 0.0),
                                      child: Opacity(
                                        opacity: a1.value,
                                        child: WriteReview(
                                            topFindingsObj:
                                                this.widget.topFindingsObj),
                                      ),
                                    );
                                  },
                                  transitionDuration:
                                      Duration(milliseconds: 500),
                                  barrierDismissible: true,
                                  barrierLabel: '',
                                  context: context,
                                  pageBuilder:
                                      (context, animation1, animation2) {});
                            },
                            child: Row(
                              children: <Widget>[
                                Container(
                                  margin:
                                      EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
                                  child: SvgPicture.asset(
                                      'assets/images/plus_icon_White.svg'),
                                ),
                                new Text("addReviews").tr(),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
                    width: MediaQuery.of(context).size.width * 0.94,
                    child: widget.topFindingsObj.reviewsList.length != 0
                        ? Card(
                            shape: RoundedRectangleBorder(
                              side: BorderSide(color: Colors.white70, width: 1),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Container(
                              margin: EdgeInsets.only(bottom: 20.0, top: 5.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                      child: ReviewWidget(
                                          reviewsList: widget
                                              .topFindingsObj.reviewsList)),
                                ],
                              ),
                            ),
                          )
                        : Container(),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  _getCurrentTypesList() {
    switch (widget.currentSelectedText) {
//      case "TYPES":
//        return Card(
//          shape: RoundedRectangleBorder(
//              side: BorderSide(color: Colors.white70, width: 1),
//              borderRadius: BorderRadius.circular(10)),
//          child: Column(
//            children: List.generate(widget.typesList.length, (index) {
//              return Row(
//                children: <Widget>[
//                  Container(
//                    padding: EdgeInsets.all(10.0),
//                    child: SvgPicture.asset('assets/images/check_icon.svg',
//                        height: 20.0, width: 20.0),
//                  ),
//                  Container(
//                    padding: EdgeInsets.all(10.0),
//                    child: Text(widget.typesList[index]),
//                  ),
//                ],
//              );
//            }),
//          ),
//        );
//        break;

      case "SERVICES":
        return Card(
          shape: RoundedRectangleBorder(
              side: BorderSide(color: Colors.white70, width: 1),
              borderRadius: BorderRadius.circular(10)),
          child: Column(
            children:
                List.generate(widget.topFindingsObj.services.length, (index) {
              return Row(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(10.0),
                    child: SvgPicture.asset('assets/images/check_icon.svg',
                        height: 20.0, width: 20.0),
                  ),
                  Container(
                    padding: EdgeInsets.all(10.0),
                    child: Text(widget.topFindingsObj.services[index] != null ? widget.topFindingsObj.services[index] : ""),
                  ),
                ],
              );
            }),
          ),
        );
        break;

      case "ACCEPTED INSURANCES":
        return Card(
          shape: RoundedRectangleBorder(
              side: BorderSide(color: Colors.white70, width: 1),
              borderRadius: BorderRadius.circular(10)),
          child: Column(
            children:
                List.generate(widget.topFindingsObj.insurances.length, (index) {
              return Row(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(10.0),
                    child: SvgPicture.asset('assets/images/check_icon.svg',
                        height: 20.0, width: 20.0),
                  ),
                  Container(
                    padding: EdgeInsets.all(10.0),
                    child: Text(widget.topFindingsObj.insurances[index]),
                  ),
                ],
              );
            }),
          ),
        );
        break;

      default:
    }
  }

  _callNumber(String number) async {
    await FlutterPhoneDirectCaller.callNumber(number);
  }

  _requestVideoCall(String name) async {
    ConfirmDialog dialog = new ConfirmDialog(
        context: context,
        title: "Confirm",
        confirmMessage: "Are you sure you want to request a video call with $name?",
        okText: "Confirm",
        cancelText: "Cancel",
        okFunction: () => {Navigator.pop(context)},
        cancelFunction: () => {});
    dialog.showAlertDialog(context);
  }

  _captureTextClick(text) {
    widget.currentSelectedText = text;
  }

  _getListData() {
    List<Widget> widgets = [];
    for (int i = 0; i <= 1; i++) {
      widgets.add(
        Column(
          children: <Widget>[
            FlatButton(
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onPressed: () {
                  setState(() {
                    _captureTextClick(widget.catListName[i]);
                  });
                },
                color: Colors.transparent,
                padding: EdgeInsets.all(0.0),
                child: Column(
                  children: <Widget>[
                    Text(widget.catList[i],
                            style: widget.currentSelectedText ==
                                    widget.catListName[i]
                                ? TextStyle(
                                    fontSize: 14,
                                    fontFamily: 'Open-Sans',
                                    fontWeight: FontWeight.bold,
                                    color:
                                        new Color(0xff17AF6B).withOpacity(1.0))
                                : TextStyle(
                                    fontSize: 14,
                                    fontFamily: 'Open-Sans',
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black54))
                        .tr(),
                  ],
                )),
          ],
        ),
      );
    }
    return widgets;
  }

  Future navigateToSearchMedicine(context) async {
    await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => searchMedicine()),
    );
  }
}
