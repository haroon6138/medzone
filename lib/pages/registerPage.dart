import 'package:MedZone/pages/registerFormPage.dart';
import 'package:MedZone/utils/transitions/fade_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
// import 'package:sign_in_with_apple/sign_in_with_apple.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
          title: Text("Sign Up",
              style: TextStyle(
                fontSize: 16.0,
                fontWeight: FontWeight.bold,
                letterSpacing: 0.5,
                fontFamily: 'Open-Sans',
              )),
          backgroundColor: new Color(0xff17AF6B).withOpacity(1.0)),
      body: Container(
        margin: EdgeInsets.all(20.0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Welcome to MedZone",
                  style: TextStyle(
                      fontSize: 22.0,
                      fontWeight: FontWeight.bold,
                      fontFamily: "Open-Sans-Bold",
                      letterSpacing: 0.6)),
              Container(
                margin: EdgeInsets.only(top: 20.0),
                child: Text(
                    "Sign up to see and manage your appointments, favourite doctors & much more",
                    style: TextStyle(fontSize: 16.0, letterSpacing: 0.4)),
              ),
              // Container(
              //   margin: EdgeInsets.only(top: 40.0, left: 20.0, right: 20.0),
              //   child: SignInWithAppleButton(
              //     text: "Sign up with Apple",
              //     style: SignInWithAppleButtonStyle.whiteOutlined,
              //     onPressed: () async {
              //       final credential =
              //           await SignInWithApple.getAppleIDCredential(scopes: [
              //         AppleIDAuthorizationScopes.email,
              //         AppleIDAuthorizationScopes.fullName,
              //       ]);
              //       print(credential.email);
              //       print(credential.familyName);
              //       print(credential.givenName);
              //       print(credential.userIdentifier);
              //     },
              //   ),
              // ),
              Container(
                margin: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
                height: 50.0,
                width: MediaQuery.of(context).size.width * 0.9,
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                      side: BorderSide(
                          color: new Color(0xffFF2B56).withOpacity(1.0))),
                  color: new Color(0xffFF2B56).withOpacity(1.0),
                  onPressed: () {
                    openRegisterForm();
                  },
                  child: Column(
                    children: <Widget>[
                      Container(
                        transform: Matrix4.translationValues(0.0, 5.0, 0.0),
                        margin: EdgeInsets.only(top: 7.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              transform:
                                  Matrix4.translationValues(0.0, 0.0, 0.0),
                              margin: EdgeInsets.only(left: 10.0, right: 10.0),
                              child: SvgPicture.asset(
                                  "assets/images/search_medicine.svg",
                                  width: 20.0,
                                  height: 20.0,
                                  fit: BoxFit.contain),
                            ),
                            Text('Sign up with email',
                                overflow: TextOverflow.fade,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16.5,
                                    letterSpacing: 0.8)),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  openRegisterForm() {
    Navigator.push(context, FadePage(page: RegisterFormPage()));
  }
}
