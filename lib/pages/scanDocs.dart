import 'dart:io';

import 'package:MedZone/services/CustomTextStyles.dart';
import 'package:document_scanner/document_scanner.dart';
import 'package:flutter/material.dart';

class ScanDocs extends StatefulWidget {
  const ScanDocs({Key key}) : super(key: key);

  @override
  _ScanDocsState createState() => _ScanDocsState();
}

class _ScanDocsState extends State<ScanDocs> {

  File scannedDocument;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        title: Text("Scan Insurance Card", style: CustomTextStyle.display5(context)),
        backgroundColor: new Color(0xff17AF6B).withOpacity(1.0),
      ),
      body: Center(
        child: Container(
          height: 600.0,
          child: scannedDocument != null
              ? Image(
            image: FileImage(scannedDocument),
          )
              : DocumentScanner(
            onDocumentScanned: (ScannedImage scannedImage) {
              print("document : " + scannedImage.croppedImage);
              setState(() {
                scannedDocument = scannedImage.getScannedDocumentAsFile();
                Navigator.pop(context, scannedDocument);
              });
            },
          ),
        ),
      ),
    );
  }
}
