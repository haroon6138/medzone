import 'package:MedZone/pages/loginFormPage.dart';
import 'package:MedZone/pages/registerFormPage.dart';
import 'package:MedZone/utils/transitions/fade_page.dart';
import 'package:flutter/material.dart';
// import 'package:sign_in_with_apple/sign_in_with_apple.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
          title: Text("Login",
              style: TextStyle(
                fontSize: 16.0,
                fontWeight: FontWeight.bold,
                letterSpacing: 0.5,
                fontFamily: 'Open-Sans',
              )),
          backgroundColor: new Color(0xff17AF6B).withOpacity(1.0)),
      body: Container(
        margin: EdgeInsets.all(20.0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Welcome to MedZone",
                  style: TextStyle(
                      fontSize: 22.0,
                      fontWeight: FontWeight.bold,
                      fontFamily: "Open-Sans-Bold",
                      letterSpacing: 0.6)),
              Container(
                margin: EdgeInsets.only(top: 20.0),
                child: Text(
                    "Log in to see and manage your appointments, favourite doctors & much more",
                    style: TextStyle(fontSize: 16.0, letterSpacing: 0.4)),
              ),
              // Container(
              //   margin: EdgeInsets.only(top: 40.0, left: 20.0, right: 20.0),
              //   child: SignInWithAppleButton(
              //     style: SignInWithAppleButtonStyle.whiteOutlined,
              //     onPressed: () async {
              //       final credential =
              //           await SignInWithApple.getAppleIDCredential(scopes: [
              //         AppleIDAuthorizationScopes.email,
              //         AppleIDAuthorizationScopes.fullName,
              //       ]);
              //       print(credential.email);
              //       print(credential.familyName);
              //       print(credential.givenName);
              //       print(credential.userIdentifier);
              //     },
              //   ),
              // ),
              Container(
                margin: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
                height: 50.0,
                width: MediaQuery.of(context).size.width * 0.9,
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                      side: BorderSide(
                          color: new Color(0xffFF2B56).withOpacity(1.0))),
                  color: new Color(0xffFF2B56).withOpacity(1.0),
                  onPressed: () {
                    openLoginFormPage();
                  },
                  child: Column(
                    children: <Widget>[
                      Container(
                        transform: Matrix4.translationValues(0.0, 5.0, 0.0),
                        margin: EdgeInsets.only(top: 7.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                                transform:
                                    Matrix4.translationValues(0.0, 0.0, 0.0),
                                margin:
                                    EdgeInsets.only(left: 10.0, right: 10.0),
                                child: Icon(Icons.login, color: Colors.white)),
                            Text('Login with email',
                                overflow: TextOverflow.fade,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16.5,
                                    letterSpacing: 0.8)),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              InkWell(
                onTap: () {
                  navigateToRegisterPage();
                },
                child: Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.only(top: 20.0),
                  child: Text("Don't have an account? Sign up now.",
                      style: TextStyle(
                          fontSize: 16.0,
                          letterSpacing: 0.4,
                          color: new Color(0xffFF2B56).withOpacity(1.0),
                          fontWeight: FontWeight.bold,
                          decoration: TextDecoration.underline
                      )),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  navigateToRegisterPage() async {
    await Navigator.push(context, FadePage(page: RegisterFormPage()));
  }

  openLoginFormPage() {
    Navigator.push(
        context, FadePage(page: LoginFormPage(isAppointmentLogin: false)));
  }
}
