import 'dart:io';

import 'package:MedZone/pages/scanDocs.dart';
import 'package:MedZone/services/CustomTextStyles.dart';
import 'package:MedZone/utils/transitions/fade_page.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class Insurance extends StatefulWidget {
  const Insurance({Key key}) : super(key: key);

  @override
  _InsuranceState createState() => _InsuranceState();
}

class _InsuranceState extends State<Insurance> {
  File frontScannedDocument;
  File backScannedDocument;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Insurance Details", style: CustomTextStyle.display5(context)),
        backgroundColor: new Color(0xff17AF6B).withOpacity(1.0),
      ),
      body: Container(
        margin: EdgeInsets.all(20.0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: [
              Container(
                child: Text("insuranceUpload", style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold, letterSpacing: 0.6)).tr(),
              ),
              frontScannedDocument == null
                  ? Container(
                      margin: EdgeInsets.fromLTRB(0.0, 40.0, 0.0, 0.0),
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
                        onPressed: () {
                          scanDocs(true);
                        },
                        textColor: Colors.white,
                        padding: const EdgeInsets.all(0.0),
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: 80,
                          decoration: new BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              gradient: new LinearGradient(
                                colors: [
                                  Color.fromARGB(255, 41, 180, 214),
                                  Color.fromARGB(255, 62, 218, 164),
                                ],
                              )),
                          padding: const EdgeInsets.all(10.0),
                          child: Center(
                            child: Text("uploadInsuranceFront",
                                    overflow: TextOverflow.fade, textAlign: TextAlign.center, style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 13, letterSpacing: 0.8))
                                .tr(),
                          ),
                        ),
                      ),
                    )
                  : Center(
                      child: Container(
                        margin: EdgeInsets.fromLTRB(0.0, 40.0, 0.0, 0.0),
                        height: 200.0,
                        child: Image(
                          image: FileImage(frontScannedDocument),
                        ),
                      ),
                    ),
              backScannedDocument == null
                  ? Container(
                      margin: EdgeInsets.fromLTRB(0.0, 40.0, 0.0, 0.0),
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
                        onPressed: () {
                          scanDocs(false);
                        },
                        textColor: Colors.white,
                        padding: const EdgeInsets.all(0.0),
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: 80,
                          decoration: new BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              gradient: new LinearGradient(
                                colors: [
                                  Color.fromARGB(255, 41, 180, 214),
                                  Color.fromARGB(255, 62, 218, 164),
                                ],
                              )),
                          padding: const EdgeInsets.all(10.0),
                          child: Center(
                            child: Text("uploadInsuranceBack",
                                    overflow: TextOverflow.fade, textAlign: TextAlign.center, style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 13, letterSpacing: 0.8))
                                .tr(),
                          ),
                        ),
                      ),
                    )
                  : Center(
                      child: Container(
                        margin: EdgeInsets.fromLTRB(0.0, 40.0, 0.0, 0.0),
                        height: 200.0,
                        child: Image(
                          image: FileImage(backScannedDocument),
                        ),
                      ),
                    ),
              if (frontScannedDocument != null && backScannedDocument != null)
                Container(
                  margin: EdgeInsets.only(top: 50.0, bottom: 20.0, left: 10.0, right: 10.0),
                  height: 55.0,
                  width: MediaQuery.of(context).size.width * 0.9,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0), side: BorderSide(color: new Color(0xffFF2B56).withOpacity(1.0))),
                    color: new Color(0xffFF2B56).withOpacity(1.0),
                    onPressed: () {
                      // goAppointmentSchedule(context);
                    },
                    child: Column(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(top: 14.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Text('Update Insurance', textAlign: TextAlign.center, style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 18.0, letterSpacing: 0.8)),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }

  scanDocs(bool isFront) async {
    Navigator.push(context, FadePage(page: ScanDocs())).then((value) {
      print(value);
      setState(() {
        if (isFront)
          frontScannedDocument = value;
        else
          backScannedDocument = value;
      });
    });
  }
}
