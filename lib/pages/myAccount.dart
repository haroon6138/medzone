import 'package:MedZone/config/config.dart';
import 'package:MedZone/models/AuthUserProvider.dart';
import 'package:MedZone/pages/insurance.dart';
import 'package:MedZone/pages/login.dart';
import 'package:MedZone/pages/loginFormPage.dart';
import 'package:MedZone/pages/registerFormPage.dart';
import 'package:MedZone/pages/registerPage.dart';
import 'package:MedZone/services/CommonServices.dart';
import 'package:MedZone/services/Covid-Services.dart';
import 'package:MedZone/services/CustomTextStyles.dart';
import 'package:MedZone/utils/transitions/fade_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';

class MyAccount extends StatefulWidget {
  @override
  _MyAccountState createState() => _MyAccountState();
}

class _MyAccountState extends State<MyAccount> {
  AuthUserProvider authUserProvider;

  @override
  Widget build(BuildContext context) {
    authUserProvider = Provider.of<AuthUserProvider>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("My Account", style: CustomTextStyle.display5(context)),
        backgroundColor: new Color(0xff17AF6B).withOpacity(1.0),
      ),
      body: Container(
        decoration: BoxDecoration(color: Colors.white),
        margin: EdgeInsets.only(top: 15.0),
        padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              authUserProvider.getIsLogin()
                  ? Container(
                      margin:
                          EdgeInsets.only(left: 15.0, top: 15.0, bottom: 15.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Text(
                              "Welcome " +
                                  authUserProvider
                                      .getAuthUser()
                                      .user
                                      .firstName +
                                  " " +
                                  authUserProvider.getAuthUser().user.lastName,
                              style: TextStyle(
                                  fontSize: 22.0,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: "Open-Sans-Bold",
                                  letterSpacing: 0.6)),
                        ],
                      ),
                    )
                  : InkWell(
                      onTap: () {
                        openLogin();
                      },
                      child: Row(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: 15.0, right: 10.0),
                            child: Icon(Icons.launch, color: Colors.black),
                          ),
                          Container(
                            child: Text("Log in",
                                style: TextStyle(
                                    fontSize: 16.0,
                                    letterSpacing: 0.5,
                                    fontFamily: 'Open-Sans')),
                          ),
                        ],
                      ),
                    ),
              Container(
                margin: EdgeInsets.only(left: 15.0, top: 5.0, bottom: 5.0),
                child: Divider(
                  color: Colors.grey,
                ),
              ),
              authUserProvider.getIsLogin()
                  ? InkWell(
                      onTap: () {
                        openInsurance();
                      },
                      child: Row(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: 15.0, right: 10.0),
                            child: Icon(Icons.credit_card, color: Colors.black),
                          ),
                          Container(
                            child: Text("Insurance",
                                style: TextStyle(
                                    fontSize: 16.0,
                                    letterSpacing: 0.5,
                                    fontFamily: 'Open-Sans')),
                          ),
                        ],
                      ),
                    )
                  : InkWell(
                      onTap: () {
                        openRegister();
                      },
                      child: Row(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: 15.0, right: 10.0),
                            child: Icon(Icons.person, color: Colors.black),
                          ),
                          Container(
                            child: Text("Sign up",
                                style: TextStyle(
                                    fontSize: 16.0,
                                    letterSpacing: 0.5,
                                    fontFamily: 'Open-Sans')),
                          ),
                        ],
                      ),
                    ),
              Container(
                margin: EdgeInsets.only(left: 15.0, top: 5.0, bottom: 5.0),
                child: Divider(
                  color: Colors.grey,
                ),
              ),
              authUserProvider.getIsLogin()
                  ? InkWell(
                      onTap: () {
                        logout();
                      },
                      child: Row(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: 15.0, right: 10.0),
                            child: Icon(Icons.cancel, color: Colors.black),
                          ),
                          Container(
                            child: Text("Log out",
                                style: TextStyle(
                                    fontSize: 16.0,
                                    letterSpacing: 0.5,
                                    fontFamily: 'Open-Sans')),
                          ),
                        ],
                      ),
                    )
                  : Container(),
              authUserProvider.getIsLogin()
                  ? Container(
                      margin:
                          EdgeInsets.only(left: 15.0, top: 5.0, bottom: 5.0),
                      child: Divider(
                        color: Colors.grey,
                      ),
                    )
                  : Container(),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(left: 15.0, right: 10.0),
                    child: Text("Contact us",
                        style: TextStyle(
                            fontSize: 16.0,
                            letterSpacing: 0.5,
                            fontFamily: 'Open-Sans')),
                  ),
                  Container(
                    margin: EdgeInsets.only(right: 15.0),
                    child: Icon(Icons.arrow_forward_ios, color: Colors.black),
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.only(left: 15.0, top: 5.0, bottom: 5.0),
                child: Divider(
                  color: Colors.grey,
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(left: 15.0, right: 10.0),
                    child: Text("Terms of use",
                        style: TextStyle(
                            fontSize: 16.0,
                            letterSpacing: 0.5,
                            fontFamily: 'Open-Sans')),
                  ),
                  Container(
                    margin: EdgeInsets.only(right: 15.0),
                    child: Icon(Icons.arrow_forward_ios, color: Colors.black),
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.only(left: 15.0, top: 5.0, bottom: 5.0),
                child: Divider(
                  color: Colors.grey,
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    margin:
                        EdgeInsets.only(left: 15.0, right: 10.0, bottom: 5.0),
                    child: Text("Privacy Policy",
                        style: TextStyle(
                            fontSize: 16.0,
                            letterSpacing: 0.5,
                            fontFamily: 'Open-Sans')),
                  ),
                  Container(
                    margin: EdgeInsets.only(right: 15.0),
                    child: Icon(Icons.arrow_forward_ios, color: Colors.black),
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.only(top: 5.0, bottom: 5.0),
                child: Divider(
                  color: Colors.grey[100],
                  thickness: 15.0,
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(
                        left: 15.0, right: 10.0, bottom: 5.0, top: 5.0),
                    child: Text("Share MedZone",
                        style: TextStyle(
                            fontSize: 16.0,
                            letterSpacing: 0.5,
                            fontFamily: 'Open-Sans')),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  logout() {
    APIService service = new APIService();
    EasyLoading.show(status: CommonService.getLoadingMessage(), maskType: EasyLoadingMaskType.black);
    service.logoutUser(context).then((res) {
      authUserProvider.setIsLogin(false);
      CommonService.removePref(AUTHENTICATED_USER);
      CommonService.removePref(JWT_TOKEN);
      EasyLoading.dismiss();
      CommonService.showToast(context, "Successfully Logged Out");
    }).catchError((err) {
      EasyLoading.dismiss();
      print(err);
    });
  }

  openLogin() {
    Navigator.push(context, FadePage(page: Login()));
  }

  openRegister() {
    Navigator.push(context, FadePage(page: RegisterFormPage()));
  }

  openInsurance() {
    Navigator.push(context, FadePage(page: Insurance()));
  }

}

