import 'package:MedZone/models/response/MedicalSpecialitiesResponse.dart';
import 'package:MedZone/services/CommonServices.dart';
import 'package:MedZone/services/Covid-Services.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

class SelectSpeciality extends StatefulWidget {
  @override
  _SelectSpecialityState createState() => _SelectSpecialityState();
}

class _SelectSpecialityState extends State<SelectSpeciality> {
  final myNameController = TextEditingController();
  APIService service;
  List<Data> data;

  List<Data> _searchResult = [];

  bool isDataLoaded = false;

  @override
  void initState() {
    service = new APIService();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      getDoctorSpecialities(context);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: new Color(0xffF1F2F2).withOpacity(1.0),
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text("Search",
            style: TextStyle(
              fontSize: 16.0,
              fontWeight: FontWeight.bold,
              letterSpacing: 0.5,
              fontFamily: 'Open-Sans',
            )),
        centerTitle: true,
        backgroundColor: new Color(0xff17AF6B).withOpacity(1.0),
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Container(
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: 50.0,
                      child: Row(
                        children: <Widget>[
                          new Flexible(
                            child: new TextField(
                              textInputAction: TextInputAction.done,
//                              onSubmitted: (_) =>
//                                  FocusScope.of(context).,
                              controller: myNameController,
                              onChanged: onSearchTextChanged,
                              decoration: const InputDecoration(
                                contentPadding: const EdgeInsets.all(15.0),
                                floatingLabelBehavior:
                                    FloatingLabelBehavior.never,
                                labelText:
                                    "Search for cardiology, dentist, flu etc",
                                labelStyle: TextStyle(color: Colors.grey),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(color: Colors.grey[300]),
                padding: EdgeInsets.fromLTRB(15.0, 10.0, 0.0, 15.0),
                child: Text("All specialities",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16.0,
                        fontFamily: "Open-Sans-Bold")),
              ),
              isDataLoaded && data.length != 0
                  ? Container(
                      margin: EdgeInsets.fromLTRB(15.0, 10.0, 20.0, 10.0),
                      // height: MediaQuery.of(context).size.height,
                      child: _searchResult.length != 0 ||
                              myNameController.text.isNotEmpty
                          ? ListView.builder(
                              itemCount: _searchResult.length,
                              shrinkWrap: true,
                              physics: ScrollPhysics(),
                              itemBuilder: (context, index) {
                                return InkWell(
                                  onTap: () {
                                    onSpecialitySelected(_searchResult[index]);
                                  },
                                  child: Container(
                                    margin: EdgeInsets.fromLTRB(
                                        0.0, 10.0, 0.0, 10.0),
                                    child: Text(
                                        _searchResult[index].name.toLowerCase(),
                                        style: TextStyle(fontSize: 15.0)),
                                  ),
                                );
                              },
                            )
                          : ListView.builder(
                              itemCount: data.length,
                              shrinkWrap: true,
                              physics: ScrollPhysics(),
                              itemBuilder: (context, index) {
                                return InkWell(
                                  onTap: () {
                                    onSpecialitySelected(data[index]);
                                  },
                                  child: Container(
                                    margin: EdgeInsets.fromLTRB(
                                        0.0, 10.0, 0.0, 10.0),
                                    child: Text(data[index].name.toLowerCase(),
                                        style: TextStyle(fontSize: 15.0)),
                                  ),
                                );
                              },
                            ),
                    )
                  : Container(),
            ],
          ),
        ),
      ),
    );
  }

  onSpecialitySelected(Data data) {
    print(data.name);
    Navigator.of(context).pop(data);
  }

  onSearchTextChanged(String text) async {
    _searchResult.clear();
    if (text.isEmpty) {
      setState(() {});
      return;
    }

    data.forEach((userDetail) {
      if (userDetail.name.toLowerCase().contains(text))
        _searchResult.add(userDetail);
    });

    setState(() {});
  }

  getDoctorSpecialities(context) {
    MedicalSpecialitiesResponse medicalSpecialitiesResponse =
        new MedicalSpecialitiesResponse();
    EasyLoading.show(
        status: CommonService.getLoadingMessage(),
        maskType: EasyLoadingMaskType.black);
    service.getDoctorSpecialities(false, context).then((res) {
      setState(() {
        medicalSpecialitiesResponse = MedicalSpecialitiesResponse.fromJson(res);
        data = medicalSpecialitiesResponse.data;
        print(data.length);
        isDataLoaded = true;
        EasyLoading.dismiss();
      });
    }).catchError((err) {
      EasyLoading.dismiss();
      print(err);
    });
  }
}
