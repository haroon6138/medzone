import 'package:MedZone/models/currentLocation.dart';
import 'package:MedZone/services/CommonServices.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';

class SearchResultMap extends StatefulWidget {
  SearchResultMap(this.currentLat, this.currentLong);

  var currentLat;
  var currentLong;

  @override
  _SearchResultMapState createState() => _SearchResultMapState();
}

class _SearchResultMapState extends State<SearchResultMap> {
  LatLng _initialPosition;
  GoogleMapController mapController;
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  var locationProvider;

  @override
  void initState() {
    super.initState();
    _initialPosition = LatLng(widget.currentLat, widget.currentLong);
  }

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
    mapController.setMapStyle(CommonService.mapStyle);
  }

  @override
  Widget build(BuildContext context) {
    locationProvider = Provider.of<CurrentLocation>(context);
    return Scaffold(
      appBar: AppBar(
        actions: [
          Center(
              child: InkWell(
            onTap: () {
              navigateToSearchList();
            },
            child: Container(
                margin: EdgeInsets.only(right: 10.0),
                child: Text("List",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15.0,
                        letterSpacing: 0.8))),
          ))
        ],
        title: Text("Doctor Locations",
            style: TextStyle(
              fontSize: 16.0,
              fontWeight: FontWeight.bold,
              letterSpacing: 0.5,
              fontFamily: 'Open-Sans',
            )),
        backgroundColor: new Color(0xff17AF6B).withOpacity(1.0),
      ),
      body: Container(
        child: GoogleMap(
          zoomControlsEnabled: true,
          myLocationButtonEnabled: true,
          myLocationEnabled: true,
          onMapCreated: _onMapCreated,
          onCameraMove: (object) {
            widget.currentLat = object.target.latitude;
            widget.currentLong = object.target.longitude;
          },
//          onCameraIdle: _getAddressFromLatLng,
//          padding: EdgeInsets.only(bottom: 0.0),
          initialCameraPosition: CameraPosition(
            target: _initialPosition,
            zoom: 13.0,
          ),
        ),
      ),
    );
  }

  navigateToSearchList() {
    Navigator.pop(context);
  }
}
