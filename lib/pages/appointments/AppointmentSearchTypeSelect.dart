import 'package:MedZone/custom_widget/appointment_type_select_card.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class AppointmentSearchTypeSelect extends StatefulWidget {
  @override
  _AppointmentSearchTypeSelectState createState() =>
      _AppointmentSearchTypeSelectState();
}

class _AppointmentSearchTypeSelectState
    extends State<AppointmentSearchTypeSelect> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: new Color(0xffF1F2F2).withOpacity(1.0),
        appBar: AppBar(
          elevation: 0.0,
          centerTitle: true,
          title: Text("MedZone",
              style: TextStyle(
                fontSize: 16.0,
                fontWeight: FontWeight.bold,
                letterSpacing: 0.5,
                fontFamily: 'Open-Sans',
              )),
          backgroundColor: new Color(0xff17AF6B).withOpacity(1.0),
        ),
        body: Container(
          height: MediaQuery.of(context).size.height,
          padding: EdgeInsets.fromLTRB(20, 10, 16, 16),
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                new Color(0xff17AF6B).withOpacity(1.0),
                new Color(0xffF1F2F2).withOpacity(1.0),
              ],
              stops: [0.2, 0.5],
            ),
          ),
          child: SingleChildScrollView(
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Container(
                    alignment: Alignment.center,
                    margin: EdgeInsets.only(bottom: 5.0, top: 10.0),
                    child: Text("Select your preferred search type",
                        textAlign: TextAlign.center,
                        overflow: TextOverflow.clip,
                        style: TextStyle(
                            fontSize: 18.0,
                            letterSpacing: 0.5,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Open-Sans-bold',
                            color: Colors.white)),
                  ),
                  AppointmentTypeSelectCard(
                    image: "assets/images/fever.svg",
                    headingText: "Search By Speciality",
                    descText: "Search an appointment with the clinic",
                    searchType: "clinic",
                  ),
                  AppointmentTypeSelectCard(
                    image: "assets/images/doctor.svg",
                    headingText: "Search By Doctor",
                    descText: "Search an appointment with the doctor",
                    searchType: "doctor",
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
