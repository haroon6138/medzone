import 'package:MedZone/config/config.dart';
import 'package:MedZone/models/response/InsuranceCompaniesResponse.dart';
import 'package:MedZone/models/response/MedicalSpecialitiesResponse.dart';
import 'package:MedZone/models/response/SearchDoctorsResponse.dart';
import 'package:MedZone/pages/SelectInsurance.dart';
import 'package:MedZone/pages/appointments/SearchResult.dart';
import 'package:MedZone/pages/appointments/SelectSpeciality.dart';
import 'package:MedZone/services/CommonServices.dart';
import 'package:MedZone/services/Covid-Services.dart';
import 'package:MedZone/utils/transitions/fade_page.dart';
import 'package:date_picker_timeline/date_picker_timeline.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:google_maps_webservice/places.dart';

final kGoogleApiKey = "AIzaSyCyDbWUM9d_sBUGIE8PcuShzPaqO08NSC8";

class SearchByClinic extends StatefulWidget {
  @override
  _SearchByClinicState createState() => _SearchByClinicState();
}

class _SearchByClinicState extends State<SearchByClinic> {
  final mySearchIllnessController = TextEditingController();
  final mySpecialityController = TextEditingController();
  final myLocationController = TextEditingController();
  final myInsuranceController = TextEditingController();

  GoogleMapsPlaces _places = GoogleMapsPlaces(apiKey: kGoogleApiKey);
  DateTime _selectedDateValue = DateTime.now();
  DatePickerController _controller = DatePickerController();

  Data selectedSpeciality;
  InsuranceData selectedInsuranceCompany;
  double selectedLat = 0.0;
  double selectedLong = 0.0;
  String selectedLocation = "";

  @override
  void initState() {
    super.initState();
    selectedSpeciality = new Data();
    selectedInsuranceCompany = new InsuranceData();
    selectedSpeciality.id = 0;
    selectedInsuranceCompany.id = 0;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: new Color(0xffF1F2F2).withOpacity(1.0),
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        elevation: 0.0,
        centerTitle: true,
        title: Text("MedZone",
            style: TextStyle(
              fontSize: 16.0,
              fontWeight: FontWeight.bold,
              letterSpacing: 0.5,
              fontFamily: 'Open-Sans',
            )),
        backgroundColor: new Color(0xff17AF6B).withOpacity(1.0),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        padding: EdgeInsets.fromLTRB(20, 10, 16, 16),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              new Color(0xff17AF6B).withOpacity(1.0),
              new Color(0xffF1F2F2).withOpacity(1.0),
            ],
            stops: [0.2, 0.6],
          ),
        ),
        child: SingleChildScrollView(
          child: Container(
            width: MediaQuery.of(context).size.width,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(bottom: 5.0, top: 5.0),
                  child: Text("Let's make you an appointment:",
                      textAlign: TextAlign.center,
                      overflow: TextOverflow.clip,
                      style: TextStyle(
                          fontSize: 18.0,
                          letterSpacing: 0.5,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Open-Sans-bold',
                          color: Colors.white)),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10.0),
                  child: TextField(
                    onTap: () {
                      print("Clicked");
                      openSelectSpecialityDialog();
                    },
                    readOnly: true,
                    showCursor: false,
                    controller: mySpecialityController,
                    decoration: InputDecoration(
                      isDense: true,
                      // Added this
                      contentPadding: EdgeInsets.all(15),
                      hintText: 'Select Speciality',
                      prefixIcon: Icon(Icons.search,
                          size: 30.0, color: Colors.grey[700]),
                      hintStyle: TextStyle(color: Colors.grey[700]),
                      filled: true,
                      fillColor: Colors.white,
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(8.0)),
                        borderSide: BorderSide(color: Colors.white, width: 2),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(8.0)),
                        borderSide: BorderSide(color: Colors.white, width: 2),
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10.0),
                  child: TextField(
                    onTap: () {
                      print("Clicked");
                      openLocationSelection();
                    },
                    readOnly: true,
                    showCursor: false,
                    controller: myLocationController,
                    decoration: InputDecoration(
                      isDense: true,
                      // Added this
                      contentPadding: EdgeInsets.all(15),
                      hintText: 'Select Location',
                      prefixIcon: Icon(Icons.location_on,
                          size: 30.0, color: Colors.grey[700]),
                      hintStyle: TextStyle(color: Colors.grey[700]),
                      filled: true,
                      fillColor: Colors.white,
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(8.0)),
                        borderSide: BorderSide(color: Colors.white, width: 2),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(8.0)),
                        borderSide: BorderSide(color: Colors.white, width: 2),
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10.0, bottom: 5.0),
                  child: Text("Select Date:",
                      textAlign: TextAlign.center,
                      overflow: TextOverflow.clip,
                      style: TextStyle(
                          fontSize: 18.0,
                          letterSpacing: 0.5,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Open-Sans-bold',
                          color: Colors.white)),
                ),
                Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10.0)),
                  child: DatePicker(
                    DateTime.now(),
                    height: 100,
                    controller: _controller,
                    initialSelectedDate: DateTime.now(),
                    selectionColor: new Color(0xff17AF6B).withOpacity(1.0),
                    selectedTextColor: Colors.white,
//                    inactiveDates: [
//                      DateTime.now().add(Duration(days: 3)),
//                      DateTime.now().add(Duration(days: 4)),
//                      DateTime.now().add(Duration(days: 7))
//                    ],
                    onDateChange: (date) {
                      // New date selected
                      setState(() {
                        _selectedDateValue = date;
                        print(_selectedDateValue);
                      });
                    },
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10.0),
                  child: TextField(
                    onTap: () {
                      print("Clicked");
                      openSelectInsuranceDialog();
                    },
                    readOnly: true,
                    controller: myInsuranceController,
                    decoration: InputDecoration(
                      isDense: true,
                      // Added this
                      contentPadding: EdgeInsets.all(15),
                      hintText: 'Select Insurance',
                      prefixIcon: Icon(Icons.credit_card,
                          size: 30.0, color: Colors.grey[700]),
                      hintStyle: TextStyle(color: Colors.grey[700]),
                      filled: true,
                      fillColor: Colors.white,
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(8.0)),
                        borderSide: BorderSide(color: Colors.white, width: 2),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(8.0)),
                        borderSide: BorderSide(color: Colors.white, width: 2),
                      ),
                    ),
                  ),
                ),
                Row(children: <Widget>[
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.only(top: 30.0),
                      height: 55.0,
                      width: MediaQuery.of(context).size.width * 0.9,
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            side: BorderSide(
                                color: new Color(0xffFF2B56).withOpacity(1.0))),
                        color: new Color(0xffFF2B56).withOpacity(1.0),
                        onPressed: () {
                          // openSearchResultsPage();
                          if (isAllDataSelected())
                            getDoctors(context);
                          else
                            CommonService.showToast(context,
                                "Please select all the options to continue");
                        },
                        child: Column(
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(top: 14.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Text('Find',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20.0,
                                          letterSpacing: 0.8)),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ]),
                // Container(
                //   margin: EdgeInsets.only(bottom: 5.0, top: 30.0),
                //   child: Text("Our top-searched specialities:",
                //       overflow: TextOverflow.clip,
                //       style: TextStyle(
                //           fontSize: 20.0,
                //           letterSpacing: 0.5,
                //           fontWeight: FontWeight.bold,
                //           fontFamily: 'Open-Sans-bold',
                //           color: Colors.black87)),
                // ),
                // Container(
                //   margin: EdgeInsets.only(top: 25.0),
                //   child: Row(
                //     children: <Widget>[
                //       Expanded(
                //         child: Container(
                //           child: Column(
                //             children: [
                //               SvgPicture.asset("assets/images/skin.svg",
                //                   width: 65.0,
                //                   height: 65.0,
                //                   fit: BoxFit.contain),
                //               Text("Dermatology",
                //                   style: TextStyle(
                //                       fontSize: 13.0,
                //                       fontFamily: 'Open-Sans-Bold'))
                //             ],
                //           ),
                //         ),
                //       ),
                //       Expanded(
                //         child: Container(
                //           child: Column(
                //             children: [
                //               SvgPicture.asset("assets/images/cardiology.svg",
                //                   width: 65.0,
                //                   height: 65.0,
                //                   fit: BoxFit.contain),
                //               Text("Cardiology",
                //                   style: TextStyle(
                //                       fontSize: 13.0,
                //                       fontFamily: 'Open-Sans-Bold'))
                //             ],
                //           ),
                //         ),
                //       ),
                //       Expanded(
                //         child: Container(
                //           child: Column(
                //             children: [
                //               SvgPicture.asset("assets/images/eye.svg",
                //                   width: 65.0,
                //                   height: 65.0,
                //                   fit: BoxFit.contain),
                //               Text("Eye Doctor",
                //                   style: TextStyle(
                //                       fontSize: 13.0,
                //                       fontFamily: 'Open-Sans-Bold'))
                //             ],
                //           ),
                //         ),
                //       ),
                //     ],
                //   ),
                // ),
                // Container(
                //   margin: EdgeInsets.only(top: 25.0),
                //   child: Row(
                //     children: <Widget>[
                //       Expanded(
                //         child: Container(
                //           child: Column(
                //             children: [
                //               SvgPicture.asset("assets/images/psychiatrist.svg",
                //                   width: 65.0,
                //                   height: 65.0,
                //                   fit: BoxFit.contain),
                //               Text("Psychiatrist",
                //                   style: TextStyle(
                //                       fontSize: 13.0,
                //                       fontFamily: 'Open-Sans-Bold'))
                //             ],
                //           ),
                //         ),
                //       ),
                //       Expanded(
                //         child: Container(
                //           child: Column(
                //             children: [
                //               SvgPicture.asset("assets/images/female.svg",
                //                   width: 65.0,
                //                   height: 65.0,
                //                   fit: BoxFit.contain),
                //               Text("OB-GYNE",
                //                   style: TextStyle(
                //                       fontSize: 13.0,
                //                       fontFamily: 'Open-Sans-Bold'))
                //             ],
                //           ),
                //         ),
                //       ),
                //       Expanded(
                //         child: Container(
                //           child: Column(
                //             children: [
                //               SvgPicture.asset("assets/images/baby.svg",
                //                   width: 65.0,
                //                   height: 65.0,
                //                   fit: BoxFit.contain),
                //               Text("Pediatric",
                //                   style: TextStyle(
                //                       fontSize: 13.0,
                //                       fontFamily: 'Open-Sans-Bold'))
                //             ],
                //           ),
                //         ),
                //       ),
                //     ],
                //   ),
                // ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  openSelectSpecialityDialog() {
    Navigator.push(context, FadePage(page: SelectSpeciality())).then((value) {
      if (value != null)
        setState(() {
          selectedSpeciality = value;
          mySpecialityController.text = selectedSpeciality.name;
          print(_selectedDateValue.toString().split(" ")[0]);
        });
    });
  }

  openSelectInsuranceDialog() {
    Navigator.push(context, FadePage(page: SelectInsurance())).then((value) {
      if (value != null)
        setState(() {
          selectedInsuranceCompany = value;
          myInsuranceController.text = selectedInsuranceCompany.name;
          CommonService.setPrefs(SELECTED_INSURANCE_COMPANY_ID,
              selectedInsuranceCompany.id.toString());
          print(_selectedDateValue.toString().split(" ")[0]);
        });
    });
  }

  bool isAllDataSelected() {
    if (selectedSpeciality.id != 0 &&
        selectedLat != 0.0 &&
        selectedLong != 0.0 &&
        selectedInsuranceCompany.id != 0)
      return true;
    else
      return false;
  }

  getDoctors(context) {
    APIService service = new APIService();
    SearchDoctorsResponse searchDoctorsResponse = new SearchDoctorsResponse();
    EasyLoading.show(
        status: CommonService.getLoadingMessage(),
        maskType: EasyLoadingMaskType.black);
    service
        .getDoctors(
            selectedSpeciality.id,
            selectedLat.toString(),
            selectedLong.toString(),
            _selectedDateValue.toString().split(" ")[0],
            context)
        .then((res) {
      setState(() {
        searchDoctorsResponse = SearchDoctorsResponse.fromJson(res);
        EasyLoading.dismiss();
        if (searchDoctorsResponse.data.length != 0) {
          Navigator.push(
              context,
              FadePage(
                  page: SearchResult(
                      searchDoctorsResponse: searchDoctorsResponse)));
        } else {
          CommonService.showToast(context, "No doctors available");
        }
      });
    }).catchError((err) {
      EasyLoading.dismiss();
      print(err);
    });
  }

  openLocationSelection() async {
    Prediction place = await PlacesAutocomplete.show(
        context: context,
        apiKey: kGoogleApiKey,
        mode: Mode.overlay,
        language: "en",
        components: [new Component(Component.country, "us")]);

    if (place != null && place.placeId != null) {
      PlacesDetailsResponse response =
          await _places.getDetailsByPlaceId(place.placeId);
      selectedLat = response.result.geometry.location.lat;
      selectedLong = response.result.geometry.location.lng;

      myLocationController.text = response.result.formattedAddress;

      print(selectedLat);
      print(selectedLong);
    }
    setState(() {});
  }
}
