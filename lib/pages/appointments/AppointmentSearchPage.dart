import 'package:MedZone/models/AppointmentsProvider.dart';
import 'package:MedZone/models/AuthUserProvider.dart';
import 'package:MedZone/pages/appointments/widgets/AppointmentView.dart';
import 'package:MedZone/pages/login.dart';
import 'package:MedZone/services/CustomTextStyles.dart';
import 'package:MedZone/utils/transitions/fade_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

import 'SearchByClinic.dart';

class AppointmentSearch extends StatefulWidget {
  final int index;
  final Function cancelFunction;

  AppointmentSearch(this.index, {@required this.cancelFunction});

  @override
  _AppointmentSearchState createState() => _AppointmentSearchState();
}

class _AppointmentSearchState extends State<AppointmentSearch>
    with SingleTickerProviderStateMixin {
  bool hasAppointments = true;

  AuthUserProvider authUserProvider;
  AppointmentsProvider appointmentsProvider;

  static TabController _tabController;

  @override
  void initState() {
    _tabController = new TabController(length: 2, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    authUserProvider = Provider.of<AuthUserProvider>(context);
    appointmentsProvider = Provider.of<AppointmentsProvider>(context);
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Appointments", style: CustomTextStyle.display5(context)),
        backgroundColor: new Color(0xff17AF6B).withOpacity(1.0),
      ),
      body: authUserProvider.getIsLogin()
          ? ((appointmentsProvider.getAppointments() != null &&
                  appointmentsProvider.getAppointments().data.length != 0)
              ? _getAppointmentsUI()
              : _getNoAppointmentsUI())
          : _getLoginUI(),
    );
  }

  _getNoAppointmentsUI() {
    return SingleChildScrollView(
      child: Container(
        margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.2),
        child: Center(
          child: Column(
            children: <Widget>[
              Container(
                  child: SvgPicture.asset('assets/images/calendar_noAppos.svg',
                      width: 130.0, height: 130.0)),
              Container(
                margin: EdgeInsets.only(left: 80.0, right: 80.0, top: 20.0),
                child: Text("Always stay informed about your appointments",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 22.0,
                        letterSpacing: 0.5,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Open-Sans-Bold',
                        color: Colors.black)),
              ),
              Container(
                  margin: EdgeInsets.only(left: 80.0, right: 80.0, top: 20.0),
                  child: Text("Book an appointment now",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 18.0,
                          letterSpacing: 0.5,
                          fontFamily: 'Open-Sans',
                          color: Colors.black))),
              Container(
                margin: EdgeInsets.only(left: 60.0, right: 60.0, top: 20.0),
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                      side: BorderSide(
                          color: new Color(0xff17AF6B).withOpacity(1.0))),
                  color: new Color(0xff17AF6B).withOpacity(1.0),
                  onPressed: () {},
                  child: Column(
                    children: <Widget>[
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(10.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Text('Book an appointment',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18.0,
                                    letterSpacing: 0.5)),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _getLoginUI() {
    return Container(
      margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.15),
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Container(
                child: SvgPicture.asset('assets/images/calendar_noAppos.svg',
                    width: 130.0, height: 130.0)),
            Container(
              margin: EdgeInsets.only(left: 80.0, right: 80.0, top: 20.0),
              child: Text("Always stay informed about your appointments",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 22.0,
                      letterSpacing: 0.5,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Open-Sans-Bold',
                      color: Colors.black)),
            ),
            Container(
                margin: EdgeInsets.only(left: 80.0, right: 80.0, top: 20.0),
                child: Text("Book an appointment now",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 18.0,
                        letterSpacing: 0.5,
                        fontFamily: 'Open-Sans',
                        color: Colors.black))),
            Container(
              margin: EdgeInsets.only(left: 45.0, right: 45.0, top: 20.0),
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    side: BorderSide(
                        color: new Color(0xff17AF6B).withOpacity(1.0))),
                color: new Color(0xff17AF6B).withOpacity(1.0),
                onPressed: () {
                  openBookAppointment();
                },
                child: Column(
                  children: <Widget>[
                    Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.all(10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text('Book an appointment',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18.0,
                                  letterSpacing: 0.5)),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 45.0, right: 45.0, top: 50.0),
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    side: BorderSide(
                        color: new Color(0xffFF2B56).withOpacity(1.0))),
                color: new Color(0xffFF2B56).withOpacity(1.0),
                onPressed: () {
                  openLogin();
                },
                child: Column(
                  children: <Widget>[
                    Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.all(15.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          Text('Already have an account? Log in',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 14.0,
                                  letterSpacing: 0.5)),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  openLogin() {
    Navigator.push(context, FadePage(page: Login()));
  }

  openBookAppointment() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => SearchByClinic()));
  }

  _getAppointmentsUI() {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.all(10.0),
          child: TabBar(
            indicatorColor: Color(0xff17AF6B),
            controller: _tabController,
            tabs: [
              Tab(
                  child:
                      Text("Upcoming", style: TextStyle(color: Colors.black))),
              Tab(
                child: Text("History", style: TextStyle(color: Colors.black)),
              )
            ],
          ),
        ),
        Container(
          height: MediaQuery.of(context).size.height * 0.75,
          child: TabBarView(
            physics: NeverScrollableScrollPhysics(),
            children: [
              Container(
                child: appointmentsProvider.getUpcomingAppointments().length !=
                        0
                    ? ListView.builder(
                        shrinkWrap: true,
                        itemCount:
                            appointmentsProvider.getUpcomingAppointments() !=
                                    null
                                ? appointmentsProvider
                                    .getUpcomingAppointments()
                                    .length
                                : 0,
                        itemBuilder: (context, index) {
                          return AppointmentView(
                            appointmentData: appointmentsProvider
                                .getUpcomingAppointments()[index],
                            cancelFunction: widget.cancelFunction,
                          );
                        },
                      )
                    : _getNoAppointmentsUI(),
              ),
              Container(
                child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: appointmentsProvider.getPastAppointments() != null
                      ? appointmentsProvider.getPastAppointments().length
                      : 0,
                  itemBuilder: (context, index) {
                    return AppointmentView(
                      appointmentData:
                          appointmentsProvider.getPastAppointments()[index],
                      cancelFunction: widget.cancelFunction,
                    );
                  },
                ),
              )
            ],
            controller: _tabController,
          ),
        ),
      ],
    );
  }
}
