import 'package:MedZone/models/response/SearchDoctorsResponse.dart';
import 'package:MedZone/pages/appointments/widgets/DoctorView.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class BookSuccess extends StatefulWidget {
  String dateStr;
  String timeStr;
  int isVirtual;
  DoctorData doc;
  HospitalsData selectedHospital;

  BookSuccess(
      {@required this.dateStr,
      @required this.timeStr,
      @required this.isVirtual,
      @required this.doc,
      @required this.selectedHospital});

  @override
  _BookSuccessState createState() => _BookSuccessState();
}

class _BookSuccessState extends State<BookSuccess> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        centerTitle: true,
        backgroundColor: new Color(0xff17AF6B).withOpacity(1.0),
        title: Text("Book Appointment",
            style: TextStyle(
              fontSize: 16.0,
              fontWeight: FontWeight.bold,
              letterSpacing: 0.5,
              fontFamily: 'Open-Sans',
            )),
      ),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.only(top: 20.0),
                child: Center(
                  child: SvgPicture.asset("assets/images/check.svg",
                      width: 100.0, height: 100.0),
                ),
              ),
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.only(top: 15.0, bottom: 10.0),
                child: Text("THANK YOU!",
                    overflow: TextOverflow.clip,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 18.0,
                        letterSpacing: 0.5,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Open-Sans-bold',
                        color: Colors.grey[800])),
              ),
              Container(
                alignment: Alignment.center,
                child: Text("Your appointment has been booked successfully.",
                    style: TextStyle(
                        fontSize: 14.0,
                        color: Colors.grey[900],
                        letterSpacing: 0.5)),
              ),
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.only(top: 5.0, bottom: 5.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("REFERENCE ID:",
                        style: TextStyle(
                            fontSize: 14.0,
                            color: Colors.grey[600],
                            letterSpacing: 0.5)),
                    Text(" MQ50355404",
                        style: TextStyle(
                            fontSize: 14.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.grey[900],
                            letterSpacing: 0.5)),
                  ],
                ),
              ),
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.only(bottom: 15.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Consultation Type:",
                        style: TextStyle(
                            fontSize: 14.0,
                            color: Colors.grey[600],
                            letterSpacing: 0.5)),
                    widget.isVirtual == 1 ? Text(" Virtual Appointment",
                        style: TextStyle(
                            fontSize: 14.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.grey[900],
                            letterSpacing: 0.5)) : Text(" Walk-In Appointment",
                        style: TextStyle(
                            fontSize: 14.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.grey[900],
                            letterSpacing: 0.5)),
                  ],
                ),
              ),
              Container(
                child: Divider(
                  height: 5.0,
                  color: Colors.grey[300],
                  thickness: 1.0,
                ),
              ),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Container(
                      margin: EdgeInsets.only(left: 30.0, top: 10.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("DATE",
                              style: TextStyle(
                                  fontSize: 14.0,
                                  color: Colors.grey[500],
                                  letterSpacing: 0.7)),
                          // Text("Tue, 27 Mar, 2020",
                          Text(widget.dateStr,
                              style: TextStyle(
                                  fontSize: 14.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey[900],
                                  letterSpacing: 0.7)),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(right: 30.0, top: 10.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text("TIME",
                              style: TextStyle(
                                  fontSize: 14.0,
                                  color: Colors.grey[500],
                                  letterSpacing: 0.7)),
                          // Text("09:00 AM",
                          Text(widget.timeStr,
                              style: TextStyle(
                                  fontSize: 14.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey[900],
                                  letterSpacing: 0.7)),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.only(top: 20.0),
                child: Text("Doctor Details",
                    overflow: TextOverflow.clip,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 18.0,
                        letterSpacing: 0.5,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Open-Sans-bold',
                        color: Colors.grey[800])),
              ),
              Container(
                margin: EdgeInsets.only(left: 15.0, right: 15.0),
                child: DoctorView(
                    isShowAddress: false,
                    doc: widget.doc,
                    isClickable: false,
                    isMyDoctor: false),
              ),
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.only(top: 20.0),
                child: Text("Appointment Location",
                    overflow: TextOverflow.clip,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 18.0,
                        letterSpacing: 0.5,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Open-Sans-bold',
                        color: Colors.grey[800])),
              ),
              Container(
                child: Card(
                  margin: EdgeInsets.only(left: 15.0, right: 15.0, top: 5.0),
                  color: Colors.white,
                  elevation: 3.0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Container(
                    decoration: BoxDecoration(),
                    padding: EdgeInsets.all(10.0),
                    width: MediaQuery.of(context).size.width,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text("Appointment Location",
                                style: TextStyle(
                                    fontSize: 14.0,
                                    color: Colors.grey[900],
                                    letterSpacing: 0.7)),
                            Row(
                              children: [
                                Text("8.6 km away",
                                    style: TextStyle(
                                        fontSize: 12.0,
                                        color: Colors.grey[900],
                                        letterSpacing: 0.7)),
                                Container(
                                  margin:
                                      EdgeInsets.fromLTRB(5.0, 0.0, 5.0, 0.0),
                                  child: Icon(Icons.directions,
                                      color: Color(0xff17AF6B).withOpacity(1.0),
                                      size: 25.0),
                                ),
                              ],
                            ),
                          ],
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 10.0),
                          // child: Text("NYC Health + Hospitals",
                          child: Text(widget.selectedHospital.name,
                              style: TextStyle(
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey[900],
                                  letterSpacing: 0.7)),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              width: MediaQuery.of(context).size.width * 0.75,
                              margin: EdgeInsets.only(top: 5.0, bottom: 5.0),
                              // child: Text("1901 1st Avenue, New York, NY 10029",
                              child: Text(widget.selectedHospital.address1,
                                  overflow: TextOverflow.clip,
                                  style: TextStyle(
                                      fontSize: 14.0,
                                      color: Colors.grey[700],
                                      letterSpacing: 0.5)),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 100.0,
              ),
            ],
          ),
        ),
      ),
      bottomSheet: Row(children: <Widget>[
        Expanded(
          child: Container(
            margin: EdgeInsets.only(
                top: 10.0, bottom: 20.0, left: 15.0, right: 15.0),
            height: 55.0,
            width: MediaQuery.of(context).size.width * 0.9,
            child: RaisedButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                  side: BorderSide(
                      color: new Color(0xffFF2B56).withOpacity(1.0))),
              color: new Color(0xffFF2B56).withOpacity(1.0),
              onPressed: () {
                openHomePage();
              },
              child: Column(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 14.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text('OK Thanks!',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 18.0,
                                letterSpacing: 0.8)),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ]),
    );
  }

  openHomePage() {
    Navigator.popUntil(context, ModalRoute.withName('/'));
  }
}
