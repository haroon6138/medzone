import 'dart:io';

import 'package:MedZone/config/config.dart';
import 'package:MedZone/models/AuthUserProvider.dart';
import 'package:MedZone/models/response/DoctorSlotsResponse.dart';
import 'package:MedZone/models/response/SearchDoctorsResponse.dart';
import 'package:MedZone/pages/appointments/BookSuccess.dart';
import 'package:MedZone/pages/appointments/widgets/DoctorView.dart';
import 'package:MedZone/pages/appointments/widgets/WorkingAddresses.dart';
import 'package:MedZone/pages/loginFormPage.dart';
import 'package:MedZone/services/CommonServices.dart';
import 'package:MedZone/services/Covid-Services.dart';
import 'package:MedZone/utils/PlatformBridge.dart';
import 'package:MedZone/utils/date_uitl.dart';
import 'package:MedZone/utils/transitions/fade_page.dart';
import 'package:MedZone/utils/transitions/slide_up_page.dart';
import 'package:date_picker_timeline/date_picker_widget.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animated_dialog/flutter_animated_dialog.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';

class AppointmentSchedule extends StatefulWidget {
  dynamic doc;
  dynamic selectedHospital;

  AppointmentSchedule({this.doc, this.selectedHospital});

  @override
  _AppointmentScheduleState createState() => _AppointmentScheduleState();
}

class _AppointmentScheduleState extends State<AppointmentSchedule> {
  DateTime _selectedDateValue = DateTime.now();
  DatePickerController _controller = DatePickerController();

  DoctorSlotsResponse doctorSlotsResponse;
  bool isDataLoaded = false;
  var type = "";
  String selectedTime = "";

  String selectedTimeBookSuccess = "";

  int selectedButtonIndex = 0;

  AuthUserProvider authUserProvider;

  @override
  void initState() {
    doctorSlotsResponse = new DoctorSlotsResponse();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      getTimeSlots();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    final double itemHeight = ((size.height - kToolbarHeight - 24) * 0.30) / 2;
    final double itemWidth = size.width / 2;

    authUserProvider = Provider.of<AuthUserProvider>(context);

    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        centerTitle: true,
        title: Text("Select a time slot",
            style: TextStyle(
              fontSize: 16.0,
              fontWeight: FontWeight.bold,
              letterSpacing: 0.5,
              fontFamily: 'Open-Sans',
            )),
        backgroundColor: new Color(0xff17AF6B).withOpacity(1.0),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.only(left: 10.0, right: 10.0),
              child: DoctorView(
                isClickable: false,
                isShowAddress: false,
                doc: widget.doc,
                isMyDoctor: false,
              ),
            ),
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
              child: Text(getFormattedDate(), overflow: TextOverflow.clip, textAlign: TextAlign.center, style: TextStyle(fontSize: 18.0, letterSpacing: 0.5, fontWeight: FontWeight.bold, fontFamily: 'Open-Sans-bold', color: Colors.grey[800])),
            ),
            Container(
              child: Divider(
                height: 4.0,
                color: Colors.grey[300],
                thickness: 1.0,
              ),
            ),
            Container(
              decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(10.0)),
              child: DatePicker(
                DateTime.now(),
                height: 100,
                controller: _controller,
                initialSelectedDate: DateTime.now(),
                selectionColor: new Color(0xff17AF6B).withOpacity(1.0),
                selectedTextColor: Colors.white,
//                    inactiveDates: [
//                      DateTime.now().add(Duration(days: 3)),
//                      DateTime.now().add(Duration(days: 4)),
//                      DateTime.now().add(Duration(days: 7))
//                    ],
                onDateChange: (date) {
                  // New date selected
                  setState(() {
                    _selectedDateValue = date;
                    print(_selectedDateValue);
                    getTimeSlots();
                  });
                },
              ),
            ),
            Container(
              child: Divider(
                height: 4.0,
                color: Colors.grey[300],
                thickness: 1.0,
              ),
            ),
            Container(
              child: InkWell(
                onTap: () {
                  openWorkAddresses(context);
                },
                child: Card(
                  margin: EdgeInsets.all(10.0),
                  color: Colors.white,
                  elevation: 3.0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Container(
                    decoration: BoxDecoration(),
                    padding: EdgeInsets.all(10.0),
                    width: MediaQuery.of(context).size.width,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text("Appointment Location", style: TextStyle(fontSize: 14.0, color: Colors.grey[900], letterSpacing: 0.7)),
                            Row(
                              children: [
                                Text(widget.selectedHospital.distance + " km away", style: TextStyle(fontSize: 12.0, color: Colors.grey[900], letterSpacing: 0.7)),
                                Container(
                                  margin: EdgeInsets.fromLTRB(5.0, 0.0, 5.0, 0.0),
                                  child: Icon(Icons.directions, color: Color(0xff17AF6B).withOpacity(1.0), size: 25.0),
                                ),
                              ],
                            ),
                          ],
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 10.0),
                          child: Text(widget.selectedHospital.name, style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold, color: Colors.grey[900], letterSpacing: 0.7)),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              width: MediaQuery.of(context).size.width * 0.75,
                              margin: EdgeInsets.only(top: 5.0, bottom: 5.0),
                              child: Text(widget.selectedHospital.address1, overflow: TextOverflow.clip, style: TextStyle(fontSize: 14.0, color: Colors.grey[700], letterSpacing: 0.5)),
                            ),
                            Container(
                              margin: EdgeInsets.fromLTRB(5.0, 0.0, 5.0, 0.0),
                              child: Icon(Icons.keyboard_arrow_down, color: Colors.grey[600], size: 30.0),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 5.0),
              child: Divider(
                height: 5.0,
                color: Colors.grey[300],
                thickness: 1.0,
              ),
            ),
            isDataLoaded && doctorSlotsResponse.data.length != 0
                ? Container(
                    alignment: Alignment.center,
                    margin: EdgeInsets.only(top: 10.0, bottom: 0.0),
                    child: Text("Select time slot", overflow: TextOverflow.clip, textAlign: TextAlign.center, style: TextStyle(fontSize: 18.0, letterSpacing: 0.5, fontWeight: FontWeight.bold, fontFamily: 'Open-Sans-bold', color: Colors.grey[800])),
                  )
                : Container(
                    child: Text("No time slot available. Please select a different date or hospital", overflow: TextOverflow.clip, textAlign: TextAlign.center, style: TextStyle(fontSize: 18.0, letterSpacing: 0.5, fontWeight: FontWeight.bold, fontFamily: 'Open-Sans-bold', color: Colors.grey[800])),
                  ),
            isDataLoaded && doctorSlotsResponse.data.length != 0
                ? Container(
                    margin: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                    height: MediaQuery.of(context).size.height * 0.55,
                    child: GridView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: doctorSlotsResponse.data.length,
                      gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 4,
                        childAspectRatio: (itemWidth / itemHeight),
                      ),
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
                          margin: EdgeInsets.all(5.0),
                          child: ButtonTheme(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5.0),
                                side: BorderSide(
                                  color: Color(0xff17AF6B),
                                  style: BorderStyle.solid,
                                  width: 1.0,
                                ),
                              ),
                              minWidth: MediaQuery.of(context).size.width * 0.2,
                              height: 20.0,
                              child: index == selectedButtonIndex ? getSelectedButton(index) : getNormalButton(index)),
                        );
                      },
                    ),
                  )
                : Container(),
            SizedBox(
              height: 200.0,
            ),
          ],
        ),
      ),
      bottomSheet: Row(children: <Widget>[
        Expanded(
          child: Container(
            margin: EdgeInsets.only(top: 10.0, bottom: 20.0, left: 10.0, right: 10.0),
            height: 55.0,
            width: MediaQuery.of(context).size.width * 0.9,
            child: RaisedButton(
              shape: (doctorSlotsResponse != null && doctorSlotsResponse.data.length != 0) ? RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0), side: BorderSide(color: new Color(0xffFF2B56).withOpacity(1.0))) : null,
              color: new Color(0xffFF2B56).withOpacity(1.0),
              onPressed: doctorSlotsResponse.data.length != 0
                  ? () {
                      bookAppointment();
                    }
                  : null,
              child: Column(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 14.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text('Confirm Appointment', textAlign: TextAlign.center, style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 18.0, letterSpacing: 0.8)),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ]),
    );
  }

  Widget getNormalButton(int index) {
    return RaisedButton(
      color: Colors.white,
      textColor: new Color(0xFF60686b),
      elevation: 0.0,
      onPressed: () {
        setState(() {
          selectedButtonIndex = index;
          selectedTime = doctorSlotsResponse.data[selectedButtonIndex];
          selectedTimeBookSuccess = selectedTime;
          print(selectedTimeBookSuccess);
          var df = DateFormat("h:mma");
          var dt = df.parse(selectedTime.replaceAll(" ", ""));
          selectedTime = DateFormat('HH:mm').format(dt);
          print(selectedTime);
        });
      },
      child: Text(doctorSlotsResponse.data[index], style: TextStyle(fontSize: 12.0, fontWeight: FontWeight.bold)),
    );
  }

  Widget getSelectedButton(int index) {
    return RaisedButton(
      color: Color(0xff17AF6B),
      textColor: Colors.white,
      elevation: 0.0,
      onPressed: () {
        setState(() {
          selectedButtonIndex = index;
          selectedTime = doctorSlotsResponse.data[selectedButtonIndex];
          selectedTimeBookSuccess = selectedTime;
          print(selectedTimeBookSuccess);
          var df = DateFormat("h:mma");
          var dt = df.parse(selectedTime.replaceAll(" ", ""));
          selectedTime = DateFormat('HH:mm').format(dt);
          print(selectedTime);
        });
      },
      child: Text(doctorSlotsResponse.data[index], style: TextStyle(fontSize: 12.0, fontWeight: FontWeight.bold)),
    );
  }

  getTimeSlots() {
    setState(() {
      isDataLoaded = false;
    });
    APIService service = new APIService();
    EasyLoading.show(status: CommonService.getLoadingMessage(), maskType: EasyLoadingMaskType.black);
    service.getDoctorSlots(widget.doc.id, widget.selectedHospital.id, _selectedDateValue.toString().split(" ")[0], context).then((res) {
      setState(() {
        doctorSlotsResponse = DoctorSlotsResponse.fromJson(res);
        print(doctorSlotsResponse.data);
        isDataLoaded = true;
        EasyLoading.dismiss();
      });
    }).catchError((err) {
      EasyLoading.dismiss();
      print(err);
    });
  }

  openWorkAddresses(context) {
    Navigator.push(context, SlideUpPageRoute(widget: WorkingAddresses(hospitalsData: widget.doc.hospitalsData, isHospitalSelect: true), fullscreenDialog: true)).then((value) {
      if (value != null) {
        print(value.name);
        setState(() {
          widget.selectedHospital = value;
          getTimeSlots();
        });
      }
    });
  }

  bookAppointment() async {
    if (authUserProvider.getIsLogin()) {
      if (doctorSlotsResponse.data.length != 0) {
        if (widget.doc.videoSupport != 1) {
          bookAppointmentAPI(0);
        } else {
          showAppointmentDialog();
        }
      } else
        CommonService.showToast(context, "Please select a time slot to continue");
    } else {
      openLoginPage();
    }
  }

  Future<bool> askVideoCallPermission() async {
    if (!(await Permission.camera.request().isGranted) || !(await Permission.microphone.request().isGranted)) {
      return false;
    }
    if (!(await PlatformBridge.shared().isDrawOverAppsPermissionAllowed())) {
      await drawOverAppsMessageDialog(context);
      return false;
    }
    return true;
  }

  Future drawOverAppsMessageDialog(BuildContext context) async {
    await showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: Text("Please select 'MedZone' from the apps list and allow draw over app permission to continue."),
          contentPadding: EdgeInsets.fromLTRB(24.0, 20.0, 24.0, 0.0),
          actions: [
            FlatButton(
              child: Text("Cancel"),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            FlatButton(
              child: Text("Go to Settings"),
              onPressed: () async {
                await PlatformBridge.shared().askDrawOverAppsPermission();
                Navigator.pop(context);
              },
            )
          ],
        );
      },
    );
  }

  showAppointmentDialog() {
    showAnimatedDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return ClassicGeneralDialogWidget(
          titleText: 'Appointment type select',
          contentText: 'This doctor supports virtual consultation. Would you like to book a virtual consultation?',
          positiveText: "Yes",
          negativeText: "No",
          onPositiveClick: () async {
            Navigator.of(context).pop();
            if (Platform.isAndroid) {
              if (await askVideoCallPermission()) bookAppointmentAPI(1);
            } else {
              bookAppointmentAPI(1);
            }
          },
          onNegativeClick: () {
            Navigator.of(context).pop();
            bookAppointmentAPI(0);
          },
        );
      },
      animationType: DialogTransitionType.slideFromBottomFade,
      curve: Curves.fastOutSlowIn,
      duration: Duration(seconds: 1),
    );
  }

  bookAppointmentAPI(int isVirtual) async {
    var insuranceID = await CommonService.getPrefs(SELECTED_INSURANCE_COMPANY_ID, false);
    bool isError = false;
    String errorText = "";
    APIService service = new APIService();
    EasyLoading.show(status: CommonService.getLoadingMessage(), maskType: EasyLoadingMaskType.black);
    service.makeAppointment(widget.doc.id, selectedTime, _selectedDateValue.toString().split(" ")[0], "0", insuranceID, widget.selectedHospital.hospitalId, isVirtual, context).then((res) {
      setState(() {
        EasyLoading.dismiss();
        if (res.containsKey("response") && res['response'] == "true") {
          isError = false;
          CommonService.showToast(context, "Appointment booked successfully");
          openBookSuccess(isVirtual);
        } else {
          isError = true;
          errorText = res['data'];
          CommonService.showToast(context, errorText);
        }
      });
    }).catchError((err) {
      EasyLoading.dismiss();
      print(err);
      CommonService.showToast(context, err);
    });
  }

  openLoginPage() {
    Navigator.push(context, FadePage(page: LoginFormPage(isAppointmentLogin: true))).then((value) {
      if (authUserProvider.getIsLogin()) {
        bookAppointment();
        // if (widget.doc.videoSupport != 1)
        //   bookAppointmentAPI(0);
        // else
        //   showAppointmentDialog();
      }
    });
  }

  openBookSuccess(int isVirtual) {
    Navigator.push(
        context,
        FadePage(
            page: BookSuccess(
          dateStr: getFormattedDate(),
          timeStr: selectedTimeBookSuccess,
          isVirtual: isVirtual,
          doc: widget.doc,
          selectedHospital: widget.selectedHospital,
        )));
  }

  setTodayDate() {
    setState(() {
      _controller.animateToDate(DateTime.now());
    });
  }

  String getFormattedDate() {
    String formattedDate;
    setState(() {
      formattedDate = DateUtil.getMonthDayYearDateFormatted(_selectedDateValue);
    });
    return formattedDate;
  }
}
