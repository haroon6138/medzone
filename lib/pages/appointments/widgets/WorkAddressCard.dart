import 'dart:math' show cos, sqrt, asin;

import 'package:MedZone/models/currentLocation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class WorkAddressCard extends StatefulWidget {
  dynamic hospitalsData;
  bool isHospitalSelect = false;
  var currentLat;
  var currentLong;

  WorkAddressCard(
      {this.hospitalsData,
      this.isHospitalSelect,
      this.currentLat,
      this.currentLong});

  @override
  _WorkAddressCardState createState() => _WorkAddressCardState();
}

class _WorkAddressCardState extends State<WorkAddressCard> {
  CurrentLocation locationProvider;

  @override
  Widget build(BuildContext context) {
    locationProvider = Provider.of<CurrentLocation>(context);
    return InkWell(
      onTap: () {
        if (widget.isHospitalSelect) {
          backToAppointments();
        } else {
          var url = 'https://www.google.com/maps/dir/?api=1&destination=' +
              widget.hospitalsData.latlong.split(",")[0] +
              ',' +
              widget.hospitalsData.latlong.split(",")[1] +
              '&origin=' +
              widget.currentLat.toString() +
              ',' +
              widget.currentLong.toString() +
              '&travelmode=driving';
          launch(url);
        }
      },
      child: Card(
        margin: EdgeInsets.fromLTRB(5.0, 10.0, 5.0, 8.0),
        color: Colors.white,
        elevation: 3.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        child: Container(
          decoration: BoxDecoration(),
          padding: EdgeInsets.all(10.0),
          width: MediaQuery.of(context).size.width,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width * 0.5,
                    child: Text(widget.hospitalsData.name,
                        overflow: TextOverflow.clip,
                        style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.grey[700],
                            letterSpacing: 0.7)),
                  ),
                  Row(
                    children: [
                      Text(
                          calculateDistance(
                                      locationProvider.currentLatitude,
                                      locationProvider.currentLongitude,
                                      double.parse(widget.hospitalsData.latlong
                                          .split(",")[0]),
                                      double.parse(widget.hospitalsData.latlong
                                          .split(",")[1]))
                                  .toString() +
                              " mi",

                          // widget.hospitalsData.distance + " km away",
                          style: TextStyle(
                              fontSize: 12.0,
                              color: Colors.grey[700],
                              letterSpacing: 0.7)),
                      Container(
                        margin: EdgeInsets.fromLTRB(5.0, 0.0, 5.0, 0.0),
                        child: Icon(Icons.directions,
                            color: Color(0xff17AF6B).withOpacity(1.0),
                            size: 25.0),
                      ),
                    ],
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.only(top: 10.0),
                child: Text(widget.hospitalsData.address1,
                    style: TextStyle(
                        fontSize: 14.0,
                        color: Colors.grey[700],
                        letterSpacing: 0.7)),
              ),
              Container(
                margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
                child: Text("Mon - Fri: 09:00 - 06:00\nSat: 09:00 - 02:00",
                    style: TextStyle(
                        fontSize: 14.0,
                        color: Colors.grey[900],
                        letterSpacing: 0.7)),
              ),
            ],
          ),
        ),
      ),
    );
  }

  int calculateDistance(lat1, lon1, lat2, lon2) {
    double distance;
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
    distance = 12742 * asin(sqrt(a));
    distance  = distance / 1.6;
    return distance.floor();
  }

  backToAppointments() {
    if (widget.isHospitalSelect) {
      Navigator.of(context).pop(widget.hospitalsData);
    }
  }
}
