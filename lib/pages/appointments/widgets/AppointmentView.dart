import 'package:MedZone/models/response/AppointmentsResponse.dart';
import 'package:MedZone/utils/confirm_dialog.dart';
import 'package:MedZone/utils/date_uitl.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class AppointmentView extends StatefulWidget {
  @override
  _AppointmentViewState createState() => _AppointmentViewState();

  bool isShowAddress = true;
  AppointmentData appointmentData;
  final Function cancelFunction;

  AppointmentView(
      {@required this.appointmentData, @required this.cancelFunction});
}

class _AppointmentViewState extends State<AppointmentView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: widget.appointmentData.doctor != null ? Card(
        margin: EdgeInsets.fromLTRB(5.0, 10.0, 5.0, 8.0),
        color: Colors.white,
        elevation: 3.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        child: Container(
          decoration: BoxDecoration(),
          width: MediaQuery.of(context).size.width,
          child: IntrinsicHeight(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Container(
                    decoration: BoxDecoration(
                      // borderRadius: BorderRadius.circular(10),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10.0),
                          bottomLeft: Radius.circular(10.0)),
                      color: new Color(0xff17AF6B).withOpacity(1.0),
                    ),
                    constraints: BoxConstraints.expand(),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(getParsedDate(widget.appointmentData.date),
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 16.0)),
                        Container(
                          margin: EdgeInsets.only(top: 5.0),
                          child: Text(
                              getParsedTime(widget.appointmentData.time),
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15.0)),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  flex: 4,
                  child: Container(
                    padding: EdgeInsets.all(5.0),
                    child: Row(
                      children: [
                        Container(
                          margin: EdgeInsets.fromLTRB(3.0, 0.0, 10.0, 0.0),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(100.0),
                            child:
                                widget.appointmentData.doctor.avatarUrl != null
                                    ? Image.network(
                                        widget.appointmentData.doctor.avatarUrl,
                                        fit: BoxFit.fill,
                                        height: 50.0,
                                        width: 50.0)
                                    : new Image.asset(
                                        "assets/images/medzone_logo.png",
                                        fit: BoxFit.fill,
                                        height: 50.0,
                                        width: 50.0),
                          ),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                                "Dr. " +
                                    widget.appointmentData.doctor.firstName +
                                    " " +
                                    widget.appointmentData.doctor.lastName,
                                style: TextStyle(
                                    fontSize: 15.0,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.grey[700],
                                    letterSpacing: 0.5)),
                            Container(
                              margin: EdgeInsets.only(top: 3.0),
                              child: Text("Primary Care",
                                  style: TextStyle(
                                      fontSize: 14.0,
                                      color: Colors.grey[800],
                                      letterSpacing: 1.0)),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 3.0, bottom: 0.0),
                              child: Text(
                                  widget.appointmentData.isVirtual == 1
                                      ? "Virtual Appointment"
                                      : "Walk-In Appointment",
                                  style: TextStyle(
                                      fontSize: 13.0,
                                      fontWeight: FontWeight.bold,
                                      color: new Color(0xff17AF6B)
                                          .withOpacity(1.0),
                                      letterSpacing: 1.0)),
                            ),
                            widget.isShowAddress
                                ? Container(
                                    margin:
                                        EdgeInsets.only(top: 3.0, bottom: 10.0),
                                    child: Text(
                                        widget.appointmentData.hospital != null
                                            ? widget
                                                .appointmentData.hospital.name
                                            : "Hospital",
                                        style: TextStyle(
                                            fontSize: 12.0,
                                            color: Colors.grey[600],
                                            letterSpacing: 1.0)),
                                  )
                                : Container(),
                            InkWell(
                              onTap: () {
                                cancelAppointment();
                              },
                              child: widget.appointmentData.expired == 0
                                  ? Container(
                                      margin: EdgeInsets.only(top: 10.0),
                                      child: Text('Cancel Appointment',
                                          // textAlign: TextAlign.end,
                                          style: TextStyle(
                                              color: Colors.red,
                                              fontWeight: FontWeight.bold,
                                              decoration:
                                                  TextDecoration.underline,
                                              fontSize: 12.0,
                                              letterSpacing: 0.8)),
                                    )
                                  : Container(),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ) : Container(),
    );
  }

  cancelAppointment() {
    ConfirmDialog dialog = new ConfirmDialog(
        context: context,
        confirmMessage: "Are you sure you want to cancel this appointment?",
        okText: "Confirm",
        cancelText: "Cancel",
        okFunction: () => {
              Navigator.pop(context),
              widget.cancelFunction(widget.appointmentData.id)
            },
        cancelFunction: () => {});
    dialog.showAlertDialog(context);
  }

  String getParsedDate(String date) {
    if(date != null) {
      DateTime parsedDate = DateTime.parse(date);
      return parsedDate.day.toString() +
          " " +
          DateUtil.getMonth(parsedDate.month);
    } else
      return "";
  }

  getParsedTime(String date) {
    if(date != null) {
      var df = DateFormat("HH:mm:SS"); // h:mma
      var dt = df.parse(date);
      return DateFormat('h:mm a').format(dt);
    } else
      return "";
  }
}
