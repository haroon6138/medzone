import 'dart:math' show cos, sqrt, asin;

import 'package:MedZone/models/currentLocation.dart';
import 'package:MedZone/pages/appointments/DoctorProfile.dart';
import 'package:MedZone/utils/confirm_dialog.dart';
import 'package:MedZone/utils/transitions/fade_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:rating_bar/rating_bar.dart';

class DoctorView extends StatelessWidget {
  dynamic doc;
  bool isMyDoctor = false;
  final Function removeFunction;

  DoctorView(
      {this.isClickable = true,
      this.isShowAddress = true,
      this.doc,
      this.isMyDoctor,
      this.removeFunction});

  bool isClickable;
  bool isShowAddress;
  CurrentLocation locationProvider;

  @override
  Widget build(BuildContext context) {
    locationProvider = Provider.of<CurrentLocation>(context);
    return InkWell(
      onTap: () {
        getDoctorsProfile(doc, context);
      },
      child: Card(
        margin: EdgeInsets.fromLTRB(5.0, 10.0, 5.0, 8.0),
        color: Colors.white,
        elevation: 3.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        child: Container(
          decoration: BoxDecoration(),
          padding: EdgeInsets.all(10.0),
          width: MediaQuery.of(context).size.width,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 1,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(100.0),
                  child: doc.avatarUrl != null ? Image.network(doc.avatarUrl,
                      fit: BoxFit.fill, height: 70.0, width: 45.0) : new Image.asset("assets/images/medzone_logo.png", fit: BoxFit.fill, height: 70.0, width: 45.0),
                ),
              ),
              Expanded(
                flex: 4,
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.55,
                  margin: EdgeInsets.fromLTRB(20.0, 0.0, 0.0, 0.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                              "Dr. " +
                                  doc.firstName +
                                  " " +
                                  doc.lastName +
                                  ", MD",
                              style: TextStyle(
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey[700],
                                  letterSpacing: 1.0)),
                          isShowAddress && doc.latlong != null
                              ? Text(
                                  calculateDistance(
                                              locationProvider.currentLatitude,
                                              locationProvider.currentLongitude,
                                              double.parse(
                                                  doc.latlong.split(",")[0]),
                                              double.parse(
                                                  doc.latlong.split(",")[1]))
                                          .toString() +
                                      " mi",
                                  style: TextStyle(
                                      fontSize: 12.0,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.grey[500],
                                      letterSpacing: 1.0))
                              : Container(),
                        ],
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 3.0),
                        child: Text(
                            isMyDoctor && doc.speciality != null
                                ? doc.speciality.name
                                : "Primary Care",
                            style: TextStyle(
                                fontSize: 14.0,
                                color: Colors.grey[800],
                                letterSpacing: 1.0)),
                      ),
                      isShowAddress
                          ? Container(
                              margin: EdgeInsets.only(top: 3.0),
                              child: Text(doc.hospitalsData[0].hospitalAddress,
                                  style: TextStyle(
                                      fontSize: 12.0,
                                      color: Colors.grey[600],
                                      letterSpacing: 1.0)),
                            )
                          : Container(),
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(top: 5.0),
                            child: RatingBar.readOnly(
                              initialRating: doc.rating != null
                                  ? doc.rating.toDouble()
                                  : 5.0,
                              size: 20.0,
                              filledColor: Colors.yellow[700],
                              emptyColor: Colors.grey[500],
                              isHalfAllowed: true,
                              halfFilledIcon: Icons.star_half,
                              filledIcon: Icons.star,
                              emptyIcon: Icons.star,
                            ),
                          ),
                          // Container(
                          //   margin: EdgeInsets.only(
                          //       top: 3.0, left: 3.0, right: 3.0),
                          //   child: Text("(161)",
                          //       style: TextStyle(
                          //           fontSize: 12.0,
                          //           color: Colors.grey[600],
                          //           letterSpacing: 1.0)),
                          // ),
                          doc.videoSupport != 0 ? Container(
                            margin: EdgeInsets.only(
                                top: 3.0, left: 3.0, right: 3.0),
                            child: SvgPicture.asset("assets/images/video_camera.svg",
                                width: 25.0, height: 30.0),
                          ) : Container(),
                        ],
                      ),
                      isMyDoctor
                          ? InkWell(
                              onTap: () {
                                removeDoctor(context);
                              },
                              child: Container(
                                margin: EdgeInsets.only(top: 15.0),
                                child: Text('Remove',
                                    // textAlign: TextAlign.end,
                                    style: TextStyle(
                                        color: Colors.red,
                                        fontWeight: FontWeight.bold,
                                        decoration: TextDecoration.underline,
                                        fontSize: 12.0,
                                        letterSpacing: 0.8)),
                              ),
                            )
                          : Container(),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  removeDoctor(BuildContext context) {
    ConfirmDialog dialog = new ConfirmDialog(
        context: context,
        confirmMessage: "Are you sure you want to remove this doctor?",
        okText: "Confirm",
        cancelText: "Cancel",
        okFunction: () => {Navigator.pop(context), removeFunction(doc.id)},
        cancelFunction: () => {});
    dialog.showAlertDialog(context);
  }

  int calculateDistance(lat1, lon1, lat2, lon2) {
    double distance;
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
    distance = 12742 * asin(sqrt(a));
    distance  = distance / 1.6;
    return distance.floor();
  }

  String getDoctorSpeciality(List<String> docSpecial) {
    String docSpeciality = "";
    docSpecial.forEach((v) {
      docSpeciality = docSpeciality + v + "\n";
    });
    return docSpeciality;
  }

  getDoctorsProfile(dynamic doc, context) {
    if (isClickable)
      Navigator.push(context, FadePage(page: DoctorProfile(doc: doc)));
  }
}
