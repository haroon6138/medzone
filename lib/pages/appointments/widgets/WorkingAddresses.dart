import 'package:MedZone/models/currentLocation.dart';
import 'package:MedZone/pages/appointments/widgets/WorkAddressCard.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class WorkingAddresses extends StatefulWidget {
  List<dynamic> hospitalsData;
  bool isHospitalSelect = false;

  WorkingAddresses({this.hospitalsData, this.isHospitalSelect});

  @override
  _WorkingAddressesState createState() => _WorkingAddressesState();
}

class _WorkingAddressesState extends State<WorkingAddresses> {

  CurrentLocation locationProvider;

  @override
  Widget build(BuildContext context) {
    locationProvider = Provider.of<CurrentLocation>(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: new Color(0xff17AF6B).withOpacity(1.0),
        elevation: 0.0,
        centerTitle: true,
        title: Text("Appointment Locations",
            style: TextStyle(
              fontSize: 16.0,
              fontWeight: FontWeight.bold,
              letterSpacing: 0.5,
              fontFamily: 'Open-Sans',
            )),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        padding: EdgeInsets.all(10.0),
        child: ListView.builder(
          itemCount: widget.hospitalsData.length,
          itemBuilder: (context, index) {
            return WorkAddressCard(hospitalsData: widget.hospitalsData[index], isHospitalSelect: widget.isHospitalSelect, currentLat: locationProvider.currentLatitude, currentLong: locationProvider.currentLongitude);
          },
        ),
      ),
    );
  }
}
