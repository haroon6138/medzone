import 'package:MedZone/custom_widget/circle_image.dart';
import 'package:MedZone/custom_widget/write_review.dart';
import 'package:MedZone/models/TopFindingsModel.dart';
import 'package:MedZone/models/response/DoctorReviewsResponse.dart';
import 'package:MedZone/services/CommonServices.dart';
import 'package:MedZone/services/Covid-Services.dart';
import 'package:MedZone/utils/date_uitl.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:rating_bar/rating_bar.dart';

class DoctorReviews extends StatefulWidget {
  dynamic doc;

  DoctorReviews({@required this.doc});

  @override
  _DoctorReviewsState createState() => _DoctorReviewsState();
}

class _DoctorReviewsState extends State<DoctorReviews> {
  topFindings topFindingsObj;

  bool isDataLoaded = false;
  DoctorReviewsResponse doctorReviewsResponse;

  @override
  void initState() {
    topFindingsObj = new topFindings(id: 10, type: "pharmacy");

    WidgetsBinding.instance.addPostFrameCallback((_) {
      getDoctorReviews();
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: new Color(0xff17AF6B).withOpacity(1.0),
        elevation: 0.0,
        centerTitle: true,
        title: Text("Reviews",
            style: TextStyle(
              fontSize: 16.0,
              fontWeight: FontWeight.bold,
              letterSpacing: 0.5,
              fontFamily: 'Open-Sans',
            )),
      ),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(children: <Widget>[
                Expanded(
                  child: Container(
                    margin: EdgeInsets.only(
                        top: 20.0, bottom: 10.0, left: 40.0, right: 40.0),
                    height: 50.0,
                    width: MediaQuery.of(context).size.width * 0.9,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          side: BorderSide(
                              color: new Color(0xffFF2B56).withOpacity(1.0))),
                      color: new Color(0xffFF2B56).withOpacity(1.0),
                      onPressed: () {
                        showGeneralDialog(
                            barrierColor: Colors.black.withOpacity(0.5),
                            transitionBuilder: (context, a1, a2, widget) {
                              final curvedValue =
                                  Curves.easeInOutBack.transform(a1.value) -
                                      1.0;
                              return Transform(
                                transform: Matrix4.translationValues(
                                    0.0, curvedValue * 200, 0.0),
                                child: Opacity(
                                  opacity: a1.value,
                                  child: WriteReview(
                                      topFindingsObj: topFindingsObj),
                                ),
                              );
                            },
                            transitionDuration: Duration(milliseconds: 500),
                            barrierDismissible: true,
                            barrierLabel: '',
                            context: context,
                            pageBuilder: (context, animation1, animation2) {});
                      },
                      child: Column(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(top: 12.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Text('Write Your Review',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18.0,
                                        letterSpacing: 0.8)),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ]),
              isDataLoaded
                  ? Container(
                      height: MediaQuery.of(context).size.height,
                      padding: EdgeInsets.all(10.0),
                      child: ListView.builder(
                        itemCount: doctorReviewsResponse.data.length,
                        itemBuilder: (context, index) {
                          return Card(
                            margin: EdgeInsets.fromLTRB(5.0, 10.0, 5.0, 8.0),
                            color: Colors.white,
                            elevation: 2.0,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Container(
                              child: Column(
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Column(
                                        children: <Widget>[
                                          Container(
                                            padding: EdgeInsets.fromLTRB(
                                                5.0, 5.0, 5.0, 5.0),
                                            child: Card(
                                              margin: EdgeInsets.fromLTRB(
                                                  8.0, 0.0, 8.0, 0.0),
                                              shape: RoundedRectangleBorder(
                                                side: BorderSide(
                                                    color: Colors.white70,
                                                    width: 1),
                                                borderRadius:
                                                    BorderRadius.circular(50),
                                              ),
                                              child: CircleImage(
                                                  image:
                                                      "assets/images/medzone_logo.png",
                                                  width: 75.0,
                                                  height: 75.0),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Container(
                                                padding:
                                                    EdgeInsets.only(top: 5.0),
                                                child: RatingBar.readOnly(
                                                  size: 25.0,
                                                  filledColor: Colors.amber,
                                                  emptyColor: Colors.amber,
                                                  halfFilledColor: Colors.amber,
                                                  initialRating:
                                                      doctorReviewsResponse
                                                          .data[index].rating
                                                          .toDouble(),
                                                  isHalfAllowed: true,
                                                  halfFilledIcon:
                                                      Icons.star_half,
                                                  filledIcon: Icons.star,
                                                  emptyIcon: Icons.star_border,
                                                ),
                                              ),
                                            ],
                                          ),
                                          Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Container(
                                                padding: EdgeInsets.all(0.0),
                                                margin: EdgeInsets.fromLTRB(
                                                    5.0, 0.0, 0.0, 0.0),
                                                child: Text(
                                                  doctorReviewsResponse
                                                          .data[index]
                                                          .customer
                                                          .firstName +
                                                      " " +
                                                      doctorReviewsResponse
                                                          .data[index]
                                                          .customer
                                                          .lastName,
                                                  textAlign: TextAlign.start,
                                                  style: TextStyle(
                                                      fontSize: 16,
                                                      color: Colors.black87,
                                                      letterSpacing: 0.1,
                                                      fontFamily: 'Open-Sans',
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                              ),
                                            ],
                                          ),
                                          Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Container(
                                                margin: EdgeInsets.fromLTRB(
                                                    5.0, 0.0, 0.0, 0.0),
                                                padding: EdgeInsets.all(0.0),
                                                child: Text(
                                                  doctorReviewsResponse
                                                              .data[index]
                                                              .createdAt !=
                                                          null
                                                      ? getDateString(
                                                          doctorReviewsResponse
                                                              .data[index]
                                                              .createdAt)
                                                      : "1 day ago",
                                                  textAlign: TextAlign.start,
                                                  style: TextStyle(
                                                      fontSize: 13,
                                                      color: Colors.grey[500],
                                                      letterSpacing: 0.1,
                                                      fontFamily: 'Open-Sans'),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  Row(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Container(
                                            padding: const EdgeInsets.all(10.0),
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.90,
                                            child: Text(doctorReviewsResponse
                                                .data[index].review),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                    )
                  : Container(),
            ],
          ),
        ),
      ),
    );
  }

  getDoctorReviews() {
    APIService service = new APIService();
    EasyLoading.show(
        status: CommonService.getLoadingMessage(),
        maskType: EasyLoadingMaskType.black);
    service.getDoctorReviews(widget.doc.id, context).then((res) {
      setState(() {
        print(res);
        doctorReviewsResponse = DoctorReviewsResponse.fromJson(res);
        isDataLoaded = true;
        EasyLoading.dismiss();
      });
    }).catchError((err) {
      EasyLoading.dismiss();
      print(err);
    });
  }

  getDateString(String dateStr) {
    DateTime dateTime = DateTime.parse(dateStr);
    print(dateTime);
    return DateUtil.getMonthDayYearDateFormatted(dateTime);
  }
}
