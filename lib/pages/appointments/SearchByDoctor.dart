import 'package:MedZone/pages/appointments/widgets/DoctorView.dart';
import 'package:flutter/material.dart';

class SearchByDoctor extends StatefulWidget {
  @override
  _SearchByDoctorState createState() => _SearchByDoctorState();
}

class _SearchByDoctorState extends State<SearchByDoctor> {
  final mySearchDoctorController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: new Color(0xffF1F2F2).withOpacity(1.0),
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        elevation: 0.0,
        centerTitle: true,
        title: Text("MedZone",
            style: TextStyle(
              fontSize: 16.0,
              fontWeight: FontWeight.bold,
              letterSpacing: 0.5,
              fontFamily: 'Open-Sans',
            )),
        backgroundColor: new Color(0xff17AF6B).withOpacity(1.0),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        padding: EdgeInsets.fromLTRB(20, 10, 16, 16),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              new Color(0xff17AF6B).withOpacity(1.0),
              new Color(0xffF1F2F2).withOpacity(1.0),
            ],
            stops: [0.2, 0.6],
          ),
        ),
        child: SingleChildScrollView(
          child: Container(
            width: MediaQuery.of(context).size.width,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: [
                Container(
                  margin: EdgeInsets.only(bottom: 5.0, top: 5.0),
                  child: Text("Let's make you an appointment:",
                      textAlign: TextAlign.center,
                      overflow: TextOverflow.clip,
                      style: TextStyle(
                          fontSize: 18.0,
                          letterSpacing: 0.5,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Open-Sans-bold',
                          color: Colors.white)),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10.0),
                  child: TextField(
                    onTap: () {
                      print("Clicked");
                    },
                    readOnly: false,
                    showCursor: false,
                    controller: mySearchDoctorController,
                    decoration: InputDecoration(
                      isDense: true,
                      contentPadding: EdgeInsets.all(15),
                      hintText: 'Search Doctor',
                      prefixIcon: Icon(Icons.search,
                          size: 30.0, color: Colors.grey[700]),
                      hintStyle: TextStyle(color: Colors.grey[700]),
                      filled: true,
                      fillColor: Colors.white,
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(8.0)),
                        borderSide: BorderSide(color: Colors.white, width: 2),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(8.0)),
                        borderSide: BorderSide(color: Colors.white, width: 2),
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 5.0, top: 30.0),
                  child: Text("Our Top Rated Doctors:",
                      overflow: TextOverflow.clip,
                      style: TextStyle(
                          fontSize: 20.0,
                          letterSpacing: 0.5,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Open-Sans-bold',
                          color: Colors.white)),
                ),
                Container(
                  height: MediaQuery.of(context).size.height,
//                  padding: EdgeInsets.all(10.0),
                  child: ListView.builder(
                    itemCount: 10,
                    itemBuilder: (context, index) {
                      return DoctorView();
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
