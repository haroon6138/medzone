import 'package:MedZone/models/AuthUserProvider.dart';
import 'package:MedZone/pages/appointments/AppointmentSchedule.dart';
import 'package:MedZone/pages/appointments/DoctorReviews.dart';
import 'package:MedZone/pages/appointments/widgets/WorkingAddresses.dart';
import 'package:MedZone/pages/loginFormPage.dart';
import 'package:MedZone/services/CommonServices.dart';
import 'package:MedZone/services/Covid-Services.dart';
import 'package:MedZone/utils/transitions/fade_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:rating_bar/rating_bar.dart';
import 'package:share/share.dart';

class DoctorProfile extends StatefulWidget {
  dynamic doc;

  DoctorProfile({this.doc});

  @override
  _DoctorProfileState createState() => _DoctorProfileState();
}

class _DoctorProfileState extends State<DoctorProfile> {
  AuthUserProvider authUserProvider;

  @override
  Widget build(BuildContext context) {
    authUserProvider = Provider.of<AuthUserProvider>(context);

    return Scaffold(
      backgroundColor: new Color(0xffF1F2F2).withOpacity(1.0),
      appBar: AppBar(
        actions: [
          Center(
              child: InkWell(
            onTap: () {
              shareDoctor();
            },
            child: Container(
                margin: EdgeInsets.only(right: 10.0),
                child: Text("Share",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        letterSpacing: 0.8,
                        fontSize: 14.0))),
          )),
          Center(
              child: InkWell(
            onTap: () {
              favouriteDoctor();
            },
            child: Container(
                margin: EdgeInsets.only(right: 10.0),
                child: Icon(Icons.favorite_border)),
          )),
        ],
        elevation: 0.0,
        centerTitle: true,
        title: Text(
            "Dr. " + widget.doc.firstName + " " + widget.doc.lastName + ", MD",
            style: TextStyle(
              fontSize: 16.0,
              fontWeight: FontWeight.bold,
              letterSpacing: 0.5,
              fontFamily: 'Open-Sans',
            )),
        backgroundColor: new Color(0xff17AF6B).withOpacity(1.0),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 15.0),
                child: Center(
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(100.0),
                    child: widget.doc.avatarUrl != null ? Image.network(widget.doc.avatarUrl,
                        fit: BoxFit.fill, height: 120.0, width: 120.0) : new Image.asset("assets/images/medzone_logo.png", fit: BoxFit.fill, height: 120.0, width: 120.0),
                  ),
                ),
              ),
              Container(
                child: Text(
                    "Dr. " +
                        widget.doc.firstName +
                        " " +
                        widget.doc.lastName +
                        ", MD",
                    style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.grey[800],
                        letterSpacing: 1.0)),
              ),
              Container(
                margin: EdgeInsets.only(top: 5.0),
                child: Text("Primary Care",
                    style: TextStyle(
                        fontSize: 16.0,
                        color: Colors.grey[800],
                        letterSpacing: 1.0)),
              ),
              Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 5.0),
                    child: RatingBar.readOnly(
                      initialRating: widget.doc.rating != null
                          ? widget.doc.rating.toDouble()
                          : 5.0,
                      size: 25.0,
                      filledColor: Colors.yellow[700],
                      emptyColor: Colors.grey[500],
                      isHalfAllowed: true,
                      halfFilledIcon: Icons.star_half,
                      filledIcon: Icons.star,
                      emptyIcon: Icons.star,
                    ),
                  ),
                  widget.doc.video != 0 ? Container(
                    margin: EdgeInsets.only(
                        top: 3.0, left: 15.0, right: 0.0),
                    child: SvgPicture.asset("assets/images/video_camera.svg",
                        width: 25.0, height: 30.0),
                  ) : Container(),
                ],
              ),
              Divider(
                height: 20.0,
                color: Colors.grey[300],
                thickness: 3.0,
              ),
              Container(
                alignment: Alignment.centerLeft,
                margin: EdgeInsets.only(top: 5.0, left: 10.0, right: 10.0),
                child: Text("Professional Summary",
                    style: TextStyle(
                        fontSize: 16.0,
                        color: Colors.grey[800],
                        fontWeight: FontWeight.bold,
                        letterSpacing: 1.0)),
              ),
              Container(
                margin: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                alignment: Alignment.centerLeft,
                child: Text(
                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin euismod lobortis diam, a posuere risus faucibus vitae. Sed sed consequat mi. Donec non metus a lacus mattis commodo vel in ante. Phasellus sodales hendrerit aliquet. Integer pretium risus nec ligula commodo facilisis. Maecenas finibus, velit eu interdum lobortis, quam ligula consectetur libero, et luctus dui urna at ipsum. Fusce ut sem sollicitudin, pretium dui nec, finibus nibh. Vestibulum pellentesque leo vel est bibendum semper. Fusce molestie sapien et hendrerit ullamcorper. In felis nibh, ornare pharetra pharetra ac, sollicitudin a ipsum. Quisque commodo vel metus vel aliquet. Aenean dictum leo sagittis, rhoncus eros nec, condimentum urna. Sed sit amet sapien non quam lobortis dictum. Ut non iaculis nisi. Morbi ac iaculis neque. Fusce sollicitudin ante non neque venenatis, et sagittis risus maximus. Aliquam dolor magna, feugiat vel dapibus at, auctor placerat dui. Fusce et mi sed libero bibendum feugiat vel non augue. In accumsan orci at ligula luctus tempor.",
                  overflow: TextOverflow.ellipsis,
                  maxLines: 6,
                  softWrap: false,
                  style: TextStyle(fontSize: 14.0, letterSpacing: 0.6),
                ),
              ),
              Divider(
                height: 25.0,
                color: Colors.grey[300],
                thickness: 2.0,
              ),
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: [
                      Container(
                        margin:
                            EdgeInsets.only(top: 0.0, left: 10.0, right: 5.0),
                        child: SvgPicture.asset("assets/images/doctor-icon.svg",
                            width: 45.0, height: 45.0),
                      ),
                      Container(
                        margin:
                            EdgeInsets.only(top: 0.0, left: 5.0, right: 5.0),
                        child: Text("Personal Information",
                            style: TextStyle(
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.grey[800],
                                letterSpacing: 1.0)),
                      ),
                    ],
                  ),
                  Container(
                    alignment: Alignment.centerRight,
                    margin: EdgeInsets.only(top: 0.0, left: 10.0, right: 20.0),
                    child: Icon(Icons.arrow_forward_ios,
                        color: Colors.grey, size: 20.0),
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.only(left: 53.0),
                child: Divider(
                  height: 25.0,
                  color: Colors.grey[300],
                  thickness: 2.0,
                ),
              ),
              InkWell(
                onTap: () {
                  gotoWorkAddresses(context);
                },
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: [
                        Container(
                          margin:
                              EdgeInsets.only(top: 0.0, left: 10.0, right: 5.0),
                          child: SvgPicture.asset(
                              "assets/images/appo-locations.svg",
                              width: 45.0,
                              height: 45.0),
                        ),
                        Container(
                          margin:
                              EdgeInsets.only(top: 0.0, left: 5.0, right: 5.0),
                          child: Text("Appointment Locations",
                              style: TextStyle(
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey[800],
                                  letterSpacing: 1.0)),
                        ),
                      ],
                    ),
                    Container(
                      alignment: Alignment.centerRight,
                      margin:
                          EdgeInsets.only(top: 0.0, left: 10.0, right: 20.0),
                      child: Icon(Icons.arrow_forward_ios,
                          color: Colors.grey, size: 20.0),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 53.0),
                child: Divider(
                  height: 25.0,
                  color: Colors.grey[300],
                  thickness: 2.0,
                ),
              ),
              InkWell(
                onTap: () {
                  gotoReviews(context);
                },
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: [
                        Container(
                          margin:
                              EdgeInsets.only(top: 0.0, left: 10.0, right: 5.0),
                          child: SvgPicture.asset("assets/images/star.svg",
                              width: 45.0, height: 45.0),
                        ),
                        Container(
                          margin:
                              EdgeInsets.only(top: 0.0, left: 5.0, right: 5.0),
                          child: Text("Patient Reviews",
                              style: TextStyle(
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey[800],
                                  letterSpacing: 1.0)),
                        ),
                      ],
                    ),
                    Container(
                      alignment: Alignment.centerRight,
                      margin:
                          EdgeInsets.only(top: 0.0, left: 10.0, right: 20.0),
                      child: Icon(Icons.arrow_forward_ios,
                          color: Colors.grey, size: 20.0),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 53.0),
                child: Divider(
                  height: 25.0,
                  color: Colors.grey[300],
                  thickness: 2.0,
                ),
              ),
              SizedBox(
                height: 100.0,
              ),
            ],
          ),
        ),
      ),
      bottomSheet: Row(children: <Widget>[
        Expanded(
          child: Container(
            margin: EdgeInsets.only(
                top: 10.0, bottom: 20.0, left: 10.0, right: 10.0),
            height: 55.0,
            width: MediaQuery.of(context).size.width * 0.9,
            child: RaisedButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                  side: BorderSide(
                      color: new Color(0xffFF2B56).withOpacity(1.0))),
              color: new Color(0xffFF2B56).withOpacity(1.0),
              onPressed: () {
                goAppointmentSchedule(context);
              },
              child: Column(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 14.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text('Book Appointment',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 18.0,
                                letterSpacing: 0.8)),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ]),
    );
  }

  favouriteDoctor() {
    if (authUserProvider.getIsLogin()) {
      APIService service = new APIService();
      EasyLoading.show(
          status: CommonService.getLoadingMessage(),
          maskType: EasyLoadingMaskType.black);
      service.addFavouriteDoctor(widget.doc.id, context).then((res) {
        setState(() {
          EasyLoading.dismiss();
        });
      }).catchError((err) {
        EasyLoading.dismiss();
        print(err);
      });
    } else {
      openLoginPage();
    }
  }

  openLoginPage() {
    Navigator.push(
            context, FadePage(page: LoginFormPage(isAppointmentLogin: true)))
        .then((value) {
      if (authUserProvider.getIsLogin()) {
        favouriteDoctor();
      }
    });
  }

  shareDoctor() {
    Share.share(
        'Dr. John Doe, MD on MedZone - https://medzone.app.link/lzz3KQNL0ab');
  }

  gotoWorkAddresses(context) {
    Navigator.push(
        context,
        FadePage(
            page: WorkingAddresses(hospitalsData: widget.doc.hospitalsData, isHospitalSelect: false)));
  }

  gotoReviews(context) {
    Navigator.push(context, FadePage(page: DoctorReviews(doc: widget.doc)));
  }

  goAppointmentSchedule(context) {
    Navigator.push(
        context,
        FadePage(
            page: AppointmentSchedule(
                doc: widget.doc,
                selectedHospital: widget.doc.hospitalsData[0])));
  }
}
