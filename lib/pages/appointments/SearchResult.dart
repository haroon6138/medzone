import 'package:MedZone/models/currentLocation.dart';
import 'package:MedZone/models/response/SearchDoctorsResponse.dart';
import 'package:MedZone/pages/appointments/SearchResultMap.dart';
import 'package:MedZone/pages/appointments/widgets/DoctorView.dart';
import 'package:MedZone/utils/transitions/fade_page.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SearchResult extends StatefulWidget {
  SearchDoctorsResponse searchDoctorsResponse;

  SearchResult({this.searchDoctorsResponse});

  @override
  _SearchResultState createState() => _SearchResultState();
}

class _SearchResultState extends State<SearchResult> {
  var locationProvider;

  @override
  Widget build(BuildContext context) {
    locationProvider = Provider.of<CurrentLocation>(context);
    return Scaffold(
      backgroundColor: new Color(0xffF1F2F2).withOpacity(1.0),
      appBar: AppBar(
        actions: [
          Center(
              child: InkWell(
            onTap: () {
              navigateToMaps();
            },
            child: Container(
                margin: EdgeInsets.only(right: 10.0),
                child: Text("Map",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        letterSpacing: 0.8,
                        fontSize: 15.0))),
          ))
        ],
        elevation: 0.0,
        centerTitle: true,
        title: Text("Doctors",
            style: TextStyle(
              fontSize: 16.0,
              fontWeight: FontWeight.bold,
              letterSpacing: 0.5,
              fontFamily: 'Open-Sans',
            )),
        backgroundColor: new Color(0xff17AF6B).withOpacity(1.0),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        padding: EdgeInsets.all(10.0),
        child: ListView.builder(
          itemCount: widget.searchDoctorsResponse.data.length,
          itemBuilder: (context, index) {
            return DoctorView(doc: widget.searchDoctorsResponse.data[index], isMyDoctor: false,);
          },
        ),
      ),
    );
  }

  navigateToMaps() {
    Navigator.push(
        context,
        FadePage(
            page: SearchResultMap(locationProvider.getCurrentLat(),
                locationProvider.getCurrentLong())));
  }
}
