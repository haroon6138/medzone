import 'package:MedZone/models/IncomingCallData.dart';
import 'package:MedZone/models/conference/room_model.dart';
import 'package:MedZone/pages/conference/widgets/platform_exception_alert_dialog.dart';
import 'package:MedZone/pages/homePage.dart';
import 'package:flutter/material.dart';
import 'package:just_audio/just_audio.dart';

import 'conference_page.dart';

class IncomingCall extends StatefulWidget {
  IncomingCallData incomingCallData;

  IncomingCall({@required this.incomingCallData});

  @override
  _IncomingCallState createState() => _IncomingCallState();
}

class _IncomingCallState extends State<IncomingCall>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;

  final player = AudioPlayer();

  @override
  void initState() {
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 500));

    WidgetsBinding.instance.addPostFrameCallback((_) => _runAnimation());
    super.initState();
  }

  @override
  void dispose() {
    _animationController.dispose();
    player.stop();
    disposeAudioResources();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          decoration: BoxDecoration(color: Colors.grey[700]),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(top: 30.0),
                alignment: Alignment.center,
                child: Text("Incoming Video Call",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 26.0,
                        color: Colors.white,
                        letterSpacing: 1.0)),
              ),
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.fromLTRB(50.0, 30.0, 50.0, 20.0),
                child: Image.asset(
                    'assets/images/medzone_logo.png', width: 100),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 0.0),
                child: Divider(
                  color: Colors.white,
                  thickness: 1.0,
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 20.0),
                alignment: Alignment.center,
                child: Text(widget.incomingCallData.doctorname,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 22.0,
                        fontWeight: FontWeight.bold,
                        letterSpacing: 0.8,
                        color: Colors.white)),
              ),
              Container(
                margin: EdgeInsets.only(top: 10.0),
                alignment: Alignment.center,
                child: Text(widget.incomingCallData.clinicname,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 22.0,
                        letterSpacing: 0.8,
                        color: Colors.white)),
              ),
              Container(
                margin: EdgeInsets.only(top: 10.0),
                alignment: Alignment.center,
                child: Text(widget.incomingCallData.speciality,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 22.0,
                        letterSpacing: 0.8,
                        color: Colors.white)),
              ),
              Container(
                decoration: BoxDecoration(
                  color: Colors.grey[900].withOpacity(0.8),
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                ),
                padding: EdgeInsets.all(20.0),
                margin: EdgeInsets.only(top: 20.0),
                child: Column(
                  children: <Widget>[
                    Text("Appointment Information",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                            letterSpacing: 1.0,
                            color: Colors.white)),
                    Container(
                      margin: EdgeInsets.only(top: 20.0),
                      child: Text(
                          widget.incomingCallData.appointmentdate +
                              " " +
                              widget.incomingCallData.appointmenttime,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 20.0,
                              letterSpacing: 1.0,
                              color: Colors.white)),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 20.0),
                      child: Text(widget.incomingCallData.clinicname,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 20.0,
                              letterSpacing: 1.0,
                              color: Colors.white)),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 100.0),
                alignment: Alignment.center,
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    RotationTransition(
                        turns: Tween(begin: 0.0, end: -.1)
                            .chain(CurveTween(curve: Curves.elasticIn))
                            .animate(_animationController),
                        child: Container(
                          child: RawMaterialButton(
                            onPressed: () {
                              _submit();
                            },
                            elevation: 2.0,
                            fillColor: Colors.green,
                            child: Icon(
                              Icons.call,
                              color: Colors.white,
                              size: 35.0,
                            ),
                            padding: EdgeInsets.all(15.0),
                            shape: CircleBorder(),
                          ),
                        )),
                    Container(
                      child: RawMaterialButton(
                        onPressed: () {
                          HomePageWidget.isOpenCallPage = false;
                          backToHome();
                        },
                        elevation: 2.0,
                        fillColor: Colors.red,
                        child: Icon(
                          Icons.call_end,
                          color: Colors.white,
                          size: 35.0,
                        ),
                        padding: EdgeInsets.all(15.0),
                        shape: CircleBorder(),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          )),
    );
  }

  void _runAnimation() async {
    setAudioFile();
    for (int i = 0; i < 100; i++) {
      await _animationController.forward();
      await _animationController.reverse();
    }
  }

  Future<void> _submit() async {
    backToHome();
    try {
      final roomModel = RoomModel(
          // name: "TestRoom",
          // token: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImN0eSI6InR3aWxpby1mcGE7dj0xIn0.eyJqdGkiOiJTSzFjNDlmMjhjZjAwODIyMzYwNDQxNTg5YjVjZmJhZjI5LTE2MjUwOTMyMDAiLCJpc3MiOiJTSzFjNDlmMjhjZjAwODIyMzYwNDQxNTg5YjVjZmJhZjI5Iiwic3ViIjoiQUM5YjQ3NDZjNzIwODc0NzA5YWQzMTQ2YjMyMDUyNmM1MyIsImV4cCI6MTYyNTA5NjgwMCwiZ3JhbnRzIjp7ImlkZW50aXR5IjoiUDEiLCJ2aWRlbyI6eyJyb29tIjoiVGVzdFJvb20ifX19.xMP8gZX7bFCCKQK9XcbnNWd1DzN2GY_QoS78hn4GxQo",
          // identity: "P1");
          name: widget.incomingCallData.name,
          token: widget.incomingCallData.sessionId,
          identity: widget.incomingCallData.identity);

      await Navigator.of(context).push(
        MaterialPageRoute<ConferencePage>(
          fullscreenDialog: true,
          builder: (BuildContext context) =>
              ConferencePage(roomModel: roomModel),
        ),
      );
    } catch (err) {
      print(err);
      await PlatformExceptionAlertDialog(
        exception: err,
      ).show(context);
    }
  }

  void backToHome() {
    player.stop();
//    disposeAudioResources();
    Navigator.of(context).pop();
  }

  disposeAudioResources() async {
    await player.dispose();
  }

  void setAudioFile() async {
    player.stop();
    await player.setVolume(1.0); // full volume
    try {
      await player.setAsset('assets/sounds/ring_60Sec.mp3').then((value) {
        player.setLoopMode(LoopMode.one); // loop ring sound
        player.play();
      }).catchError((err) {
        print("Error: $err");
      });
    } catch (e) {
      print("Error: $e");
    }
  }
}
