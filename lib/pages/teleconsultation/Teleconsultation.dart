import 'package:MedZone/custom_widget/nearest_maps_list_item.dart';
import 'package:MedZone/models/TopFindingsModel.dart';
import 'package:MedZone/models/currentLocation.dart';
import 'package:MedZone/models/response/AllNearestProvidersResponse.dart';
import 'package:MedZone/services/CommonServices.dart';
import 'package:MedZone/services/Covid-Services.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';

class Teleconsultation extends StatefulWidget {
  const Teleconsultation({@required this.lat, @required this.long});

  final double lat;
  final double long;

  @override
  _TeleconsultationState createState() => _TeleconsultationState();
}

class _TeleconsultationState extends State<Teleconsultation> with SingleTickerProviderStateMixin {
  static TabController _tabController;
  List<topFindings> pharList = [];
  List<topFindings> vetList = [];

  @override
  void initState() {
    _tabController = new TabController(length: 2, vsync: this);
    WidgetsBinding.instance.addPostFrameCallback((_) {
      getAllPharmacies();
    });
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final locationProvider = Provider.of<CurrentLocation>(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
          title: Text("Teleconsultation",
              style: TextStyle(
                fontSize: 16.0,
                fontWeight: FontWeight.bold,
                letterSpacing: 0.5,
                fontFamily: 'Open-Sans',
              )),
          backgroundColor: new Color(0xff17AF6B).withOpacity(1.0)),
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
            child: TabBar(
              onTap: (index) {
                setState(() {
                  if (index == 1) {
                    getAllVeterinarians();
                  } else {
                    getAllPharmacies();
                  }
                  print(index);
                });
              },
              indicatorColor: Color(0xff17AF6B),
              controller: _tabController,
              tabs: [
                Tab(child: Text("Pharmacies", style: TextStyle(color: Colors.black))),
                Tab(
                  child: Text("Veterinarians", style: TextStyle(color: Colors.black)),
                )
              ],
            ),
          ),
          Expanded(
            child: Container(
              child: TabBarView(
                physics: NeverScrollableScrollPhysics(),
                children: [
                  Container(
                    child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: pharList.length,
                      itemBuilder: (context, index) {
                        return Container(
                          padding: const EdgeInsets.all(12.0),
                          child: nearestMapsListItem(pharList[index], widget.lat, widget.long, isTeleconsultation: true),
                        );
                      },
                    ),
                  ),
                  Container(
                    child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: vetList.length,
                      itemBuilder: (context, index) {
                        return Container(
                          padding: const EdgeInsets.all(12.0),
                          child: nearestMapsListItem(vetList[index], widget.lat, widget.long, isTeleconsultation: true),
                        );
                      },
                    ),
                  )
                ],
                controller: _tabController,
              ),
            ),
          ),
        ],
      ),
    );
  }

  getAllPharmacies() {
    pharList.clear();
    APIService service = new APIService();
    EasyLoading.show(status: CommonService.getLoadingMessage(), maskType: EasyLoadingMaskType.black);
    AllNearestProvidersResponse allNearestProvidersResponse = new AllNearestProvidersResponse();
    service.getAllNearestProviders("pharmacies", widget.lat.toString(), widget.long.toString(), context).then((res) {
      allNearestProvidersResponse = AllNearestProvidersResponse.fromJson(res);
      allNearestProvidersResponse.result.forEach((element) {
        var topFindingsObj = new topFindings(
            id: element.id,
            type: "pharmacies",
            address: element.address1,
            image: element.logo == null ? "assets/images/medzone_logo.png" : element.logo,
            name: element.name,
            distance: element.distance,
            days: CommonService.getDays(element.days),
            timing: CommonService.getTimings(element.hours),
            deliveryTiming: "05:00 pm",
            contactNo: element.phoneNumber,
            webURL: "https://themedzone.com/",
            latitude: double.parse(element.latlong.split(",")[0]),
            longitude: double.parse(element.latlong.split(",")[1]),
            waitingTime: element.waitingTime,
            rating: element.rating.toDouble());
        pharList.add(topFindingsObj);
      });
      setState(() {

      });
      EasyLoading.dismiss();
    }).catchError((err) {
      print(err);
      EasyLoading.dismiss();
    });
  }

  getAllVeterinarians() {
    vetList.clear();
    APIService service = new APIService();
    EasyLoading.show(status: CommonService.getLoadingMessage(), maskType: EasyLoadingMaskType.black);
    AllNearestProvidersResponse allNearestProvidersResponse = new AllNearestProvidersResponse();
    service.getAllNearestProviders("veterinarians", widget.lat.toString(), widget.long.toString(), context).then((res) {
      allNearestProvidersResponse = AllNearestProvidersResponse.fromJson(res);
      allNearestProvidersResponse.result.forEach((element) {
        var topFindingsObj = new topFindings(
            id: element.id,
            type: "veterinarians",
            address: element.address1,
            image: element.logo == null ? "assets/images/medzone_logo.png" : element.logo,
            name: element.name,
            distance: element.distance,
            days: CommonService.getDays(element.days),
            timing: CommonService.getTimings(element.hours),
            deliveryTiming: "05:00 pm",
            contactNo: element.phoneNumber,
            webURL: "https://themedzone.com/",
            latitude: double.parse(element.latlong.split(",")[0]),
            longitude: double.parse(element.latlong.split(",")[1]),
            waitingTime: element.waitingTime,
            rating: element.rating.toDouble());
        vetList.add(topFindingsObj);
      });
      setState(() {

      });
      EasyLoading.dismiss();
    }).catchError((err) {
      print(err);
      EasyLoading.dismiss();
    });
  }
}
