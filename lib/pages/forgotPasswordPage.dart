import 'package:MedZone/pages/updatePasswordPage.dart';
import 'package:MedZone/services/CommonServices.dart';
import 'package:MedZone/services/Covid-Services.dart';
import 'package:MedZone/utils/sms-popup.dart';
import 'package:MedZone/utils/transitions/fade_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

class ForgotPasswordPage extends StatefulWidget {
  const ForgotPasswordPage({Key key}) : super(key: key);

  @override
  _ForgotPasswordPageState createState() => _ForgotPasswordPageState();
}

class _ForgotPasswordPageState extends State<ForgotPasswordPage> {
  final myEmailController = TextEditingController();
  final myOTPController = TextEditingController();

  bool isCodeSent = false;
  @override
  void initState() {
    // myEmailController.text = "haroon6138@gmail.com";
    // myOTPController.text = "CVB8045";
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final node = FocusScope.of(context);
    return Scaffold(
      appBar: AppBar(
          title: Text("Forgot Password",
              style: TextStyle(
                fontSize: 16.0,
                fontWeight: FontWeight.bold,
                letterSpacing: 0.5,
                fontFamily: 'Open-Sans',
              )),
          backgroundColor: new Color(0xff17AF6B).withOpacity(1.0)),
      body: Container(
          height: MediaQuery.of(context).size.height,
          margin: EdgeInsets.all(20.0),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: [
                Container(
                  margin: EdgeInsets.only(top: 10.0),
                  child: Text("We will send you a OTP code on your registered email address to verify you & reset your password.", style: TextStyle(fontSize: 16.0, letterSpacing: 0.4)),
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
                      width: MediaQuery.of(context).size.width * 0.89,
                      height: 80.0,
                      child: Row(
                        children: <Widget>[
                          new Flexible(
                            child: new TextField(
                              controller: myEmailController,
                              textInputAction: TextInputAction.done,
                              onSubmitted: (_) {
                                sendOTPCode();
                              },
                              decoration: const InputDecoration(
                                border: OutlineInputBorder(),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.green, width: 2.0),
                                ),
                                labelText: "Email Address",
                                labelStyle: TextStyle(color: Colors.grey),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                !isCodeSent
                    ? Container(
                        margin: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
                        height: 50.0,
                        width: MediaQuery.of(context).size.width * 0.89,
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0), side: BorderSide(color: new Color(0xffFF2B56).withOpacity(1.0))),
                          color: new Color(0xffFF2B56).withOpacity(1.0),
                          onPressed: () {
                            // verifyPhone();
                            sendOTPCode();
                            // showSMSDialog();
                          },
                          child: Column(
                            children: <Widget>[
                              Container(
                                transform: Matrix4.translationValues(0.0, 5.0, 0.0),
                                margin: EdgeInsets.only(top: 7.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text('Submit', overflow: TextOverflow.fade, style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 16.5, letterSpacing: 0.8)),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                    : Container(),
                isCodeSent
                    ? Container(
                        child: Column(
                          children: [
                            InkWell(
                              onTap: () {
                                sendOTPCode();
                              },
                              child: Container(
                                alignment: Alignment.center,
                                margin: EdgeInsets.only(top: 15.0, bottom: 20.0),
                                child: Text("Didn't receive OTP? Send again.", style: TextStyle(fontSize: 16.0, letterSpacing: 0.4, color: new Color(0xffFF2B56).withOpacity(1.0), fontWeight: FontWeight.bold, decoration: TextDecoration.underline)),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 10.0),
                              child: Text("An OTP code has been sent to the registered email address, Please enter it below.", style: TextStyle(fontSize: 16.0, letterSpacing: 0.4)),
                            ),
                            Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  margin: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
                                  width: MediaQuery.of(context).size.width * 0.89,
                                  height: 80.0,
                                  child: Row(
                                    children: <Widget>[
                                      new Flexible(
                                        child: new TextField(
                                          controller: myOTPController,
                                          textInputAction: TextInputAction.done,
                                          onSubmitted: (_) {
                                            verifyOTPCode();
                                            // navigateToUpdatePassword();
                                          },
                                          decoration: const InputDecoration(
                                            border: OutlineInputBorder(),
                                            focusedBorder: OutlineInputBorder(
                                              borderSide: BorderSide(color: Colors.green, width: 2.0),
                                            ),
                                            labelText: "Verification Code",
                                            labelStyle: TextStyle(color: Colors.grey),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            Container(
                              margin: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
                              height: 50.0,
                              width: MediaQuery.of(context).size.width * 0.89,
                              child: RaisedButton(
                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0), side: BorderSide(color: new Color(0xffFF2B56).withOpacity(1.0))),
                                color: new Color(0xffFF2B56).withOpacity(1.0),
                                onPressed: () {
                                  // verifyOTPCode();
                                  navigateToUpdatePassword();
                                },
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      transform: Matrix4.translationValues(0.0, 5.0, 0.0),
                                      margin: EdgeInsets.only(top: 7.0),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          Text('Verify', overflow: TextOverflow.fade, style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 16.5, letterSpacing: 0.8)),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      )
                    : Container()
              ],
            ),
          )),
    );
  }

  verifyOTPCode() {
    if (myOTPController.text.isEmpty) {
      CommonService.showToast(context, "Please enter OTP Code.");
    } else {
      APIService service = new APIService();
      EasyLoading.show(status: CommonService.getLoadingMessage(), maskType: EasyLoadingMaskType.black);
      service.verifyOTPOnEmail(myEmailController.text.trim(), myOTPController.text.trim(), context).then((res) {
        setState(() {
          print(res);
          EasyLoading.dismiss();
          if (res.containsKey("success") && res['success'] == true) {
            navigateToUpdatePassword();
          } else {
            CommonService.showToast(context, res['message']);
          }
        });
      }).catchError((err) {
        EasyLoading.dismiss();
        print(err);
      });
    }
  }

  navigateToUpdatePassword() async {
    await Navigator.push(context, FadePage(page: UpdatePassword(userEmail: myEmailController.text)));
  }

  sendOTPCode() {
    if (myEmailController.text.isEmpty || !isEmail(myEmailController.text)) {
      CommonService.showToast(context, "Please enter correct email address");
    } else {
      APIService service = new APIService();
      EasyLoading.show(status: CommonService.getLoadingMessage(), maskType: EasyLoadingMaskType.black);
      service.sendOTPOnEmail(myEmailController.text.trim(), context).then((res) {
        setState(() {
          print(res);
          EasyLoading.dismiss();
          if (res.containsKey("success") && res['success'] == true) {
            CommonService.showToast(context, res['message']);
            isCodeSent = true;
          } else {
            CommonService.showToast(context, res['message']);
            isCodeSent = false;
          }
        });
      }).catchError((err) {
        EasyLoading.dismiss();
        print(err);
      });
    }
  }

  void showSMSDialog() {
    new SMSOTP(
      context,
      (value) {
        // submit(model, value);
      },
      () => {
        // Navigator.pop(context),
      },
    ).displayDialog(context);
  }

  bool isEmail(String string) {
    if (string == null || string.isEmpty) {
      return true;
    }
    const pattern = r'^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$';
    final regExp = RegExp(pattern);
    if (!regExp.hasMatch(string)) {
      return false;
    }
    return true;
  }
}
