import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class NoLocation extends StatefulWidget {
  @override
  _NoLocationState createState() => _NoLocationState();
}

class _NoLocationState extends State<NoLocation> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return new Future(() => false);
      },
      child: Scaffold(
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/grey_bg.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Center(
                child: SvgPicture.asset('assets/images/no_location_icon.svg',
                    width: 120.0, height: 120.0),
              ),
              Container(
                  margin: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 20.0),
                  child: Text("noLocation",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: new Color(0xff17AF6B).withOpacity(1.0),
                              fontSize: 20,
                              fontFamily: 'Open-Sans-bold',
                              fontWeight: FontWeight.bold))
                      .tr()),
              Container(
                margin: EdgeInsets.fromLTRB(40.0, 20.0, 40.0, 20.0),
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                      side: BorderSide(
                          color: new Color(0xff17AF6B).withOpacity(1.0))),
                  color: new Color(0xff17AF6B).withOpacity(1.0),
                  onPressed: () {
//                  navigateToHomePage(context);
                  },
                  child: Container(
                    margin: EdgeInsets.only(top: 0.0, left: 25.0),
                    height: 40.0,
                    child: Row(
//                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          child: Image.asset(
                              "assets/images/location_pin_white.png",
                              width: 30.0,
                              height: 30.0,
                              fit: BoxFit.contain),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 30.0),
                          child: Text('openSettings',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18.0,
                                      letterSpacing: 0.8))
                              .tr(),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  onWillPop() {}
}
