import 'package:MedZone/custom_widget/medicine_card.dart';
import 'package:MedZone/models/response/SearchMedicineResponse.dart';
import 'package:MedZone/models/response/TopMedicinesResponse.dart';
import 'package:MedZone/services/CommonServices.dart';
import 'package:MedZone/services/Covid-Services.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';


import 'medicineSearchList.dart';

class searchMedicine extends StatefulWidget {
  TopMedicinesResponse topMedicinesResponse;

  SearchMedicineResponse searchMedicinesResponse;

  bool isDataLoaded = false;

  @override
  _searchMedicineState createState() => _searchMedicineState();
}

class _searchMedicineState extends State<searchMedicine> {
  final searchQueryController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    widget.isDataLoaded = false;
    widget.topMedicinesResponse = new TopMedicinesResponse();
    widget.searchMedicinesResponse = new SearchMedicineResponse();
    WidgetsBinding.instance
        .addPostFrameCallback((_) => getTopMedicines(context));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: new Color(0xffF1F2F2).withOpacity(1.0),
      appBar: AppBar(
        title: Text("searchMedHeading",
            style: TextStyle(
              fontSize: 16.0,
              fontWeight: FontWeight.bold,
              letterSpacing: 0.5,
              fontFamily: 'Open-Sans',
            )).tr(),
        backgroundColor: new Color(0xff17AF6B).withOpacity(1.0),
      ),
      body: Container(
        margin: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              child: _getInputWidget(),
            ),
            Container(
              margin: EdgeInsets.only(top: 20.0),
              child: Text("topSearches",
                      style: TextStyle(
                          fontSize: 18,
                          fontFamily: 'Open-Sans',
                          fontWeight: FontWeight.bold,
                          letterSpacing: 1.0,
                          color: Colors.black))
                  .tr(),
            ),
            widget.isDataLoaded
                ? Container(
                    height: MediaQuery.of(context).size.height * 0.7,
                    child: ListView.builder(
                      itemCount: widget.topMedicinesResponse.medDataList.length,
                      itemBuilder: (context, index) {
                        return MedicineCard(
                            widget.topMedicinesResponse.medDataList[index],
                            'assets/images/link_Arrow.svg',
                            'yes');
                      },
                    ),
                  )
                : Container(),
          ],
        ),
      ),
    );
  }

  _getInputWidget() {
    if (context.locale.toString() == 'en') {
      return new Flexible(
        child: new TextField(
          textInputAction: TextInputAction.search,
          onSubmitted: (text1) {
            _getSearchedText(context);
          },
          controller: searchQueryController,
          decoration: const InputDecoration(
            prefixIcon: Icon(Icons.search),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              borderSide: BorderSide(color: Colors.green, width: 2.0),
            ),
            labelText: "Type Medicine Name",
            labelStyle: TextStyle(color: Colors.grey),
          ),
        ),
      );
    } else {
      return new Flexible(
        child: new TextField(
          textInputAction: TextInputAction.search,
          onSubmitted: (text1) {
            print(text1);
          },
          controller: searchQueryController,
          decoration: const InputDecoration(
            prefixIcon: Icon(Icons.search),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              borderSide: BorderSide(color: Colors.green, width: 2.0),
            ),
            labelText: "Escriba el nombre del medicamento",
            labelStyle: TextStyle(color: Colors.grey),
          ),
        ),
      );
    }
  }

  getTopMedicines(context) {
    APIService service = new APIService();
    EasyLoading.show(status: CommonService.getLoadingMessage(), maskType: EasyLoadingMaskType.black);
    service.getTopMedicines(context).then((res) {
      setState(() {
        widget.topMedicinesResponse = TopMedicinesResponse.fromJson(res);
        widget.isDataLoaded = true;
        EasyLoading.dismiss();
      });
    }).catchError((err) {
      EasyLoading.dismiss();
      print(err);
    });
  }

  Future navigateToSearchMedicineList(
      context, SearchMedicineResponse searchMedicinesResponse) async {
    await Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => MedicineSearchList(
              searchMedicinesResponse: searchMedicinesResponse)),
    ).then((value) {
      getTopMedicines(context);
    });
  }

  _getSearchedText(context) {
    APIService service = new APIService();
    EasyLoading.show(status: CommonService.getLoadingMessage(), maskType: EasyLoadingMaskType.black);
    service.getSearchMedicine(searchQueryController.text, context).then((res) {
      setState(() {
        widget.searchMedicinesResponse = SearchMedicineResponse.fromJson(res);
        print(widget.searchMedicinesResponse.data[0].name);
        EasyLoading.dismiss();
        navigateToSearchMedicineList(context, widget.searchMedicinesResponse);
      });
    }).catchError((err) {
      EasyLoading.dismiss();
      print(err);
    });
  }
}
