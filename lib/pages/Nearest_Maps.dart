import 'dart:typed_data';

import 'package:MedZone/custom_widget/nearest_maps_list_item.dart';
import 'package:MedZone/models/TopFindingsModel.dart';
import 'package:MedZone/models/currentLocation.dart';
import 'package:MedZone/services/CommonServices.dart';
import 'package:MedZone/services/CustomTextStyles.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

import 'dart:ui' as ui;


class NearestMaps extends StatelessWidget {
  NearestMaps(this.currentLat, this.currentLong, this.typeName, this.items);

  final currentLat;
  final currentLong;
  final typeName;
  final List<topFindings> items;

  @override
  Widget build(BuildContext context) {
    return NearestMapsState(currentLat, currentLong, typeName, items);
  }
}

class NearestMapsState extends StatefulWidget {
  NearestMapsState(
      this.currentLat, this.currentLong, this.typeName, this.items);

  final currentLat;
  final currentLong;
  final typeName;
  List<topFindings> items;

  @override
  _NearestMapsStateState createState() => _NearestMapsStateState();
}

class _NearestMapsStateState extends State<NearestMapsState> {
  static LatLng _initialPosition;
  BitmapDescriptor pinLocationIcon;
  Set<Marker> _markers = {};
  Iterable markers = [];
  PanelController _pc = new PanelController();
  ScrollController _scrollController;
  MarkerId currentShownId = null;

  @override
  void initState() {
    super.initState();
    _initialPosition = LatLng(widget.currentLat, widget.currentLong);
    print(_initialPosition);
//    setCustomMapPin();
  }

  Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(), targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png)).buffer.asUint8List();
  }

  GoogleMapController mapController;
  List<topFindings> sortedItems = List<topFindings>();

  void _onMapCreated(GoogleMapController controller) async {

    final Uint8List markerIcon = await getBytesFromAsset('assets/images/map_pin_icon_green.png', (MediaQuery.of(context).size.width * 0.25).toInt());
//    final Marker marker = Marker(icon: BitmapDescriptor.fromBytes(markerIcon));

    setState(() {
      pinLocationIcon = BitmapDescriptor.fromBytes(markerIcon);
    });

    mapController = controller;
    mapController.setMapStyle(CommonService.mapStyle);

    setState(() {
      for (var i = 0; i < widget.items.length; i++) {
        LatLng position =
            LatLng(widget.items[i].latitude, widget.items[i].longitude);
        _markers.add(Marker(
            markerId: MarkerId("Pharmacy$i"),
            onTap: () {
              print(widget.items[i].name);
              setState(() {
                sortedItems.add(widget.items[i]);
                widget.items.removeAt(i);
                for (var i = 0; i < widget.items.length; i++) {
                  sortedItems.add(widget.items[i]);
                }
                widget.items = [];
                widget.items = sortedItems;
                sortedItems = [];
                print(widget.items.length);
                _scrollController.jumpTo(0.0);
                _pc.close();
                resetMarkersOnMap();
              });
            },
            infoWindow: InfoWindow(title: widget.items[i].name),
            position: position,
            draggable: false,
            consumeTapEvents: false,
            icon: pinLocationIcon));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final locationProvider = Provider.of<CurrentLocation>(context);
    return Scaffold(
      appBar: AppBar(
        title: _getTopHeading(),
        backgroundColor: new Color(0xff17AF6B).withOpacity(1.0),
      ),
      body: Stack(
        children: <Widget>[
          GoogleMap(
            zoomControlsEnabled: true,
            myLocationButtonEnabled: true,
            myLocationEnabled: true,
            onMapCreated: _onMapCreated,
            padding: EdgeInsets.only(bottom: 90.0),
            markers: Set.from(_markers),
            initialCameraPosition: CameraPosition(
              target: LatLng(locationProvider.getCurrentLat(),
                  locationProvider.getCurrentLong()),
              zoom: 12.0,
            ),
          ),
          SlidingUpPanel(
            controller: _pc,
            minHeight: 190.0,
            panelBuilder: (ScrollController sc) => _scrollingList(sc),
          )
        ],
      ),
    );
  }

  resetMarkersOnMap() {
    _markers.clear();
    for (var i = 0; i < widget.items.length; i++) {
      LatLng position =
          LatLng(widget.items[i].latitude, widget.items[i].longitude);
      _markers.add(Marker(
          markerId: MarkerId("Pharmacy$i"),
          onTap: () {
            print(widget.items[i].name);
            setState(() {
              sortedItems.add(widget.items[i]);
              widget.items.removeAt(i);
              for (var i = 0; i < widget.items.length; i++) {
                sortedItems.add(widget.items[i]);
              }
              widget.items = [];
              widget.items = sortedItems;
              sortedItems = [];
              print(widget.items.length);
              _scrollController.jumpTo(0.0);
              _pc.close();
            });
          },
          infoWindow: InfoWindow(title: widget.items[i].name),
          position: position,
          draggable: false,
          consumeTapEvents: false,
          icon: pinLocationIcon));
    }
  }

  animateCameraToPosition(
      GoogleMapController mapController, double lat, double long, int index) {
    mapController
        .animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(target: LatLng(lat, long), zoom: 16.0),
      ),
    )
        .then((value) {
      if (currentShownId != null) {
        mapController.hideMarkerInfoWindow(currentShownId);
      }

      MarkerId id = new MarkerId("Pharmacy$index");
      mapController.showMarkerInfoWindow(id);
      currentShownId = id;
      _pc.close();
    });
  }

  Widget _scrollingList(ScrollController sc) {
    _scrollController = sc;
    return ListView.builder(
      controller: sc,
      itemCount: widget.items.length,
      itemBuilder: (BuildContext context, int i) {
        return GestureDetector(
          onTap: () {
            print(widget.items[i].name);
            animateCameraToPosition(mapController, widget.items[i].latitude,
                widget.items[i].longitude, i);

            setState(() {
              sortedItems.add(widget.items[i]);
              widget.items.removeAt(i);
              for (var i = 0; i < widget.items.length; i++) {
                sortedItems.add(widget.items[i]);
              }
              widget.items = [];
              widget.items = sortedItems;
              sortedItems = [];
              print(widget.items.length);
              _scrollController.jumpTo(0.0);
              _pc.close();
            });
          },
          child: Container(
            padding: const EdgeInsets.all(12.0),
            child: nearestMapsListItem(
                widget.items[i], widget.currentLat, widget.currentLong),
          ),
        );
      },
    );
  }

  _getTopHeading() {
    print(widget.typeName);
    switch (widget.typeName) {
      case "Pharmacy":
        return Text('pharmaciesNearYou',
                style: CustomTextStyle.display5(context))
            .tr(args: [widget.items.length.toString()]);
        break;
      case "Urgent Care":
        return Text('urgentCareNearYou',
                style: CustomTextStyle.display5(context))
            .tr(args: [widget.items.length.toString()]);
        break;
      case "Hospital":
        return Text('hospitalsNearYou',
                style: CustomTextStyle.display5(context))
            .tr(args: [widget.items.length.toString()]);
        break;
      case "Harm Reduction":
        return Text('harmReductionNearYou',
                style: CustomTextStyle.display5(context))
            .tr(args: [widget.items.length.toString()]);
        break;
      case "Laboratory":
        return Text('labNearYou', style: CustomTextStyle.display5(context))
            .tr(args: [widget.items.length.toString()]);
        break;
      case "radiology_centres":
        return Text('radNearYou', style: CustomTextStyle.display5(context))
            .tr(args: [widget.items.length.toString()]);
        break;
      case "chiropractors":
        return Text('chiropractorNearYou',
                style: CustomTextStyle.display5(context))
            .tr(args: [widget.items.length.toString()]);
        break;
      case "physical_therapies":
        return Text('physicalTherapyNearYou',
                style: CustomTextStyle.display5(context))
            .tr(args: [widget.items.length.toString()]);
        break;
      case "veterinarians":
        return Text('veterinarianNearYou',
                style: CustomTextStyle.display5(context))
            .tr(args: [widget.items.length.toString()]);
        break;
      case "ophthalamologists":
        return Text('eyecareNearYou', style: CustomTextStyle.display5(context))
            .tr(args: [widget.items.length.toString()]);
        break;
    }
  }
}
