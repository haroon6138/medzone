import 'package:MedZone/models/currentLocation.dart';
import 'package:MedZone/services/CommonServices.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';

class SelectLocation extends StatelessWidget {
  SelectLocation(this.currentLat, this.currentLong, this.currentAddress);

  var currentLat;
  var currentLong;
  var currentAddress;

  getSelectedLocation() =>
      currentAddress.toString() +
      "/" +
      currentLat.toString() +
      "/" +
      currentLong.toString();

  @override
  Widget build(BuildContext context) {
    return SelectLocationPage(currentLat, currentLong, currentAddress);
  }
}

class SelectLocationPage extends StatefulWidget {
  SelectLocationPage(this.currentLat, this.currentLong, this.currentAddress);

  var currentLat;
  var currentLong;
  var currentAddress;

  @override
  _SelectLocationPageState createState() => _SelectLocationPageState();
}

class _SelectLocationPageState extends State<SelectLocationPage> {
  LatLng _initialPosition;
  GoogleMapController mapController;
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _initialPosition = LatLng(widget.currentLat, widget.currentLong);
  }

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
    mapController.setMapStyle(CommonService.mapStyle);
  }

  @override
  Widget build(BuildContext context) {
    final locationProvider = Provider.of<CurrentLocation>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("selectLocation",
            style: TextStyle(
              fontSize: 16.0,
              fontWeight: FontWeight.bold,
              letterSpacing: 0.5,
              fontFamily: 'Open-Sans',
            )).tr(),
        backgroundColor: new Color(0xff17AF6B).withOpacity(1.0),
      ),
      body: Stack(
        children: <Widget>[
          GoogleMap(
            zoomControlsEnabled: true,
            myLocationButtonEnabled: true,
            myLocationEnabled: true,
            onMapCreated: _onMapCreated,
            onCameraMove: (object) {
              widget.currentLat = object.target.latitude;
              widget.currentLong = object.target.longitude;
            },
            onCameraIdle: _getAddressFromLatLng,
            padding: EdgeInsets.only(bottom: 120.0),
            initialCameraPosition: CameraPosition(
              target: _initialPosition,
              zoom: 13.0,
            ),
          ),
          Center(
            child: Container(
              margin: EdgeInsets.only(bottom: 165.0),
              child: SvgPicture.asset('assets/images/map_pin_icon_green.svg',
                  width: 50.0, height: 50.0),
            ),
          ),
          Column(
            children: <Widget>[
              Expanded(
                child: Align(
                  alignment: FractionalOffset.bottomCenter,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Container(
                        color: Colors.white,
                        width: MediaQuery.of(context).size.width,
                        padding: EdgeInsets.only(top: 10.0),
                        height: 60.0,
                        child: Text("Location: " + widget.currentAddress,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 16,
                                color: Colors.black87,
                                letterSpacing: 0.5,
                                fontFamily: 'Open-Sans',
                                fontWeight: FontWeight.bold)),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: 70.0,
                        child: RaisedButton(
                          padding: const EdgeInsets.all(8.0),
                          textColor: Colors.white,
                          color: new Color(0xff17AF6B).withOpacity(1.0),
                          onPressed: () {
                            locationProvider
                                .setCurrentAddress(widget.currentAddress);
                            locationProvider.setCurrentLat(widget.currentLat);
                            locationProvider.setCurrentLong(widget.currentLong);

                            CommonService.setPrefsDouble(
                                CommonService.currentLat, widget.currentLat);
                            CommonService.setPrefsDouble(
                                CommonService.currentLong, widget.currentLong);

                            Navigator.pop(context);
                          },
                          child: new Text("selectLocation",
                                  style: TextStyle(
                                      fontSize: 16,
                                      color: Colors.white,
                                      letterSpacing: 0.5,
                                      fontFamily: 'Open-Sans',
                                      fontWeight: FontWeight.bold))
                              .tr(),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  _getAddressFromLatLng() async {
    try {
      List<Placemark> p = await geolocator.placemarkFromCoordinates(
          widget.currentLat, widget.currentLong);
      Placemark place = p[0];
      setState(() {
        widget.currentAddress =
            "${place.name}, ${place.locality}, ${place.subAdministrativeArea}, ${place.administrativeArea}";
      });
    } catch (e) {
      print(e);
    }
  }
}
