import 'package:MedZone/pages/covid-updates.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:geolocator/geolocator.dart';

class BottomButton extends StatelessWidget {
  String text;
  String image;
  String callBack;
  Placemark placeObject;

  BottomButton(
      {@required this.text, @required this.image, @required this.callBack, this.placeObject});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
      height: 60.0,
      child: RaisedButton(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
        color: Colors.white,
        onPressed: () {
          callback(context);
//          navigateToViewAll(context);
        },
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
              child: SvgPicture.asset(this.image,
                  width: 25.0, height: 25.0, fit: BoxFit.contain),
            ),
            Container(
              alignment: Alignment.bottomRight,
              margin: EdgeInsets.only(top: 3.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(this.text,
                          overflow: TextOverflow.clip,
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 12,
                              letterSpacing: 0)),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void callback(context) {
    switch (callBack) {
      case 'covid':
        navigateToCovidUpdates(context);
    }
  }

  Future navigateToCovidUpdates(context) async {
    await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => CovidUpdates(placeObject: placeObject)),
    );
  }
}
