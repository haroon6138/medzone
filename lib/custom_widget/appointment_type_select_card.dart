import 'package:MedZone/pages/appointments/SearchByClinic.dart';
import 'package:MedZone/pages/appointments/SearchByDoctor.dart';
import 'package:MedZone/utils/transitions/fade_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class AppointmentTypeSelectCard extends StatelessWidget {
  final String image;
  final String headingText;
  final String descText;
  final String searchType;

  AppointmentTypeSelectCard(
      {@required this.image,
      @required this.headingText,
      @required this.descText,
      @required this.searchType});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        navigateToSearchPage(context);
      },
      child: Card(
        margin: EdgeInsets.fromLTRB(8.0, 16.0, 8.0, 0.0),
        shape: RoundedRectangleBorder(
          side: BorderSide(color: Colors.white70, width: 1),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.all(15.0),
          child: Flex(
            direction: Axis.vertical,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                      margin: EdgeInsets.only(bottom: 15.0),
                      child:
                          SvgPicture.asset(image, width: 90.0, height: 90.0)),
                  Container(
                    margin: EdgeInsets.only(bottom: 5.0),
                    child: Text(headingText,
                        textAlign: TextAlign.center,
                        overflow: TextOverflow.clip,
                        style: TextStyle(
                            fontSize: 18.0,
                            letterSpacing: 0.5,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Open-Sans-bold',
                            color: Colors.black)),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 5.0),
                    child: Text(descText,
                        textAlign: TextAlign.center,
                        overflow: TextOverflow.clip,
                        style: TextStyle(
                            fontSize: 14.0,
                            letterSpacing: 0.5,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Open-Sans',
                            color: Colors.black)),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  navigateToSearchPage(BuildContext context) async {
    if (searchType == "clinic") {
      await Navigator.push(context, FadePage(page: SearchByClinic()));
    } else {
      await Navigator.push(context, FadePage(page: SearchByDoctor()));
    }
  }
}
