import 'package:flutter/material.dart';

class CircleImage extends StatelessWidget {
  CircleImage(
      {@required this.image, @required this.width, @required this.height});

  final String image;
  final width;
  final height;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: new Container(
          width: width,
          height: height,
          decoration: new BoxDecoration(
              shape: BoxShape.circle,
              image: new DecorationImage(
                  fit: BoxFit.cover,
                  image: image.contains("themedzone.com") ? new NetworkImage(image) : new AssetImage(image)))),
    );
  }
}
