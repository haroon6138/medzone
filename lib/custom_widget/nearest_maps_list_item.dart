import 'package:MedZone/models/TopFindingsModel.dart';
import 'package:MedZone/models/response/ReviewsResponse.dart';
import 'package:MedZone/pages/detail_page.dart';
import 'package:MedZone/services/CommonServices.dart';
import 'package:MedZone/services/Covid-Services.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_phone_direct_caller/flutter_phone_direct_caller.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:rating_bar/rating_bar.dart';

class nearestMapsListItem extends StatelessWidget {
  nearestMapsListItem(this.topFindingsObj, this.currentLat, this.currentLong, {this.isTeleconsultation = false});

  final topFindings topFindingsObj;
  final currentLat;
  final currentLong;
  bool isTeleconsultation;

  @override
  Widget build(BuildContext context) {
    return nearestMapsItem(topFindingsObj, currentLat, currentLong, isTeleconsultation);
  }
}

class nearestMapsItem extends StatefulWidget {
  nearestMapsItem(this.topFindingsObj, this.currentLat, this.currentLong, this.isTeleconsultation);

  final topFindings topFindingsObj;
  final currentLat;
  final currentLong;
  bool isTeleconsultation;

  @override
  _nearestMapsItemState createState() => _nearestMapsItemState();
}

class _nearestMapsItemState extends State<nearestMapsItem> {
  final imageSize = 50.0;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Container(
                    child: widget.topFindingsObj.image
                            .toString()
                            .contains("medzone_logo")
                        ? Image.asset(widget.topFindingsObj.image,
                            width: imageSize, height: imageSize)
                        : Image.network(widget.topFindingsObj.image,
                            width: imageSize, height: imageSize),
                  ),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width * 0.6,
                          margin: EdgeInsets.only(left: 10.0),
                          child: Text(
                            widget.topFindingsObj.name,
                            textAlign: TextAlign.start,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                fontSize: 16,
                                color: Colors.black87,
                                letterSpacing: 0.1,
                                fontFamily: 'Open-Sans',
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ]),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width * 0.6,
                        margin: EdgeInsets.only(left: 10.0),
                        child: Text(
                          widget.topFindingsObj.address,
                          textAlign: TextAlign.start,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              fontSize: 11,
                              color: Colors.grey[500],
                              letterSpacing: 0.1,
                              fontFamily: 'Open-Sans'),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(left: 8.0),
                        child: RatingBar.readOnly(
                          size: 25.0,
                          filledColor: Colors.amber,
                          emptyColor: Colors.amber,
                          halfFilledColor: Colors.amber,
                          initialRating: widget.topFindingsObj.rating,
                          isHalfAllowed: true,
                          halfFilledIcon: Icons.star_half,
                          filledIcon: Icons.star,
                          emptyIcon: Icons.star_border,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              !widget.isTeleconsultation ? Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      _callNumber(widget.topFindingsObj.contactNo);
                    },
                    child: Container(
                      margin: EdgeInsets.only(left: 40.0),
                      child: SvgPicture.asset(
                          'assets/images/green_phone_icon.svg'),
                    ),
                  ),
                ],
              ) : Container(),
            ],
          ),
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                margin: EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                            Container(
                                padding:
                                    EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0),
                                child: SvgPicture.asset(
                                    'assets/images/calender_green_icon.svg')),
                          ],
                        ),
                        Column(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0),
                              child: Text(
                                widget.topFindingsObj.days,
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.black87,
                                    letterSpacing: 0.1,
                                    fontFamily: 'Open-Sans'),
                              ),
                            ),
                          ],
                        ),
                        Column(
                          children: <Widget>[
                            Container(
                                padding:
                                    EdgeInsets.fromLTRB(40.0, 0.0, 0.0, 0.0),
                                child: SvgPicture.asset(
                                    'assets/images/clock_green_icon.svg')),
                          ],
                        ),
                        Column(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0),
                              child: Text(
                                widget.topFindingsObj.timing,
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.black87,
                                    letterSpacing: 0.1,
                                    fontFamily: 'Open-Sans'),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    (widget.topFindingsObj.type == "hospitals" ||
                            widget.topFindingsObj.type == "urgent_cares")
                        ? Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Column(
                                children: <Widget>[
                                  Container(
                                      padding: EdgeInsets.fromLTRB(
                                          10.0, 10.0, 0.0, 10.0),
                                      child: Text("Waiting Time: ",
                                          style: TextStyle(
                                              fontSize: 12,
                                              color: new Color(0xff17AF6B)
                                                  .withOpacity(1.0),
                                              fontWeight: FontWeight.bold,
                                              letterSpacing: 0.1,
                                              fontFamily: 'Open-Sans-bold'))),
                                ],
                              ),
                              Column(
                                children: <Widget>[
                                  Container(
                                    padding:
                                        EdgeInsets.fromLTRB(0, 10.0, 0.0, 0.0),
                                    child: Text(
                                      widget.topFindingsObj.waitingTime != null
                                          ? widget.topFindingsObj.waitingTime +
                                              " mins"
                                          : "N/A",
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          fontSize: 12,
                                          color: new Color(0xff17AF6B)
                                              .withOpacity(1.0),
                                          fontWeight: FontWeight.bold,
                                          letterSpacing: 0.1,
                                          fontFamily: 'Open-Sans-bold'),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          )
                        : Container(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        (widget.topFindingsObj.type == "pharmacies" && !widget.isTeleconsultation)
                            ? Container(
                                width: MediaQuery.of(context).size.width * 0.7,
                                margin: EdgeInsets.only(
                                    top: 0.0, left: 0.0, bottom: 5.0),
                                child: Text(
                                  "sameDelivery",
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                      fontSize: 12,
                                      color: new Color(0xff17AF6B)
                                          .withOpacity(1.0),
                                      letterSpacing: 0.0,
                                      fontFamily: 'Open-Sans-Black',
                                      fontWeight: FontWeight.bold),
                                ).tr(args: [
                                  widget.topFindingsObj.deliveryTiming
                                ]),
                              )
                            : Container(
                                width: MediaQuery.of(context).size.width * 0.7),
                        Container(
                          margin:
                              EdgeInsets.only(top: 0.0, left: 0.0, bottom: 5.0),
                          child: RaisedButton(
                            padding: const EdgeInsets.all(8.0),
                            textColor: Colors.white,
                            color: new Color(0xff17AF6B).withOpacity(1.0),
                            onPressed: () {
                              getProviderDetails(context);
                            },
                            child: new Text("DETAILS"),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
          Divider(
            color: Colors.grey[400],
          ),
        ],
      ),
    );
  }

  _callNumber(String number) async {
    await FlutterPhoneDirectCaller.callNumber(number);
  }

  getProviderDetails(context) {
    APIService service = new APIService();
    EasyLoading.show(status: CommonService.getLoadingMessage(), maskType: EasyLoadingMaskType.black);
    service
        .getProviderDetails(
            widget.topFindingsObj.id,
            widget.topFindingsObj.type,
            widget.currentLat,
            widget.currentLong,
            context)
        .then((res) {
      print(res);
      EasyLoading.dismiss();
      _showDetailsPage(res['result'], context);
    }).catchError((err) {
      print(err);
      EasyLoading.dismiss();
    });
  }

  _showDetailsPage(detailsResponse, context) {
    List<String> insurances = List();
    List<String> services = List();
    List<ReviewsResponse> reviewsList = List();

    detailsResponse['insurances'].forEach((element) {
      insurances.add(element['name']);
    });

    detailsResponse['services'].forEach((element) {
      services.add(element['name']);
    });

    detailsResponse['reviews'].forEach((element) {
      reviewsList.add(new ReviewsResponse.fromJson(element));
    });

    print(reviewsList.length);

    var topFindingsObj = new topFindings(
        id: widget.topFindingsObj.id,
        waitingTime: widget.topFindingsObj.waitingTime,
        type: widget.topFindingsObj.type,
        address: detailsResponse['address'],
        image: widget.topFindingsObj.image,
        featuredImage: detailsResponse['featured_image'] == null
            ? "assets/images/medzone_cover.png"
            : detailsResponse['featured_image'],
        name: widget.topFindingsObj.name,
        distance: widget.topFindingsObj.distance,
        days: widget.topFindingsObj.days,
        timing: widget.topFindingsObj.timing,
        deliveryTiming: widget.topFindingsObj.deliveryTiming,
        contactNo: widget.topFindingsObj.contactNo,
        webURL: detailsResponse['web_site_url'] == null ||
                detailsResponse['web_site_url'] == 'unknown'
            ? "https://themedzone.com"
            : detailsResponse['web_site_url'],
        latitude: widget.topFindingsObj.latitude,
        longitude: widget.topFindingsObj.longitude,
        rating: widget.topFindingsObj.rating,
        insurances: insurances,
        services: services,
        reviewsList: reviewsList);

    Navigator.push(context,
        MaterialPageRoute(builder: (context) => DetailPage(topFindingsObj)));
  }
}
