import 'package:MedZone/custom_widget/select_language.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';

class CustomRadio extends StatefulWidget {
  var currentLocale;

  CustomRadio({@required this.currentLocale});

  @override
  createState() {
    return new CustomRadioState();
  }
}

class CustomRadioState extends State<CustomRadio> {
  List<RadioModel> sampleData = new List<RadioModel>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    sampleData.add(new RadioModel(widget.currentLocale == 'en', 'english'));
    sampleData.add(new RadioModel(widget.currentLocale == 'es', 'spanish'));
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        ListView.builder(
          shrinkWrap: true,
          itemCount: sampleData.length,
          itemBuilder: (BuildContext context, int index) {
            return new InkWell(
              //highlightColor: Colors.red,
              splashColor: Colors.transparent,
              onTap: () {
                setState(() {
                  sampleData.forEach((element) => element.isSelected = false);
                  sampleData[index].isSelected = true;
                  SelectLanguage.selectedLanguage = sampleData[index].text;
                });
              },
              child: new RadioItem(sampleData[index]),
            );
          },
        ),
      ],
    );
  }
}

class RadioItem extends StatelessWidget {
  final RadioModel _item;

  RadioItem(this._item);

  @override
  Widget build(BuildContext context) {
    return new Container(
      margin: new EdgeInsets.all(15.0),
      child: new Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          new Container(
            height: 30.0,
            width: 30.0,
            child: new Center(
              child: Image.asset("assets/images/check_icon.png",
                  width: 15.0, height: 15.0),
            ),
            decoration: new BoxDecoration(
              color:
                  _item.isSelected ? new Color(0xff17AF6B) : Colors.transparent,
              border: new Border.all(
                  width: 1.0,
                  color:
                      _item.isSelected ? new Color(0xff17AF6B) : Colors.grey),
              borderRadius: const BorderRadius.all(const Radius.circular(50.0)),
            ),
          ),
          new Container(
            margin: new EdgeInsets.only(left: 15.0),
            child: new Text(_item.text, style: TextStyle(fontSize: 16.0)).tr(),
          ),
        ],
      ),
    );
  }
}

class RadioModel {
  bool isSelected;
  final String text;

  RadioModel(this.isSelected, this.text);
}
