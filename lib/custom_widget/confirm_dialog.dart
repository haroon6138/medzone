import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ConfirmDialog {
  final BuildContext context;

  final title;
  final confirmMessage;
  final okText;
  final cancelText;
  final Function okFunction;
  final Function cancelFunction;

  ConfirmDialog({@required this.context, this.title, @required this.confirmMessage, @required this.okText, @required this.cancelText, @required this.okFunction, @required this.cancelFunction});

  showAlertDialog(BuildContext context) {
    Dialog alert = Dialog(
      child: Container(
        color: Colors.transparent,
        child: Mdialog(
          context,
          title,
          confirmMessage,
          okText,
          cancelText,
          cancelFunction: cancelFunction,
          okFunction: okFunction,
        ),
      ),
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  static closeAlertDialog(BuildContext context) {
    Navigator.of(context).pop();
  }
}

class Mdialog extends StatelessWidget {
  String title;
  String description;
  final Function okFunction;
  final Function cancelFunction;
  final okText;
  final cancelText;
  BuildContext _context;

  Mdialog(this._context, this.title, this.description, this.okText, this.cancelText, {this.okFunction, this.cancelFunction});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: containerRadius(Colors.white, 12),
      padding: EdgeInsets.all(20),
      clipBehavior: Clip.antiAlias,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            // title != null ? title : TranslationBase.of(context).confirm,
            title,
            style: TextStyle(
              fontSize: 24,
              letterSpacing: -0.94,
              fontWeight: FontWeight.w600,
            ),
          ),
          Text(
            // this.confirmMessage,
            description,
            style: TextStyle(
              fontSize: 12,
              letterSpacing: -0.48,
              color: Color(0xFF848484),
              fontWeight: FontWeight.w600,
            ),
          ),
          mHeight(16),
          Row(
            children: [
              Expanded(
                child: InkWell(
                  onTap: () {
                    Navigator.of(_context).pop();
                    cancelFunction();
                  },
                  child: Container(
                    decoration: containerRadius(Color(0xFFE2E2E2), 12),
                    padding: EdgeInsets.only(top: 8,bottom: 8),
                    child: Center(child: Text(cancelText)),
                  ),
                ),
              ),
              mWidth(8),
              Expanded(
                child: InkWell(
                  onTap: okFunction,
                  child: Container(
                    decoration: containerRadius(Color(0xFF17AF6B), 12),
                    padding: EdgeInsets.only(top: 8,bottom: 8),
                    child: Center(
                      child: Text(
                        okText,
                        style: TextStyle(
                          color: Colors.white
                        ),
                      ),
                    ),
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}

Widget mHeight(double h) {
  return Container(
    height: h,
  );
}

Widget mWidth(double w) {
  return Container(
    width: w,
  );
}

Decoration containerRadius(Color background, double radius) {
  return BoxDecoration(
    color: background,
    border: Border.all(
        width: 1, //
        color: background //                  <--- border width here
    ),
    borderRadius: BorderRadius.circular(radius),
  );
}
