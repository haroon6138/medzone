import 'package:MedZone/models/response/MedicinePharmacyResult.dart';
import 'package:flutter/material.dart';

class MedicinePackageCard extends StatelessWidget {

  Packages packages;

  MedicinePackageCard({this.packages});

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.fromLTRB(8.0, 16.0, 8.0, 0.0),
      shape: RoundedRectangleBorder(
        side: BorderSide(color: Colors.white70, width: 1),
        borderRadius: BorderRadius.circular(10),
      ),
      child: Container(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.fromLTRB(11.0, 5.0, 10.0, 0.0),
                  child: Container(
                    child: Text("Quantity: " + packages.quantity.toString(),
                        style: TextStyle(
                            fontSize: 13,
                            fontFamily: 'Open-Sans-Black',
                            fontWeight: FontWeight.w900,
                            letterSpacing: 0.5,
                            color: new Color(0xff17AF6B).withOpacity(1.0))),
                  ),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.fromLTRB(11.0, 0.0, 10.0, 0.0),
                  child: Text("Price: " + "\$" + packages.price.toString(),
                      overflow: TextOverflow.fade,
                      style: TextStyle(
                          fontSize: 14,
                          fontFamily: 'Open-Sans',
                          fontWeight: FontWeight.bold,
                          letterSpacing: 0.5,
                          color: Colors.black)),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
