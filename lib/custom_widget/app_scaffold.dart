import 'package:MedZone/routes.dart';
import 'package:MedZone/services/CustomTextStyles.dart';
import 'package:flutter/material.dart';

class AppScaffold extends StatefulWidget {
  final title;
  final body;
  final bool showAppBar;
  int currentIndex = 0;

  AppScaffold(
      {this.title,
      @required this.body,
      @required this.showAppBar,
      this.currentIndex});

  @override
  _AppScaffoldState createState() => _AppScaffoldState();
}

class _AppScaffoldState extends State<AppScaffold> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: widget.showAppBar
          ? AppBar(
              title:
                  Text(widget.title, style: CustomTextStyle.display5(context)),
              backgroundColor: new Color(0xff17AF6B).withOpacity(1.0),
            )
          : null,
      body: widget.body,
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: widget.currentIndex,
        type: BottomNavigationBarType.fixed,
        selectedItemColor: new Color(0xff17AF6B).withOpacity(1.0),
        onTap: (value) {
          print(value);
          setState(() {
            widget.currentIndex = value;
            if (widget.currentIndex == 4) navigateToMyAccount(context);

            if (widget.currentIndex == 0) navigateToHome(context);
          });
        },
        // this will be set when a new tab is tapped
        items: [
          BottomNavigationBarItem(
            icon: new Icon(Icons.home),
            title: new Text('Home',
                style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold)),
          ),
          BottomNavigationBarItem(
            icon: new Icon(Icons.person),
            title: new Text('My Doctors',
                style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold)),
          ),
          BottomNavigationBarItem(
            icon: new Icon(Icons.schedule),
            title: new Text('Appointments',
                style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold)),
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.calendar_today),
              title: Text('History',
                  style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold))),
          BottomNavigationBarItem(
              icon: Icon(Icons.settings),
              title: Text('Signup/Login',
                  style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold))),
        ],
      ),
    );
  }

  Future navigateToMyAccount(context) {
    Navigator.of(context).pushNamed(MY_ACCOUNT);
  }

  Future navigateToHome(context) {
    Navigator.of(context).pushNamed(HOME);
  }
}
