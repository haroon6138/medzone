import 'package:MedZone/custom_widget/circle_image.dart';
import 'package:MedZone/custom_widget/medicine_package_card.dart';
import 'package:MedZone/models/TopFindingsModel.dart';
import 'package:MedZone/models/response/MedicinePharmacyResult.dart';
import 'package:MedZone/models/response/ReviewsResponse.dart';
import 'package:MedZone/pages/detail_page.dart';
import 'package:MedZone/services/CommonServices.dart';
import 'package:MedZone/services/Covid-Services.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_phone_direct_caller/flutter_phone_direct_caller.dart';
import 'package:flutter_svg/svg.dart';
import 'package:rating_bar/rating_bar.dart';
import 'package:easy_localization/easy_localization.dart';

class MedicinePharmacies extends StatelessWidget {
  MedicinePharmacies(
      {@required this.id,
      @required this.type,
      @required this.address,
      @required this.image,
      @required this.name,
      @required this.distance,
      @required this.days,
      @required this.timing,
      @required this.deliveryTiming,
      @required this.rating,
      @required this.webURL,
      @required this.latitude,
      @required this.longitude,
      @required this.contactNo,
      @required this.currentLatitude,
      @required this.currentLongitude,
      @required this.packages});

  final id;
  final type;
  final image;
  final name;
  final address;
  final distance;
  final days;
  final timing;
  final deliveryTiming;
  final rating;
  final webURL;
  final contactNo;
  final List<Packages> packages;
  var latitude;
  var longitude;

  var currentLatitude;
  var currentLongitude;

  final imageSize = 75.0;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        getProviderDetails(context);
      },
      child: Card(
        margin: EdgeInsets.fromLTRB(8.0, 16.0, 8.0, 0.0),
        shape: RoundedRectangleBorder(
          side: BorderSide(color: Colors.white70, width: 1),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Container(
          margin: EdgeInsets.only(bottom: 20.0, top: 5.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
                        child: Card(
                          margin: EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 0.0),
                          shape: RoundedRectangleBorder(
                            side: BorderSide(color: Colors.white70, width: 1),
                            borderRadius: BorderRadius.circular(50),
                          ),
                          child: CircleImage(
                              image: image,
                              width: imageSize,
                              height: imageSize),
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.all(0.0),
                            child: Text(
                              name,
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.black87,
                                  letterSpacing: 0.1,
                                  fontFamily: 'Open-Sans',
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width * 0.45,
                            padding: EdgeInsets.only(top: 5.0),
                            child: Text(
                              address,
                              overflow: TextOverflow.ellipsis,
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                  fontSize: 11,
                                  color: Colors.grey[500],
                                  letterSpacing: 0.1,
                                  fontFamily: 'Open-Sans'),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(top: 5.0),
                            child: RatingBar.readOnly(
                              size: 25.0,
                              maxRating: 1,
                              filledColor: Colors.amber,
                              emptyColor: Colors.amber,
                              halfFilledColor: Colors.amber,
                              initialRating: rating,
                              isHalfAllowed: true,
                              halfFilledIcon: Icons.star_half,
                              filledIcon: Icons.star,
                              emptyIcon: Icons.star_border,
                            ),
                          ),
                          Container(
                              padding: EdgeInsets.only(top: 8.0, left: 5.0),
                              child: Text(rating.toString(),
                                  style:
                                      TextStyle(fontWeight: FontWeight.bold))),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Column(
                                children: <Widget>[
                                  Container(
                                      padding: EdgeInsets.fromLTRB(
                                          10.0, 10.0, 0.0, 0.0),
                                      child: SvgPicture.asset(
                                          'assets/images/map_pin_icon_green.svg')),
                                ],
                              ),
                              Column(
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.fromLTRB(
                                        5.0, 15.0, 0.0, 0.0),
                                    child: Text("milesAway",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                                fontSize: 12,
                                                color: new Color(0xff17AF6B)
                                                    .withOpacity(1.0),
                                                letterSpacing: 0.1,
                                                fontFamily: 'Open-Sans-Black',
                                                fontWeight: FontWeight.bold))
                                        .tr(args: [distance]),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                  GestureDetector(
                    onTap: () {
                      _callNumber(contactNo);
                    },
                    child: Column(
                      children: <Widget>[
                        Container(
                            padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
                            child: SvgPicture.asset(
                                'assets/images/call_green_btn.svg')),
                        Container(
                          padding: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0),
                          child: Text(
                            "callNow",
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                fontSize: 11,
                                color: new Color(0xff17AF6B).withOpacity(1.0),
                                letterSpacing: 0.1,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Open-Sans-Bold'),
                          ).tr(),
                        )
                      ],
                    ),
                  ),
                ],
              ),
              Container(
                  height: 60.0,
                  margin: EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0),
                  child: ListView.builder(
                    itemCount: packages.length,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) {
                      return MedicinePackageCard(
                        packages: packages[index],
                      );
                    },
                  )),
            ],
          ),
        ),
      ),
    );
  }

  _callNumber(String number) async {
    await FlutterPhoneDirectCaller.callNumber(number);
  }

  getProviderDetails(context) {
    APIService service = new APIService();
    EasyLoading.show(status: CommonService.getLoadingMessage(), maskType: EasyLoadingMaskType.black);
    service
        .getProviderDetails(
            id, type, currentLatitude, currentLongitude, context)
        .then((res) {
      print(res);
      EasyLoading.dismiss();
      navigateToSubPage(res['result'], context);
    }).catchError((err) {
      print(err);
    });
  }

  Future navigateToSubPage(detailsResponse, context) async {
    List<String> insurances = List();
    List<String> services = List();
    List<ReviewsResponse> reviewsList = List();

    detailsResponse['insurances'].forEach((element) {
      insurances.add(element['name']);
    });

    detailsResponse['services'].forEach((element) {
      services.add(element['name']);
    });

    detailsResponse['reviews'].forEach((element) {
      reviewsList.add(new ReviewsResponse.fromJson(element));
    });

    print(reviewsList.length);

    var topFindingsObj = new topFindings(
        id: id,
        type: type,
        address: detailsResponse['address'],
        image: image,
        featuredImage: detailsResponse['featured_image'] == null
            ? "assets/images/medzone_cover.png"
            : detailsResponse['featured_image'],
        name: name,
        distance: distance,
        days: days,
        timing: timing,
        deliveryTiming: deliveryTiming,
        contactNo: contactNo,
        webURL: detailsResponse['web_site_url'] == null ||
                detailsResponse['web_site_url'] == 'unknown'
            ? "https://medzone.com"
            : detailsResponse['web_site_url'],
        latitude: double.parse(latitude),
        longitude: double.parse(longitude),
        rating: rating,
        insurances: insurances,
        services: services,
        reviewsList: reviewsList);
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => DetailPage(topFindingsObj)));
  }
}
