import 'package:MedZone/models/response/HomeDataResponse.dart';
import 'package:MedZone/models/response/MedicineDetailsResponse.dart';
import 'package:MedZone/models/response/MedicinePharmacyResult.dart';
import 'package:MedZone/models/response/TopMedicinesResponse.dart';
import 'package:MedZone/pages/medicineDetails.dart';
import 'package:MedZone/pages/medicineSearchResults.dart';
import 'package:MedZone/services/CommonServices.dart';
import 'package:MedZone/services/Covid-Services.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_svg/svg.dart';

class MedicineCard extends StatelessWidget {
  MedicineCard([this.data, this.image, this.clickFunction]);

  final clickFunction;
  final image;
  var currentLat;
  var currentLong;

  medData data;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (clickFunction == 'no') getMedicineDetails(context);
        if (clickFunction == 'yes') getMedicineAvailability(context);
      },
      child: Card(
        margin: EdgeInsets.fromLTRB(0.0, 16.0, 0.0, 0.0),
        shape: RoundedRectangleBorder(
          side: BorderSide(color: Colors.white70, width: 1),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Container(
          height: 60.0,
          child: Column(
            children: <Widget>[
              Container(
                margin: _getMargin(),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(data.name + ' - ' + data.formulastrength,
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 16.0,
                            letterSpacing: 0.8)),
                    Container(
                      margin: EdgeInsets.only(right: 15.0),
                      transform: _getTransformation(),
                      child: _getImage(),
                    ),
                  ],
                ),
              ),
              Row(
                children: <Widget>[
//                  Container(
//                    height: 20.0,
//                    margin: EdgeInsets.only(left: 12.0, top: 10.0),
//                    child: FlatButton(
//                      shape: RoundedRectangleBorder(
//                          borderRadius: BorderRadius.circular(8.0),
//                          side: BorderSide(
//                              color: new Color(0xff17AF6B).withOpacity(1.0))),
//                      color: new Color(0xff17AF6B).withOpacity(1.0),
//                      onPressed: () {},
//                      child: Text(data.supplier.toString().toUpperCase(),
//                          style: TextStyle(
//                              color: Colors.white,
//                              fontSize: 11.0,
//                              letterSpacing: 0.1)),
//                    ),
//                  ),
//                  Container(
//                    height: 20.0,
//                    margin: EdgeInsets.only(left: 8.0, top: 10.0),
//                    child: FlatButton(
//                      shape: RoundedRectangleBorder(
//                          borderRadius: BorderRadius.circular(8.0),
//                          side: BorderSide(
//                              color: new Color(0xff3278cc).withOpacity(1.0))),
//                      color: new Color(0xff3278cc).withOpacity(1.0),
//                      onPressed: () {},
//                      child: Text(data.shape.toString().toUpperCase(),
//                          style: TextStyle(
//                              color: Colors.white,
//                              fontSize: 11.0,
//                              letterSpacing: 0.1)),
//                    ),
//                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future navigateToSubPage(
      context, MedicinePharmacyResult medicinePharmacyResult) async {
    List<Pharmacy> pharmaciesList = List();

    medicinePharmacyResult.data.forEach((element) {
      pharmaciesList.add(new Pharmacy(
          id: element.id,
          name: element.name,
          address1: element.address1,
          logo: element.logo == null ? "" : element.logo,
          rating: element.rating,
          hours: element.hours,
          days: element.days,
          phoneNumber: element.phoneNumber,
          latlong: element.latlong,
          distance: element.distance,
          packages: element.packages));
    });

    await Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => MedicineSearchResults(
              data: data,
              pharmaciesList: pharmaciesList,
              currentLat: currentLat,
              currentLong: currentLong)),
    );
  }

  getMedicineAvailability(context) {
    APIService service = new APIService();
    MedicinePharmacyResult medicinePharmacyResult =
        new MedicinePharmacyResult();
    CommonService.getPrefsDouble(CommonService.currentLat, false).then((value) {
      currentLat = value;
      CommonService.getPrefsDouble(CommonService.currentLong, false)
          .then((value) {
        currentLong = value;
        EasyLoading.show(status: CommonService.getLoadingMessage(), maskType: EasyLoadingMaskType.black);
        service
            .getMedicineAvailability(
                data.id, currentLat.toString(), currentLong.toString(), context)
            .then((res) {
          print(res);
          medicinePharmacyResult = MedicinePharmacyResult.fromJson(res);
          EasyLoading.dismiss();
          if (medicinePharmacyResult.data.length != 0)
            navigateToSubPage(context, medicinePharmacyResult);
          else
            CommonService.showToast(
                context, "Medicine not available in our pharmacies list");
        }).catchError((err) {
          print(err);
          EasyLoading.dismiss();
        });
      });
    });
  }

  _getImage() {
    if (image == 'assets/images/link_Arrow.svg') {
      return SvgPicture.asset(image,
          width: 20.0, height: 20.0, fit: BoxFit.contain);
    }
    if (image == 'assets/images/info_icon.svg') {
      return SvgPicture.asset(image,
          width: 30.0, height: 30.0, fit: BoxFit.contain);
    }
  }

  _getMargin() {
    if (image == 'assets/images/link_Arrow.svg') {
      return EdgeInsets.only(top: 15.0, left: 15.0);
    }
    if (image == 'assets/images/info_icon.svg') {
      return EdgeInsets.only(top: 10.0, left: 15.0);
    }
  }

  _getTransformation() {
    if (image == 'assets/images/link_Arrow.svg') {
      return Matrix4.translationValues(0.0, 5.0, 0.0);
    }
    if (image == 'assets/images/info_icon.svg') {
      return Matrix4.translationValues(0.0, 5.0, 0.0);
    }
  }

  getMedicineDetails(context) async {
//    APIService service = new APIService();
//    MedicineDetailsResponse medicineDetailsResponse =
//        new MedicineDetailsResponse();
//    service.getMedicineDetails(data.id.toString(), context).then((res) {
//      medicineDetailsResponse = MedicineDetailsResponse.fromJson(res);
//      navigateToMedDetailPage(context, medicineDetailsResponse);
//    }).catchError((err) {
//      print(err);
//    }).showProgressBar(text: CommonService.getLoadingMessage(), backgroundColor: new Color(0xff17AF6B).withOpacity(0.8));
  }

  Future navigateToMedDetailPage(
      context, MedicineDetailsResponse medicineDetailsResponse) async {
    await Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => MedicineDetails(
              medicineDetailsResponse: medicineDetailsResponse)),
    );
  }
}
