import 'package:MedZone/custom_widget/circle_image.dart';
import 'package:MedZone/models/response/ReviewsResponse.dart';
import 'package:flutter/material.dart';
import 'package:rating_bar/rating_bar.dart';

class ReviewWidget extends StatelessWidget {
  List<ReviewsResponse> reviewsList;

  ReviewWidget({@required this.reviewsList});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
        physics: const NeverScrollableScrollPhysics(),
        padding: EdgeInsets.all(0.0),
        shrinkWrap: true,
        itemCount: reviewsList.length,
        itemBuilder: (context, index) {
          return Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
                        child: Card(
                          margin: EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 0.0),
                          shape: RoundedRectangleBorder(
                            side: BorderSide(color: Colors.white70, width: 1),
                            borderRadius: BorderRadius.circular(50),
                          ),
                          child: CircleImage(
                              image: "assets/images/medzone_logo.png",
                              width: 75.0,
                              height: 75.0),
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(top: 5.0),
                            child: RatingBar.readOnly(
                              size: 25.0,
                              filledColor: Colors.amber,
                              emptyColor: Colors.amber,
                              halfFilledColor: Colors.amber,
                              initialRating:
                                  reviewsList[index].rating.toDouble(),
                              isHalfAllowed: true,
                              halfFilledIcon: Icons.star_half,
                              filledIcon: Icons.star,
                              emptyIcon: Icons.star_border,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.all(0.0),
                            margin: EdgeInsets.fromLTRB(5.0, 0.0, 0.0, 0.0),
                            child: Text(
                              reviewsList[index].name,
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.black87,
                                  letterSpacing: 0.1,
                                  fontFamily: 'Open-Sans',
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.fromLTRB(5.0, 0.0, 0.0, 0.0),
                            padding: EdgeInsets.all(0.0),
                            child: Text(
                              "1 day ago",
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                  fontSize: 13,
                                  color: Colors.grey[500],
                                  letterSpacing: 0.1,
                                  fontFamily: 'Open-Sans'),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        padding: const EdgeInsets.all(10.0),
                        width: MediaQuery.of(context).size.width * 0.90,
                        child: Text(reviewsList[index].review),
                      ),
                    ],
                  ),
                ],
              ),
              Divider(
                color: Colors.grey[500],
              ),
            ],
          );
        },
      ),
    );
  }
}
