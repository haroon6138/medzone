import 'package:MedZone/custom_widget/circle_image.dart';
import 'package:MedZone/models/TopFindingsModel.dart';
import 'package:MedZone/models/currentLocation.dart';
import 'package:MedZone/models/response/ReviewsResponse.dart';
import 'package:MedZone/pages/detail_page.dart';
import 'package:MedZone/services/CommonServices.dart';
import 'package:MedZone/services/Covid-Services.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_phone_direct_caller/flutter_phone_direct_caller.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:rating_bar/rating_bar.dart';
import 'package:easy_localization/easy_localization.dart';

class TopFindingsCard extends StatelessWidget {
  TopFindingsCard(
      {@required this.id,
      @required this.type,
      @required this.address,
      @required this.image,
      @required this.name,
      @required this.distance,
      @required this.days,
      @required this.timing,
      @required this.deliveryTiming,
      @required this.rating,
      @required this.webURL,
      @required this.latitude,
      @required this.longitude,
      @required this.contactNo,
      @required this.currentLatitude,
      @required this.currentLongitude,
      this.waitingTime});

  final id;
  final type;
  final image;
  final name;
  final address;
  final distance;
  final days;
  final timing;
  final deliveryTiming;
  final rating;
  final webURL;
  final contactNo;
  var latitude;
  var longitude;
  var waitingTime;

  var currentLatitude;
  var currentLongitude;

  final imageSize = 75.0;

  @override
  Widget build(BuildContext context) {
    var locationProvider = Provider.of<CurrentLocation>(context, listen: false);
    return GestureDetector(
      onTap: () {
        getProviderDetails(context);
      },
      child: Card(
        margin: EdgeInsets.fromLTRB(8.0, 16.0, 8.0, 0.0),
        shape: RoundedRectangleBorder(
          side: BorderSide(color: Colors.white70, width: 1),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Container(
          margin: EdgeInsets.only(bottom: 20.0, top: 5.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
                        child: Card(
                          margin: EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 0.0),
                          shape: RoundedRectangleBorder(
                            side: BorderSide(color: Colors.white70, width: 1),
                            borderRadius: BorderRadius.circular(50),
                          ),
                          child: CircleImage(
                              image: image,
                              width: imageSize,
                              height: imageSize),
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width * 0.6,
                            padding: EdgeInsets.all(0.0),
                            child: Text(
                              name,
                              overflow: TextOverflow.clip,
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.black87,
                                  letterSpacing: 0.1,
                                  fontFamily: 'Open-Sans',
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width * 0.59,
                            padding: EdgeInsets.only(top: 5.0),
                            child: Text(
                              address,
                              overflow: TextOverflow.clip,
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                  fontSize: 11,
                                  color: Colors.grey[500],
                                  letterSpacing: 0.1,
                                  fontFamily: 'Open-Sans'),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(top: 5.0),
                            child: RatingBar.readOnly(
                              size: 25.0,
                              filledColor: Colors.amber,
                              emptyColor: Colors.amber,
                              halfFilledColor: Colors.amber,
                              initialRating: rating,
                              isHalfAllowed: true,
                              halfFilledIcon: Icons.star_half,
                              filledIcon: Icons.star,
                              emptyIcon: Icons.star_border,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Column(
                            children: <Widget>[
                              Container(
                                  padding:
                                      EdgeInsets.fromLTRB(20.0, 10.0, 0.0, 0.0),
                                  child: SvgPicture.asset(
                                      'assets/images/map_pin_icon_green.svg')),
                            ],
                          ),
                          Column(
                            children: <Widget>[
                              Container(
                                padding:
                                    EdgeInsets.fromLTRB(10.0, 10.0, 0.0, 0.0),
                                child: Text("milesAway",
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                            fontSize: 12,
                                            color: new Color(0xff17AF6B)
                                                .withOpacity(1.0),
                                            letterSpacing: 0.1,
                                            fontFamily: 'Open-Sans-Black',
                                            fontWeight: FontWeight.bold))
                                    .tr(args: [distance]),
                              ),
                            ],
                          ),
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Column(
                            children: <Widget>[
                              Container(
                                  padding:
                                      EdgeInsets.fromLTRB(20.0, 10.0, 0.0, 0.0),
                                  child: SvgPicture.asset(
                                      'assets/images/calender_green_icon.svg')),
                            ],
                          ),
                          Column(
                            children: <Widget>[
                              Container(
                                padding:
                                    EdgeInsets.fromLTRB(10.0, 10.0, 0.0, 0.0),
                                child: Text(
                                  days,
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                      fontSize: 12,
                                      color: Colors.black87,
                                      letterSpacing: 0.1,
                                      fontFamily: 'Open-Sans'),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Column(
                            children: <Widget>[
                              Container(
                                  padding:
                                      EdgeInsets.fromLTRB(19.0, 10.0, 0.0, 0.0),
                                  child: SvgPicture.asset(
                                      'assets/images/clock_green_icon.svg')),
                            ],
                          ),
                          Column(
                            children: <Widget>[
                              Container(
                                padding:
                                    EdgeInsets.fromLTRB(10.0, 10.0, 0.0, 0.0),
                                child: Text(
                                  timing,
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                      fontSize: 12,
                                      color: Colors.black87,
                                      letterSpacing: 0.1,
                                      fontFamily: 'Open-Sans'),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                      (type == "hospitals" || type == "urgent_cares") ? Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Column(
                            children: <Widget>[
                              Container(
                                  padding:
                                      EdgeInsets.fromLTRB(19.0, 10.0, 0.0, 0.0),
                                  child: Text("Waiting Time: ",
                                      style: TextStyle(
                                          fontSize: 12,
                                          color: new Color(0xff17AF6B)
                                              .withOpacity(1.0),
                                          fontWeight: FontWeight.bold,
                                          letterSpacing: 0.1,
                                          fontFamily: 'Open-Sans-bold'))),
                            ],
                          ),
                          Column(
                            children: <Widget>[
                              Container(
                                padding:
                                    EdgeInsets.fromLTRB(5.0, 10.0, 0.0, 0.0),
                                child: Text(
                                  waitingTime != null ? waitingTime + " mins" : "N/A",
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                      fontSize: 12,
                                      color: new Color(0xff17AF6B)
                                          .withOpacity(1.0),
                                      fontWeight: FontWeight.bold,
                                      letterSpacing: 0.1,
                                      fontFamily: 'Open-Sans-bold'),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ) : Container(),
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(right: 20.0),
                    child: GestureDetector(
                      onTap: () {
                        _callNumber(contactNo);
                      },
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          Container(
                              margin: EdgeInsets.only(right: 6.0, bottom: 3.0),
                              child: SvgPicture.asset(
                                  'assets/images/call_green_btn.svg')),
                          Container(
                            child: Text(
                              "callNow",
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                  fontSize: 11,
                                  color: new Color(0xff17AF6B).withOpacity(1.0),
                                  letterSpacing: 0.1,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'Open-Sans-Bold'),
                            ).tr(),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  _callNumber(String number) async {
    await FlutterPhoneDirectCaller.callNumber(number);
  }

  getProviderDetails(context) {
    APIService service = new APIService();
    EasyLoading.show(status: CommonService.getLoadingMessage(), maskType: EasyLoadingMaskType.black);
    service
        .getProviderDetails(
            id, type, currentLatitude, currentLongitude, context)
        .then((res) {
      print(res);
      EasyLoading.dismiss();
      navigateToSubPage(res['result'], context);
    }).catchError((err) {
      print(err);
      EasyLoading.dismiss();
    });
  }

  Future navigateToSubPage(detailsResponse, context) async {
    List<String> insurances = List();
    List<String> services = List();
    List<ReviewsResponse> reviewsList = List();

    detailsResponse['insurances'].forEach((element) {
      insurances.add(element['name']);
    });

    detailsResponse['services'].forEach((element) {
      services.add(element['name']);
    });

    detailsResponse['reviews'].forEach((element) {
      reviewsList.add(new ReviewsResponse.fromJson(element));
    });

    print(reviewsList.length);

    var topFindingsObj = new topFindings(
        id: id,
        type: type,
        address: detailsResponse['address'],
        image: image,
        featuredImage: detailsResponse['featured_image'] == null
            ? "assets/images/medzone_cover.png"
            : detailsResponse['featured_image'],
        name: name,
        distance: distance,
        days: days,
        timing: timing,
        deliveryTiming: deliveryTiming,
        contactNo: contactNo,
        webURL: detailsResponse['web_site_url'] == null ||
                detailsResponse['web_site_url'] == 'unknown'
            ? "https://medzone.com"
            : detailsResponse['web_site_url'],
        latitude: double.parse(latitude),
        longitude: double.parse(longitude),
        rating: rating,
        insurances: insurances,
        services: services,
        reviewsList: reviewsList);
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => DetailPage(topFindingsObj)));
  }
}
