import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';

import 'custom_radio.dart';

class SelectLanguage extends StatefulWidget {
  static var selectedLanguage = "";

  @override
  _SelectLanguageState createState() => _SelectLanguageState();
}

class _SelectLanguageState extends State<SelectLanguage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Dialog(
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
        child: Container(
          height: 340.0,
          width: 450.0,
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.all(20.0),
                  child: Text("changeLanguage",
                          style: TextStyle(
                              fontSize: 20.0,
                              fontWeight: FontWeight.bold,
                              fontFamily: "Open-Sans-Bold"))
                      .tr(),
                ),
                Container(
                  transform: Matrix4.translationValues(0.0, -30.0, 0.0),
                  child: CustomRadio(currentLocale: context.locale.toString()),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 40.0,
                  margin: EdgeInsets.only(left: 30.0, top: 0.0, right: 30.0),
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                        side: BorderSide(
                            color: new Color(0xff17AF6B).withOpacity(1.0))),
                    color: new Color(0xff17AF6B).withOpacity(1.0),
                    onPressed: () {
                      changeAppLanguage();
                    },
                    child: Text("SUBMIT",
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Open-Sans-Bold')),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.only(left: 100.0, top: 20.0, right: 100.0),
                  child: OutlineButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0)),
                    color: Colors.red,
                    borderSide: BorderSide(color: Colors.red),
                    highlightColor: Colors.red,
                    highlightedBorderColor: Colors.red,
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text("CANCEL",
                        style: TextStyle(
                            color: Colors.red,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Open-Sans-Bold')),
                  ),
                ),
              ]),
        ),
      ),
    );
  }

  changeAppLanguage() {
    if (SelectLanguage.selectedLanguage == "english") {
      context.locale = Locale('en');
    } else {
      context.locale = Locale('es');
    }
    Navigator.of(context).pop();
  }
}
