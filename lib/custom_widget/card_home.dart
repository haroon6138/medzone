import 'package:MedZone/models/TopFindingsModel.dart';
import 'package:MedZone/models/currentLocation.dart';
import 'package:MedZone/models/response/AllNearestProvidersResponse.dart';
import 'package:MedZone/pages/Nearest_Maps.dart';
import 'package:MedZone/services/CommonServices.dart';
import 'package:MedZone/services/Covid-Services.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

// ignore: must_be_immutable
class CardHome extends StatelessWidget {
  CardHome(
      {@required this.count,
      @required this.image,
      @required this.name,
      @required this.currentLat,
      @required this.currentLong,
      @required this.type});

  final count;
  final image;
  final name;
  final type;
  final currentLat;
  final currentLong;

  var imageSize = 46.0;

  @override
  Widget build(BuildContext context) {
    final locationProvider = Provider.of<CurrentLocation>(context);
    return GestureDetector(
      onTap: () {
        getAllNearestData(locationProvider, context);
      },
      child: Card(
        margin: EdgeInsets.fromLTRB(8.0, 16.0, 8.0, 0.0),
        shape: RoundedRectangleBorder(
          side: BorderSide(color: Colors.white70, width: 1),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Container(
          height: 140.0,
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.fromLTRB(11.0, 5.0, 0.0, 0.0),
                    child: Column(
                      children: <Widget>[
                        Text(this.count,
                            style: TextStyle(
                                fontSize: 45,
                                fontFamily: 'Open-Sans-Black',
                                fontWeight: FontWeight.w900,
                                letterSpacing: 0.5,
                                color: new Color(0xff17AF6B).withOpacity(1.0))),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(10.0, 3.0, 13.0, 0.0),
                    child: Column(
                      children: <Widget>[
                        SvgPicture.asset(this.image),
                      ],
                    ),
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width * 0.34,
                    margin: EdgeInsets.fromLTRB(11.0, 0.0, 10.0, 0.0),
                    child: Text(this.name,
                            overflow: TextOverflow.fade,
                            style: TextStyle(
                                fontSize: 14,
                                fontFamily: 'Open-Sans',
                                fontWeight: FontWeight.bold,
                                letterSpacing: 0.5,
                                color: Colors.black))
                        .tr(),
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.fromLTRB(11.0, 0.0, 10.0, 0.0),
                    child: Text("nearYou",
                            style: TextStyle(
                                fontSize: 12,
                                fontFamily: 'Open-Sans',
                                fontWeight: FontWeight.bold,
                                letterSpacing: 0.5,
                                color: Colors.grey[600]))
                        .tr(),
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.fromLTRB(11.0, 3.0, 10.0, 0.0),
                    child: Text("clickToSee",
                            style: TextStyle(
                                fontSize: 11,
                                fontFamily: 'Open-Sans',
                                fontWeight: FontWeight.bold,
                                letterSpacing: 0.5,
                                color: Colors.redAccent[200]))
                        .tr(),
                  ),
                ],
              ),
              Row(
                children: <Widget>[],
              ),
            ],
          ),
        ),
      ),
    );
  }

  getAllNearestData(locationProvider, context) {
    var providerType = tr(name);
    if (count == "0" || count == "00") {
      CommonService.showToast(context, "No " + providerType + " found near you");
    } else {
      APIService service = new APIService();
      EasyLoading.show(status: CommonService.getLoadingMessage(), maskType: EasyLoadingMaskType.black);
      AllNearestProvidersResponse allNearestProvidersResponse =
          new AllNearestProvidersResponse();
      service
          .getAllNearestProviders(
              type,
              locationProvider.getCurrentLat().toString(),
              locationProvider.getCurrentLong().toString(),
              context)
          .then((res) {
        allNearestProvidersResponse = AllNearestProvidersResponse.fromJson(res);
        print(res);
        EasyLoading.dismiss();
        navigateToSubPage(
            allNearestProvidersResponse, locationProvider, type, context);
      }).catchError((err) {
        print(err);
        EasyLoading.dismiss();
      });
    }
  }

  Future navigateToSubPage(
      AllNearestProvidersResponse allNearestProvidersResponse,
      locationProvider,
      String providerType,
      BuildContext context) async {
    var type = tr(name);

    List<topFindings> pharList = [];

    allNearestProvidersResponse.result.forEach((element) {
      var topFindingsObj = new topFindings(
          id: element.id,
          type: providerType,
          address: element.address1,
          image: element.logo == null
              ? "assets/images/medzone_logo.png"
              : element.logo,
          name: element.name,
          distance: element.distance,
          days: CommonService.getDays(element.days),
          timing: CommonService.getTimings(element.hours),
          deliveryTiming: "05:00 pm",
          contactNo: element.phoneNumber,
          webURL: "https://themedzone.com/",
          latitude: double.parse(element.latlong.split(",")[0]),
          longitude: double.parse(element.latlong.split(",")[1]),
          waitingTime: element.waitingTime,
          rating: element.rating.toDouble());
      pharList.add(topFindingsObj);
    });

    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => NearestMaps(locationProvider.getCurrentLat(),
                locationProvider.getCurrentLong(), type, pharList)));
  }
}
