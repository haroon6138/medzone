import 'package:MedZone/models/TopFindingsModel.dart';
import 'package:MedZone/services/CommonServices.dart';
import 'package:MedZone/services/Covid-Services.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:rating_bar/rating_bar.dart';

class WriteReview extends StatelessWidget {
  final topFindings topFindingsObj;

  WriteReview({@required this.topFindingsObj});

  @override
  Widget build(BuildContext context) {
    return writeReview(topFindingsObj: topFindingsObj);
  }
}

class writeReview extends StatefulWidget {
  final topFindings topFindingsObj;

  writeReview({@required this.topFindingsObj});

  @override
  _writeReviewState createState() => _writeReviewState();
}

class _writeReviewState extends State<writeReview> {
  final myNameController = TextEditingController();
  final myEmailController = TextEditingController();
  final myCommentsController = TextEditingController();

  double selectedRating = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.15),
        child: Dialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
          //this right here
          child: Container(
            height: 500.0,
            width: 450.0,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.all(20.0),
                  child: Text("Write A Review",
                      style: TextStyle(
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold,
                          fontFamily: "Open-Sans-Bold")),
                ),
                Row(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0),
                      child: Text("Rate:",
                          style: TextStyle(
                              fontSize: 18.0,
                              fontWeight: FontWeight.bold,
                              fontFamily: "Open-Sans-Bold")),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(20.0, 0.0, 0.0, 0.0),
                      child: RatingBar(
                        onRatingChanged: _onRatingChanged,
                        size: 40.0,
                        filledColor: Colors.amber,
                        emptyColor: Colors.grey,
                        halfFilledColor: Colors.amber,
                        initialRating: 0.0,
                        isHalfAllowed: false,
                        halfFilledIcon: Icons.star_half,
                        filledIcon: Icons.star,
                        emptyIcon: Icons.star,
                      ),
                    ),
                  ],
                ),

                //Name TextField
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.fromLTRB(15.0, 20.0, 20.0, 0.0),
                      width: MediaQuery.of(context).size.width * 0.68,
                      height: 50.0,
                      child: Row(
                        children: <Widget>[
                          new Flexible(
                            child: new TextField(
                              textInputAction: TextInputAction.next,
                              onSubmitted: (_) =>
                                  FocusScope.of(context).nextFocus(),
                              controller: myNameController,
                              decoration: const InputDecoration(
                                border: OutlineInputBorder(),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.green, width: 2.0),
                                ),
                                labelText: "Enter Your Name",
                                labelStyle: TextStyle(color: Colors.grey),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),

                //Email TextField
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.fromLTRB(15.0, 20.0, 20.0, 0.0),
                      width: MediaQuery.of(context).size.width * 0.68,
                      height: 50.0,
                      child: Row(
                        children: <Widget>[
                          new Flexible(
                            child: new TextField(
                              keyboardType: TextInputType.emailAddress,
                              textInputAction: TextInputAction.next,
                              onSubmitted: (_) =>
                                  FocusScope.of(context).nextFocus(),
                              controller: myEmailController,
                              decoration: const InputDecoration(
                                border: OutlineInputBorder(),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.green, width: 2.0),
                                ),
                                labelText: "Enter Your Email",
                                labelStyle: TextStyle(color: Colors.grey),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),

                //Comments TextField
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.fromLTRB(15.0, 20.0, 20.0, 0.0),
                      width: MediaQuery.of(context).size.width * 0.68,
                      height: 120.0,
                      child: Row(
                        children: <Widget>[
                          new Flexible(
                            child: new TextField(
                              maxLines: 8,
                              textInputAction: TextInputAction.done,
                              onSubmitted: (_) =>
                                  FocusScope.of(context).unfocus(),
                              controller: myCommentsController,
                              decoration: const InputDecoration(
                                border: OutlineInputBorder(),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.green, width: 2.0),
                                ),
                                labelText: "Write Comments",
                                labelStyle: TextStyle(color: Colors.grey),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),

                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(left: 17.0, top: 20.0),
                          child: RaisedButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                side: BorderSide(
                                    color: new Color(0xff17AF6B)
                                        .withOpacity(1.0))),
                            color: new Color(0xff17AF6B).withOpacity(1.0),
                            onPressed: () {
                              sendRatingToBackend();
                            },
                            child: Text("SUBMIT",
                                style: TextStyle(color: Colors.white)),
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(right: 22.0, top: 20.0),
                          child: OutlineButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0)),
                            color: Colors.red,
                            borderSide: BorderSide(color: Colors.red),
                            highlightColor: Colors.red,
                            highlightedBorderColor: Colors.red,
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text("CANCEL",
                                style: TextStyle(color: Colors.red)),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  sendRatingToBackend() {
    APIService service = new APIService();
    EasyLoading.show(
        status: CommonService.getLoadingMessage(),
        maskType: EasyLoadingMaskType.black);
    service
        .addReview(
            widget.topFindingsObj.id,
            widget.topFindingsObj.type,
            myNameController.text,
            myEmailController.text,
            selectedRating,
            myCommentsController.text,
            context)
        .then((res) {
      setState(() {
        EasyLoading.dismiss();
        Navigator.of(context).pop();
        CommonService.showToast(
            context, "Your review has been submitted successfully");
      });
    }).catchError((err) {
      EasyLoading.dismiss();
      print(err);
    });
  }

  _onRatingChanged(value) {
    print(value);
    setState(() {
      selectedRating = value;
    });
  }
}
