import 'dart:convert';

import 'package:MedZone/config/config.dart';
import 'package:MedZone/pages/No_Internet.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

class BaseAppClient {
  static post(
    String endPoint,
    context, {
    bool isJWT,
    String authToken = "",
    Map<String, dynamic> body,
    Function(dynamic response, int statusCode) onSuccess,
    Function(String error, int statusCode) onFailure,
  }) async {
    var headers;

    if (isJWT && authToken != "") {
      headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + authToken
      };
    } else {
      headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'wDjoO7v72K3EvuIz/mXKOo6CDKT4a0skfWFm5TsjS3g='
      };
    }

    String url = BASE_URL + endPoint;
    try {
      print("URL : $url");
      print("Body : ${json.encode(body)}");
      print("Headers : $headers");

      if (Provider.of<DataConnectionStatus>(context, listen: false) ==
          DataConnectionStatus.disconnected) {
        return NoInternet();
      } else {
        final response =
            await http.post(Uri.parse(url), body: json.encode(body), headers: headers);
        final int statusCode = response.statusCode;
        if (statusCode < 200 || statusCode >= 400 || json == null) {
          print("StatusCode: $statusCode");
          print("ERROR: $response");
          onFailure('Error While Fetching data', statusCode);
        } else {
          var parsed = json.decode(response.body.toString());
          print("Response : $parsed");
          onSuccess(parsed, statusCode);
        }
      }
    } catch (e) {
      print(e);
      onFailure(e.toString(), -1);
    }
  }

  static get(
    String endPoint,
    context, {
    bool isAuth,
    bool isJWT,
    String authToken = "",
    Function(dynamic response, int statusCode) onSuccess,
    Function(String error, int statusCode) onFailure,
  }) async {
    var headers;
    try {
      print("URL : $endPoint");

      if (isJWT && authToken != "") {
        headers = {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer ' + authToken
        };
      } else {
        headers = {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'wDjoO7v72K3EvuIz/mXKOo6CDKT4a0skfWFm5TsjS3g='
        };
      }

      print("HEADERS : $headers");

      if (Provider.of<DataConnectionStatus>(AppGlobal.context, listen: false) ==
          DataConnectionStatus.disconnected) {
        return NoInternet();
      } else {
        final response =
            await http.get(Uri.parse(endPoint), headers: headers);
        final int statusCode = response.statusCode;
        if (statusCode < 200 || statusCode >= 400 || json == null) {
          print("StatusCode: $statusCode");
          print("ERROR: $response");
          onFailure('Error While Fetching data', statusCode);
        } else {
          var parsed = json.decode(response.body.toString());
          onSuccess(parsed, statusCode);
        }
      }
    } catch (e) {
      print(e);
      onFailure(e.toString(), -1);
    }
  }
}
