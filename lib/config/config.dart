const MAX_SMALL_SCREEN = 660;

const  BASE_URL = 'https://themedzone.com/api/v2/'; // Production Environment

//URL to get covid updates by county
const GET_COVID_UPDATES_BY_COUNTY =
    "http://corona.lmao.ninja/v2/jhucsse/counties/";

//URL to get covid updates by state
const GET_COVID_UPDATES_BY_STATE = "http://corona.lmao.ninja/v2/states/";

const HOME_DATA = "homeCount";
const TOP_MEDS = "topMedicines";
const SEARCH_MEDS = "searchMedicine";
const MED_DETAILS = "detailsMed";
const PROVIDER_DETAILS = "details";
const ADD_REVIEW = "review";
const MEDICINE_PHARMACY = "medicinePharmacy";
const ALL_NEAREST = "allNearest";

// Appointment APIs
const GET_SPECIALITIES = "https://themedzone.com/api/v2/specialities";
const GET_DOCTORS = "search-special-doctor";
const GET_DOCTOR_SLOTS = "slots";
const REGISTER_USER = "register";
const LOGIN_USER = "login";
const LOGOUT_USER = "logout";
const MAKE_APPOINTMENT = "make-appointment";
const CANCEL_APPOINTMENT = "delete-appointment";
const APPOINTMENTS_HISTORY = "https://themedzone.com/api/v2/appointments";

const SEND_OTP_CODE = "send-code";
const VERIFY_OTP_CODE = "verify";
const RESET_PASSWORD = "reset";

const ADD_FAVOURITE_DOCTOR = "favourite-doctor-add";
const REMOVE_FAVOURITE_DOCTOR = "favourite-doctor-remove";
const GET_DOCTOR_REVIEWS = "review";
const GET_FAVOURITE_DOCTOR = "https://themedzone.com/api/v2/favourite-doctor";
const REFRESH_TOKEN = "https://themedzone.com/api/v2/refresh";

const GET_INSURANCES = "https://themedzone.com/api/v2/insurance-companies";

// Constants
const FIREBASE_TOKEN = "firebase_token";
const APNS_TOKEN = "apns_token";
const AUTH_USER = "auth_user";
const AUTHENTICATED_USER = "authenticated_user";
const JWT_TOKEN = "jwt_token";
const USER_EMAIL = "user_email";
const USER_PASSWORD = "user_password";
const SELECTED_INSURANCE_COMPANY_ID = "selected_insurance_company_id";


class AppGlobal {
  static var context;
}
