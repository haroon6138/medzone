import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ConfirmDialog {
  final BuildContext context;

  final title;
  final confirmMessage;
  final okText;
  final cancelText;
  final Function okFunction;
  final Function cancelFunction;

  ConfirmDialog(
      {@required this.context,
      this.title,
      @required this.confirmMessage,
      @required this.okText,
      @required this.cancelText,
      @required this.okFunction,
      @required this.cancelFunction});

  showAlertDialog(BuildContext context) {
    // set up the buttons
    Widget cancelButton = FlatButton(
        child: Text(this.cancelText,
            style: TextStyle(
              color: Colors.red,
            )),
        onPressed: () {
          Navigator.of(context).pop();
        });
    Widget continueButton = FlatButton(
        child: Text(okText,
            style: TextStyle(
              color: Colors.grey[700],
            )),
        onPressed: okFunction);

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: title != null ? Text(title) : Text("Confirm"),
      content: Text(this.confirmMessage),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  static closeAlertDialog(BuildContext context) {
    Navigator.of(context).pop();
  }
}
