import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';

class PlatformBridge {
  static const platform = const MethodChannel("Platform-Bridge");
  static PlatformBridge _shared;
  static BuildContext _context;

  // // Singleton
  // static final PlatformBridge _singleton = PlatformBridge._internal();
  // factory PlatformBridge() {
  //   return _singleton;
  // }
  // PlatformBridge._internal();

  static PlatformBridge shared() {
    if (_shared == null) {
      assert(true, "PlatformBridge is not initialized, (Initialized it by calling 'PlatformBridge.init(BuildContext))'.");
    }
    return _shared;
  }

  static void init(BuildContext context) {
    _context = context;
    _shared = PlatformBridge();

    Future<dynamic> incoming(MethodCall methodCall) async {
      switch (methodCall.method) {
        default:
          throw MissingPluginException('notImplemented');
      }
    }

    platform.setMethodCallHandler(incoming);
  }

  static const IS_DRAW_OVER_APPS_PERMISSION_ALLOWED = "isDrawOverAppsPermissionAllowed";
  static const ASK_DRAW_OVER_APPS_PERMISSION = "askDrawOverAppsPermission";
  static const GET_INTENT = "getIntent";

  Future<bool> isDrawOverAppsPermissionAllowed() async {
    var result = await platform.invokeMethod(IS_DRAW_OVER_APPS_PERMISSION_ALLOWED);
    return result as bool;
  }

  Future<bool> askDrawOverAppsPermission() async {
    var result = await platform.invokeMethod(ASK_DRAW_OVER_APPS_PERMISSION);
    return result as bool;
  }

  Future<bool> getIntentData() async {
    var result = await platform.invokeMethod(GET_INTENT);
    return result as bool;
  }
}
