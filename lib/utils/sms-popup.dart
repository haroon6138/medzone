import 'dart:async';
import 'package:MedZone/utils/otp_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:sms_otp_auto_verify/sms_otp_auto_verify.dart';

class SMSOTP {
  final Function onSuccess;
  final Function onFailure;
  final context;

  int remainingTime = 300;

  Future<Null> timer;

  SMSOTP(
    this.context,
    this.onSuccess,
    this.onFailure,
  );

  final verifyAccountForm = GlobalKey<FormState>();

  final TextEditingController _pinPutController = TextEditingController();

  TextEditingController digit1 = TextEditingController(text: "");
  TextEditingController digit2 = TextEditingController(text: "");
  TextEditingController digit3 = TextEditingController(text: "");
  TextEditingController digit4 = TextEditingController(text: "");

  Map verifyAccountFormValue = {
    'digit1': '',
    'digit2': '',
    'digit3': '',
    'digit4': '',
  };
  final focusD1 = FocusNode();
  final focusD2 = FocusNode();
  final focusD3 = FocusNode();
  final focusD4 = FocusNode();
  String errorMsg;

  // ProjectViewModel projectProvider;
  String displayTime = '';
  String _code;
  dynamic setState;
  static String signature;

  displayDialog(BuildContext context) async {
    return showDialog(
        context: context,
        barrierColor: Colors.black.withOpacity(0.63),
        builder: (context) {
          // projectProvider = Provider.of(context);
          return Dialog(
            backgroundColor: Colors.white,
            shape: RoundedRectangleBorder(),
            insetPadding: EdgeInsets.only(left: 21, right: 21),
            child: StatefulBuilder(builder: (context, setState) {
              if (displayTime == '') {
                startTimer(setState);
              }

              return Container(
                padding: EdgeInsets.only(left: 21, right: 18, top: 39, bottom: 59),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SvgPicture.asset(
                          "assets/images/verify_sms.svg",
                          height: 50,
                          width: 50,
                        ),
                        IconButton(
                          padding: EdgeInsets.zero,
                          icon: Icon(Icons.close),
                          constraints: BoxConstraints(),
                          onPressed: () {
                            Navigator.pop(context);
                            this.onFailure();
                          },
                        )
                      ],
                    ),
                    SizedBox(height: 22),
                    Text(
                      "Please enter the verification code sent to your registered phone number",
                      style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600, color: Color(0xff2B353E), letterSpacing: -0.48),
                    ),
                    SizedBox(height: 18),
                    Directionality(
                      textDirection: TextDirection.ltr,
                      child: Center(
                        child: OTPWidget(
                          autoFocus: true,
                          controller: _pinPutController,
                          defaultBorderColor: Color(0xffD8D8D8),
                          maxLength: 4,
                          onTextChanged: (text) {},
                          pinBoxColor: Colors.white,
                          onDone: (code) => _onOtpCallBack(code, null),
                          textBorderColor: Color(0xffD8D8D8),
                          pinBoxWidth: 60,
                          pinBoxHeight: 60,
                          pinTextStyle: TextStyle(
                            fontSize: 24.0,
                            color: Color(0xff2B353E),
                          ),
                          pinTextAnimatedSwitcherTransition: ProvidedPinBoxTextAnimation.scalingTransition,
                          pinTextAnimatedSwitcherDuration: Duration(milliseconds: 300),
                          pinBoxRadius: 10,
                          keyboardType: TextInputType.number,
                        ),
                      ),
                    ),
                    SizedBox(height: 30),
                    RichText(
                      text: TextSpan(
                        text: "Invalid code",
                        style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600, color: Color(0xff2B353E), letterSpacing: -0.48),
                        children: <TextSpan>[
                          TextSpan(
                            text: displayTime,
                            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600, color: Color(0xffD02127), letterSpacing: -0.48),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              );
            }),
          );
        });
  }

  TextStyle buildTextStyle() {
    return TextStyle(
      fontSize: 14.0,
    );
  }

  InputDecoration buildInputDecoration(BuildContext context) {
    return InputDecoration(
      counterText: " ",
      // ts/images/password_icon.png
      // contentPadding: EdgeInsets.only(top: 20, bottom: 20),
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(10)),
        borderSide: BorderSide(color: Colors.black),
      ),
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
        borderSide: BorderSide(color: Theme.of(context).primaryColor),
      ),
      errorBorder: OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
        borderSide: BorderSide(color: Theme.of(context).errorColor),
      ),
      focusedErrorBorder: OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
        borderSide: BorderSide(color: Theme.of(context).errorColor),
      ),
    );
  }

  String validateCodeDigit(value) {
    if (value.isEmpty) {
      return ' ';
    } else if (value.length == 3) {
      print(value);
    } else {
      return null;
    }
  }

  checkValue() {
    //print(verifyAccountFormValue);
    if (verifyAccountForm.currentState.validate()) {
      onSuccess(digit1.text.toString() + digit2.text.toString() + digit3.text.toString() + digit4.text.toString());
    }
  }

  getSecondsAsDigitalClock(int inputSeconds) {
    var sec_num = int.parse(inputSeconds.toString()); // don't forget the second param
    var hours = (sec_num / 3600).floor();
    var minutes = ((sec_num - hours * 3600) / 60).floor();
    var seconds = sec_num - hours * 3600 - minutes * 60;
    var minutesString = "";
    var secondsString = "";
    minutesString = minutes < 10 ? "0" + minutes.toString() : minutes.toString();
    secondsString = seconds < 10 ? "0" + seconds.toString() : seconds.toString();
    return minutesString + ":" + secondsString;
  }

  startTimer(setState) {
    this.remainingTime--;
    setState(() {
      displayTime = this.getSecondsAsDigitalClock(this.remainingTime);
    });

    timer = Future.delayed(Duration(seconds: 1), () {
      if (this.remainingTime > 0) {
        startTimer(setState);
      } else {
        Navigator.pop(context);
      }
    });
  }

  static void hideSMSBox(context) {
    Navigator.pop(context);
  }

  _onOtpCallBack(String otpCode, bool isAutofill) {
    if (otpCode.length == 4) {
      onSuccess(otpCode);
      // _pinPutController.clear();
    }
  }

  static getSignature() async {
    return await SmsRetrieved.getAppSignature();
  }
}
