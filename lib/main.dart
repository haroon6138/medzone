import 'package:MedZone/config/config.dart';
import 'package:MedZone/models/AuthUserProvider.dart';
import 'package:MedZone/models/MyDoctorsProvider.dart';
import 'package:MedZone/models/response/AppointmentsResponse.dart';
import 'package:MedZone/models/response/MyDoctorsResponse.dart';
import 'package:MedZone/pages/MyDoctorsPage.dart';
import 'package:MedZone/pages/appointments/AppointmentSearchPage.dart';
import 'package:MedZone/pages/appointments/SearchByClinic.dart';
import 'package:MedZone/pages/homePage.dart';
import 'package:MedZone/pages/myAccount.dart';
import 'package:MedZone/routes.dart';
import 'package:MedZone/services/CommonServices.dart';
import 'package:MedZone/services/Covid-Services.dart';
import 'package:MedZone/services/connectivity_service.dart';
import 'package:MedZone/utils/PlatformBridge.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:provider/provider.dart';
import 'package:firebase_core/firebase_core.dart';
import 'models/AppointmentsProvider.dart';
import 'models/currentLocation.dart';

var currentLat = 0.0;
var currentLong = 0.0;
String _currentAddress = "";

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(
    EasyLocalization(
        useOnlyLangCode: true,
        supportedLocales: [Locale('en'), Locale('es')],
        path: 'assets/lang',
        fallbackLocale: Locale('en'),
        startLocale: Locale('en'),
        child: MyApp()),
  );
  configLoading();
}

void configLoading() {
  EasyLoading.instance
    // ..displayDuration = const Duration(milliseconds: 2000)
    ..indicatorType = EasyLoadingIndicatorType.fadingCircle
    ..loadingStyle = EasyLoadingStyle.dark
    ..indicatorSize = 45.0
    ..radius = 10.0
    ..progressColor = Colors.yellow
    ..backgroundColor = Colors.green
    ..indicatorColor = Colors.yellow
    ..textColor = Colors.yellow
    ..maskColor = Colors.blue.withOpacity(0.5)
    ..userInteractions = true
    ..dismissOnTap = false;
  // ..customAnimation = CustomAnimation();
}

final homePageState = new GlobalKey<_MainPageState>();

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    PlatformBridge.init(context);
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<CurrentLocation>(
            create: (BuildContext context) =>
                CurrentLocation(currentLat, currentLong, _currentAddress)),
        ChangeNotifierProvider<AuthUserProvider>(
            create: (BuildContext context) => AuthUserProvider()),
        ChangeNotifierProvider<AppointmentsProvider>(
            create: (BuildContext context) => AppointmentsProvider()),
        ChangeNotifierProvider<MyDoctorsProvider>(
            create: (BuildContext context) => MyDoctorsProvider()),
        StreamProvider<DataConnectionStatus>(create: (context) {
          return DataConnectivityService().connectivityStreamController.stream;
        }),
      ],
      child: MaterialApp(
        localizationsDelegates: context.localizationDelegates,
        supportedLocales: context.supportedLocales,
        locale: context.locale,
        title: 'MedZone',
        debugShowCheckedModeBanner: false,
        initialRoute: HOME,
        routes: routes,
        builder: EasyLoading.init(),
        theme: ThemeData(primarySwatch: Colors.blue, fontFamily: 'Open-Sans'),
      ),
    );
  }
}

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  static int selectedPage = 0;
  List<Widget> pageList = List<Widget>();

  PersistentTabController _controller;

  AuthUserProvider authUserProvider;
  AppointmentsProvider appointmentsProvider;
  MyDoctorsProvider myDoctorsProvider;

  @override
  void initState() {
    super.initState();
    AppGlobal.context = context;
    _controller = PersistentTabController(initialIndex: 0);

    _controller.jumpToTab(0);
  }

  @override
  Widget build(BuildContext context) {
    authUserProvider = Provider.of<AuthUserProvider>(context);
    appointmentsProvider = Provider.of<AppointmentsProvider>(context);
    myDoctorsProvider = Provider.of<MyDoctorsProvider>(context);
    return Scaffold(
      body: IndexedStack(
        index: selectedPage,
        // children: pageList,
        children: [
          HomePageWidget(),
          MyDoctors(removeFunction: removeFavouriteDoctor),
          // AppointmentSearchTypeSelect(),
          SearchByClinic(),
          AppointmentSearch(selectedPage, cancelFunction: cancelAppointment),
          MyAccount()
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        selectedItemColor: new Color(0xff17AF6B).withOpacity(1.0),
        items: [
          BottomNavigationBarItem(
            icon: new Icon(Icons.home),
            title: new Text('Home',
                style: TextStyle(fontSize: 11, fontWeight: FontWeight.bold)),
          ),
          BottomNavigationBarItem(
            icon: new Icon(Icons.person),
            title: new Text('My Doctors',
                style: TextStyle(fontSize: 11, fontWeight: FontWeight.bold)),
          ),
          BottomNavigationBarItem(
            icon: new Icon(Icons.schedule),
            title: new Text('Appointments',
                style: TextStyle(fontSize: 11, fontWeight: FontWeight.bold)),
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.calendar_today),
              title: Text('History',
                  style: TextStyle(fontSize: 11, fontWeight: FontWeight.bold))),
          BottomNavigationBarItem(
              icon: Icon(Icons.settings),
              title: Text('Signup/Login',
                  style: TextStyle(fontSize: 11, fontWeight: FontWeight.bold))),
        ],
        currentIndex: selectedPage,
        onTap: onItemTapped,
      ),
    );
  }

  void onItemTapped(int index) {
    setState(() {
      selectedPage = index;
      print(selectedPage);
      if (selectedPage == 3 && authUserProvider.getIsLogin()) {
        getAppointmentsHistory();
      }
      if (selectedPage == 1 && authUserProvider.getIsLogin()) {
        getMyDoctors();
      }
    });
  }

  getMyDoctors() {
    APIService service = new APIService();
    MyDoctorsResponse myDoctorsResponse = new MyDoctorsResponse();
    EasyLoading.show(
        status: CommonService.getLoadingMessage(),
        maskType: EasyLoadingMaskType.black);
    service.getFavouriteDoctors(context).then((res) {
      print(res);
      myDoctorsResponse = MyDoctorsResponse.fromJson(res);
      myDoctorsProvider.setAppointmentsResponse(myDoctorsResponse);
      EasyLoading.dismiss();
    }).catchError((err) {
      EasyLoading.dismiss();
      print(err);
    });
  }

  getAppointmentsHistory() {
    APIService service = new APIService();
    AppointmentsResponse appointmentsResponse = new AppointmentsResponse();
    EasyLoading.show(
        status: CommonService.getLoadingMessage(),
        maskType: EasyLoadingMaskType.black);
    service.getAppointmentsHistory(context).then((res) {
      appointmentsResponse = AppointmentsResponse.fromJson(res);
      appointmentsProvider.setAppointmentsResponse(appointmentsResponse);
      EasyLoading.dismiss();
    }).catchError((err) {
      EasyLoading.dismiss();
      print(err);
    });
  }

  cancelAppointment(int appointmentID) {
    print("cancelAppointment - " + appointmentID.toString());
    APIService service = new APIService();
    EasyLoading.show(
        status: CommonService.getLoadingMessage(),
        maskType: EasyLoadingMaskType.black);
    service.cancelAppointment(appointmentID, context).then((res) {
      EasyLoading.dismiss();
      CommonService.showToast(
          context, "Appointment has been cancelled successfully.");
      getAppointmentsHistory();
    }).catchError((err) {
      EasyLoading.dismiss();
      print(err);
    });
  }

  removeFavouriteDoctor(int doctorID) {
    print("removeFavouriteDoctor - " + doctorID.toString());
    APIService service = new APIService();
    EasyLoading.show(
        status: CommonService.getLoadingMessage(),
        maskType: EasyLoadingMaskType.black);
    service.removeFavouriteDoctor(doctorID, context).then((res) {
      EasyLoading.dismiss();
      getMyDoctors();
    }).catchError((err) {
      EasyLoading.dismiss();
      print(err);
    });
  }

  static void changeIndex(int index) {
    selectedPage = index;
  }
}
