import 'package:MedZone/models/response/AuthenticatedUser.dart';
import 'package:flutter/material.dart';

class AuthUserProvider with ChangeNotifier {
  bool isLogin = false;
  AuthenticatedUser authenticatedUser;

  bool getIsLogin() => isLogin;

  AuthenticatedUser getAuthUser() => authenticatedUser;

  setIsLogin(bool isLogin) {
    this.isLogin = isLogin;
    notifyListeners();
  }

  setAuthUser(AuthenticatedUser authenticatedUser) {
    this.authenticatedUser = authenticatedUser;
    notifyListeners();
  }
}
