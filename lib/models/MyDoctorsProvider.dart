import 'package:MedZone/models/response/MyDoctorsResponse.dart';
import 'package:flutter/cupertino.dart';

class MyDoctorsProvider with ChangeNotifier {
  MyDoctorsResponse myDoctorsResponse;

  MyDoctorsResponse getMyDoctors() => myDoctorsResponse;

  setAppointmentsResponse(MyDoctorsResponse myDoctorsResponse) {
    this.myDoctorsResponse = myDoctorsResponse;
    notifyListeners();
  }
}
