class InsuranceCompaniesResponse {
  String response;
  List<InsuranceData> data;

  InsuranceCompaniesResponse({this.response, this.data});

  InsuranceCompaniesResponse.fromJson(Map<String, dynamic> json) {
    response = json['response'];
    if (json['data'] != null) {
      data = new List<InsuranceData>();
      json['data'].forEach((v) {
        data.add(new InsuranceData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['response'] = this.response;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class InsuranceData {
  int id;
  String name;
  int parentId;
  String description;
  int status;
  String createdAt;
  String updatedAt;
  int createdBy;

  InsuranceData(
      {this.id,
        this.name,
        this.parentId,
        this.description,
        this.status,
        this.createdAt,
        this.updatedAt,
        this.createdBy});

  InsuranceData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    parentId = json['parent_id'];
    description = json['description'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    createdBy = json['created_by'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['parent_id'] = this.parentId;
    data['description'] = this.description;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['created_by'] = this.createdBy;
    return data;
  }
}
