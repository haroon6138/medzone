class TopMedicinesResponse {
  String response;
  List<medData> medDataList;

  TopMedicinesResponse({this.response, this.medDataList});

  TopMedicinesResponse.fromJson(Map<String, dynamic> json) {
    response = json['response'];
    if (json['data'] != null) {
      medDataList = new List<medData>();
      json['data'].forEach((v) {
        medDataList.add(new medData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['response'] = this.response;
    if (this.medDataList != null) {
      data['data'] = this.medDataList.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class medData {
  int id;
  String name;
  String formulastrength;
  String supplier;

  medData(
      {this.id,
      this.name,
      this.formulastrength,
      this.supplier});

  medData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    formulastrength = json['formulastrength'];
    supplier = json['supplier'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['formulastrength'] = this.formulastrength;
    data['supplier'] = this.supplier;
    return data;
  }
}
