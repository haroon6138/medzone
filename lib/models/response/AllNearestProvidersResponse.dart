class AllNearestProvidersResponse {
  String response;
  List<Result> result;

  AllNearestProvidersResponse({this.response, this.result});

  AllNearestProvidersResponse.fromJson(Map<String, dynamic> json) {
    response = json['response'];
    if (json['result'] != null) {
      result = new List<Result>();
      json['result'].forEach((v) {
        result.add(new Result.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['response'] = this.response;
    if (this.result != null) {
      data['result'] = this.result.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Result {
  int id;
  String name;
  String address1;
  String logo;
  String featuredImage;
  int rating;
  List<String> hours;
  List<String> days;
  String phoneNumber;
  String distance;
  String latlong;
  dynamic waitingTime;

  Result(
      {this.id,
      this.name,
      this.address1,
      this.logo,
      this.featuredImage,
      this.rating,
      this.hours,
      this.days,
      this.phoneNumber,
      this.distance,
      this.latlong,
      this.waitingTime});

  Result.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    address1 = json['address_1'];
    logo = json['logo'];
    featuredImage = json['featured_image'];
    rating = json['rating'];
    hours = json['hours'].cast<String>();
    days = json['days'].cast<String>();
    phoneNumber = json['phone_number'];
    distance = json['distance'];
    latlong = json['latlong'];
    waitingTime = json['waiting_time'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['address_1'] = this.address1;
    data['logo'] = this.logo;
    data['featured_image'] = this.featuredImage;
    data['rating'] = this.rating;
    data['hours'] = this.hours;
    data['days'] = this.days;
    data['phone_number'] = this.phoneNumber;
    data['distance'] = this.distance;
    data['latlong'] = this.latlong;
    data['waiting_time'] = this.waitingTime;
    return data;
  }
}
