class ReviewsResponse {
  int id;
  int pharmacyId;
  String review;
  int rating;
  String name;
  String email;
  dynamic phone;
  int status;
  String createdAt;
  String updatedAt;

  ReviewsResponse(
      {this.id,
        this.pharmacyId,
        this.review,
        this.rating,
        this.name,
        this.email,
        this.phone,
        this.status,
        this.createdAt,
        this.updatedAt});

  ReviewsResponse.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    pharmacyId = json['pharmacy_id'];
    review = json['review'];
    rating = json['rating'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['pharmacy_id'] = this.pharmacyId;
    data['review'] = this.review;
    data['rating'] = this.rating;
    data['name'] = this.name;
    data['email'] = this.email;
    data['phone'] = this.phone;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
