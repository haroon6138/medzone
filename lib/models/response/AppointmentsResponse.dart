class AppointmentsResponse {
  String response;
  List<AppointmentData> data;

  AppointmentsResponse({this.response, this.data});

  AppointmentsResponse.fromJson(Map<String, dynamic> json) {
    response = json['response'];
    if (json['data'] != null) {
      data = new List<AppointmentData>();
      json['data'].forEach((v) {
        data.add(new AppointmentData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['response'] = this.response;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class AppointmentData {
  int id;
  int customerId;
  int insuranceId;
  int hospitalId;
  int doctorId;
  String date;
  String time;
  int status;
  dynamic description;
  int isVirtual;
  String createdAt;
  String updatedAt;
  Doctor doctor;
  Customer customer;
  Hospital hospital;
  int expired;

  AppointmentData(
      {this.id,
        this.customerId,
        this.insuranceId,
        this.hospitalId,
        this.doctorId,
        this.date,
        this.time,
        this.status,
        this.description,
        this.isVirtual,
        this.createdAt,
        this.updatedAt,
        this.doctor,
        this.customer,
        this.hospital,
      this.expired});

  AppointmentData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    customerId = json['customer_id'];
    insuranceId = json['insurance_id'];
    hospitalId = json['hospital_id'];
    doctorId = json['doctor_id'];
    date = json['date'];
    time = json['time'];
    status = json['status'];
    description = json['description'];
    isVirtual = json['is_virtual'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    expired = json['expired'];
    doctor =
    json['doctor'] != null ? new Doctor.fromJson(json['doctor']) : null;
    customer = json['customer'] != null
        ? new Customer.fromJson(json['customer'])
        : null;
    hospital = json['hospital'] != null
        ? new Hospital.fromJson(json['hospital'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['customer_id'] = this.customerId;
    data['insurance_id'] = this.insuranceId;
    data['hospital_id'] = this.hospitalId;
    data['doctor_id'] = this.doctorId;
    data['date'] = this.date;
    data['time'] = this.time;
    data['status'] = this.status;
    data['description'] = this.description;
    data['is_virtual'] = this.isVirtual;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['expired'] = this.expired;
    if (this.doctor != null) {
      data['doctor'] = this.doctor.toJson();
    }
    if (this.customer != null) {
      data['customer'] = this.customer.toJson();
    }
    if (this.hospital != null) {
      data['hospital'] = this.hospital.toJson();
    }
    return data;
  }
}

class Doctor {
  int id;
  int roleId;
  String firstName;
  String lastName;
  String slug;
  String slugId;
  int rating;
  String email;
  String oneSignalId;
  String contact;
  String gender;
  String avatar;
  String avatarUrl;
  dynamic emailVerifiedAt;
  int video;
  String latlong;
  dynamic settings;
  String createdAt;
  String updatedAt;

  Doctor(
      {this.id,
        this.roleId,
        this.firstName,
        this.lastName,
        this.slug,
        this.slugId,
        this.rating,
        this.email,
        this.oneSignalId,
        this.contact,
        this.gender,
        this.avatar,
        this.avatarUrl,
        this.emailVerifiedAt,
        this.video,
        this.latlong,
        this.settings,
        this.createdAt,
        this.updatedAt});

  Doctor.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    roleId = json['role_id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    slug = json['slug'];
    slugId = json['slug_id'];
    rating = json['rating'];
    email = json['email'];
    oneSignalId = json['one_signal_id'];
    contact = json['contact'];
    gender = json['gender'];
    avatar = json['avatar'];
    avatarUrl = json['avatar_url'];
    emailVerifiedAt = json['email_verified_at'];
    video = json['video'];
    latlong = json['latlong'];
    settings = json['settings'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['role_id'] = this.roleId;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['slug'] = this.slug;
    data['slug_id'] = this.slugId;
    data['rating'] = this.rating;
    data['email'] = this.email;
    data['one_signal_id'] = this.oneSignalId;
    data['contact'] = this.contact;
    data['gender'] = this.gender;
    data['avatar'] = this.avatar;
    data['avatar_url'] = this.avatarUrl;
    data['email_verified_at'] = this.emailVerifiedAt;
    data['video'] = this.video;
    data['latlong'] = this.latlong;
    data['settings'] = this.settings;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

class Customer {
  int id;
  int roleId;
  String fcmId;
  String firstName;
  String lastName;
  String slug;
  String slugId;
  String email;
  dynamic contact;
  dynamic gender;
  String avatar;
  String avatarUrl;
  dynamic emailVerifiedAt;
  dynamic latlong;
  dynamic settings;
  String createdAt;
  String updatedAt;

  Customer(
      {this.id,
        this.roleId,
        this.fcmId,
        this.firstName,
        this.lastName,
        this.slug,
        this.slugId,
        this.email,
        this.contact,
        this.gender,
        this.avatar,
        this.avatarUrl,
        this.emailVerifiedAt,
        this.latlong,
        this.settings,
        this.createdAt,
        this.updatedAt});

  Customer.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    roleId = json['role_id'];
    fcmId = json['fcm_id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    slug = json['slug'];
    slugId = json['slug_id'];
    email = json['email'];
    contact = json['contact'];
    gender = json['gender'];
    avatar = json['avatar'];
    avatarUrl = json['avatar_url'];
    emailVerifiedAt = json['email_verified_at'];
    latlong = json['latlong'];
    settings = json['settings'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['role_id'] = this.roleId;
    data['fcm_id'] = this.fcmId;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['slug'] = this.slug;
    data['slug_id'] = this.slugId;
    data['email'] = this.email;
    data['contact'] = this.contact;
    data['gender'] = this.gender;
    data['avatar'] = this.avatar;
    data['avatar_url'] = this.avatarUrl;
    data['email_verified_at'] = this.emailVerifiedAt;
    data['latlong'] = this.latlong;
    data['settings'] = this.settings;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

class Hospital {
  int id;
  String name;
  String email;
  String password;
  String phoneNumber;
  String address1;
  String address2;
  String city;
  String state;
  String postalCode;
  String webSiteUrl;
  String hours;
  String auther;
  String createdAt;
  String updatedAt;
  dynamic logo;
  dynamic waitingTime;
  int status;
  String country;
  dynamic featuredImage;
  String latlong;
  int rating;
  int searchCount;
  int recommended;

  Hospital(
      {this.id,
        this.name,
        this.email,
        this.password,
        this.phoneNumber,
        this.address1,
        this.address2,
        this.city,
        this.state,
        this.postalCode,
        this.webSiteUrl,
        this.hours,
        this.auther,
        this.createdAt,
        this.updatedAt,
        this.logo,
        this.waitingTime,
        this.status,
        this.country,
        this.featuredImage,
        this.latlong,
        this.rating,
        this.searchCount,
        this.recommended});

  Hospital.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    password = json['password'];
    phoneNumber = json['phone_number'];
    address1 = json['address_1'];
    address2 = json['address_2'];
    city = json['city'];
    state = json['state'];
    postalCode = json['postal_code'];
    webSiteUrl = json['web_site_url'];
    hours = json['hours'];
    auther = json['auther'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    logo = json['logo'];
    waitingTime = json['waiting_time'];
    status = json['status'];
    country = json['country'];
    featuredImage = json['featured_image'];
    latlong = json['latlong'];
    rating = json['rating'];
    searchCount = json['search_count'];
    recommended = json['recommended'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['password'] = this.password;
    data['phone_number'] = this.phoneNumber;
    data['address_1'] = this.address1;
    data['address_2'] = this.address2;
    data['city'] = this.city;
    data['state'] = this.state;
    data['postal_code'] = this.postalCode;
    data['web_site_url'] = this.webSiteUrl;
    data['hours'] = this.hours;
    data['auther'] = this.auther;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['logo'] = this.logo;
    data['waiting_time'] = this.waitingTime;
    data['status'] = this.status;
    data['country'] = this.country;
    data['featured_image'] = this.featuredImage;
    data['latlong'] = this.latlong;
    data['rating'] = this.rating;
    data['search_count'] = this.searchCount;
    data['recommended'] = this.recommended;
    return data;
  }
}
