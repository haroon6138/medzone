class SearchMedicineResponse {
  String response;
  List<Data> data;

  SearchMedicineResponse({this.response, this.data});

  SearchMedicineResponse.fromJson(Map<String, dynamic> json) {
    response = json['response'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['response'] = this.response;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int id;
  String name;
  String formulastrength;
  String supplier;

  Data(
      {this.id,
        this.name,
        this.formulastrength,
        this.supplier});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    formulastrength = json['formulastrength'];
    supplier = json['supplier'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['formulastrength'] = this.formulastrength;
    data['supplier'] = this.supplier;
    return data;
  }
}
