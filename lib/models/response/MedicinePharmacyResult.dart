class MedicinePharmacyResult {
  String response;
  List<Data> data;

  MedicinePharmacyResult({this.response, this.data});

  MedicinePharmacyResult.fromJson(Map<String, dynamic> json) {
    response = json['response'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['response'] = this.response;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int id;
  String name;
  String address1;
  String logo;
  int recommended;
  String featuredImage;
  int rating;
  List<String> hours;
  List<String> days;
  String phoneNumber;
  String latlong;
  String distance;
  List<Packages> packages;

  Data(
      {this.id,
        this.name,
        this.address1,
        this.logo,
        this.recommended,
        this.featuredImage,
        this.rating,
        this.hours,
        this.days,
        this.phoneNumber,
        this.latlong,
        this.distance,
        this.packages});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    address1 = json['address_1'];
    logo = json['logo'];
    recommended = json['recommended'];
    featuredImage = json['featured_image'];
    rating = json['rating'];
    hours = json['hours'].cast<String>();
    days = json['days'].cast<String>();
    phoneNumber = json['phone_number'];
    latlong = json['latlong'];
    distance = json['distance'];
    if (json['packages'] != null) {
      packages = new List<Packages>();
      json['packages'].forEach((v) {
        packages.add(new Packages.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['address_1'] = this.address1;
    data['logo'] = this.logo;
    data['recommended'] = this.recommended;
    data['featured_image'] = this.featuredImage;
    data['rating'] = this.rating;
    data['hours'] = this.hours;
    data['days'] = this.days;
    data['phone_number'] = this.phoneNumber;
    data['latlong'] = this.latlong;
    data['distance'] = this.distance;
    if (this.packages != null) {
      data['packages'] = this.packages.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Packages {
  int id;
  String name;
  int quantity;
  dynamic price;

  Packages({this.id, this.name, this.quantity, this.price});

  Packages.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    quantity = json['quantity'];
    price = json['price'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['quantity'] = this.quantity;
    data['price'] = this.price;
    return data;
  }
}