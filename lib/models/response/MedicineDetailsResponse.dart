class MedicineDetailsResponse {
  String response;
  List<Data> data;

  MedicineDetailsResponse({this.response, this.data});

  MedicineDetailsResponse.fromJson(Map<String, dynamic> json) {
    response = json['response'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['response'] = this.response;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int id;
  String name;
  String formulastrength;
  int status;
  String createdAt;
  String updatedAt;
  String imprint;
  String color;
  String size;
  String shape;
  String availability;
  String drugClass;
  String pregnancyCategory;
  String csaSchedule;
  String supplier;
  String ndc;
  int searchCount;

  Data(
      {this.id,
        this.name,
        this.formulastrength,
        this.status,
        this.createdAt,
        this.updatedAt,
        this.imprint,
        this.color,
        this.size,
        this.shape,
        this.availability,
        this.drugClass,
        this.pregnancyCategory,
        this.csaSchedule,
        this.supplier,
        this.ndc,
        this.searchCount});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    formulastrength = json['formulastrength'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    imprint = json['imprint'];
    color = json['color'];
    size = json['size'];
    shape = json['shape'];
    availability = json['availability'];
    drugClass = json['drug_class'];
    pregnancyCategory = json['pregnancy_category'];
    csaSchedule = json['csa_schedule'];
    supplier = json['supplier'];
    ndc = json['ndc'];
    searchCount = json['search_count'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['formulastrength'] = this.formulastrength;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['imprint'] = this.imprint;
    data['color'] = this.color;
    data['size'] = this.size;
    data['shape'] = this.shape;
    data['availability'] = this.availability;
    data['drug_class'] = this.drugClass;
    data['pregnancy_category'] = this.pregnancyCategory;
    data['csa_schedule'] = this.csaSchedule;
    data['supplier'] = this.supplier;
    data['ndc'] = this.ndc;
    data['search_count'] = this.searchCount;
    return data;
  }
}
