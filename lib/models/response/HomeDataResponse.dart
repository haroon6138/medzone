import 'package:MedZone/models/response/MedicinePharmacyResult.dart';

class HomeData {
  String response;
  Count count;
  TopResult topResult;

  HomeData({this.response, this.count, this.topResult});

  HomeData.fromJson(Map<String, dynamic> json) {
    response = json['response'];
    count = json['count'] != null ? new Count.fromJson(json['count']) : null;
    topResult = json['topResult'] != null
        ? new TopResult.fromJson(json['topResult'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['response'] = this.response;
    if (this.count != null) {
      data['count'] = this.count.toJson();
    }
    if (this.topResult != null) {
      data['topResult'] = this.topResult.toJson();
    }
    return data;
  }
}

class Count {
  int hospital;
  int harmReduction;
  int pharmacy;
  int urgentCare;
  int chiropractory;
  int laboratory;
  int ophthalmologist;
  int physicalTherapy;
  int radiologyCentre;
  int veterinarian;

  Count(
      {this.hospital,
      this.harmReduction,
      this.pharmacy,
      this.urgentCare,
      this.chiropractory,
      this.laboratory,
      this.ophthalmologist,
      this.physicalTherapy,
      this.radiologyCentre,
      this.veterinarian});

  Count.fromJson(Map<String, dynamic> json) {
    hospital = json['hospital'];
    harmReduction = json['harmReduction'];
    pharmacy = json['pharmacy'];
    urgentCare = json['urgentCare'];
    chiropractory = json['chiropractory'];
    laboratory = json['laboratory'];
    ophthalmologist = json['ophthalmologist'];
    physicalTherapy = json['physicalTherapy'];
    radiologyCentre = json['radiologyCentre'];
    veterinarian = json['veterinarian'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['hospital'] = this.hospital;
    data['harmReduction'] = this.harmReduction;
    data['pharmacy'] = this.pharmacy;
    data['urgentCare'] = this.urgentCare;
    data['chiropractory'] = this.chiropractory;
    data['laboratory'] = this.laboratory;
    data['ophthalmologist'] = this.ophthalmologist;
    data['physicalTherapy'] = this.physicalTherapy;
    data['radiologyCentre'] = this.radiologyCentre;
    data['veterinarian'] = this.veterinarian;
    return data;
  }
}

class TopResult {
  List<Hospital> hospital = List();
  List<HarmReduction> harmReduction = List();
  List<Pharmacy> pharmacy = List();
  List<UrgentCare> urgentCare = List();

  TopResult(
      {this.hospital, this.harmReduction, this.pharmacy, this.urgentCare});

  TopResult.fromJson(Map<String, dynamic> json) {
    if (json['hospital'] != null) {
      hospital = new List<Hospital>();
      json['hospital'].forEach((v) {
        hospital.add(new Hospital.fromJson(v));
      });
    }
    if (json['harmReduction'] != null) {
      harmReduction = new List<HarmReduction>();
      json['harmReduction'].forEach((v) {
        harmReduction.add(new HarmReduction.fromJson(v));
      });
    }
    if (json['pharmacy'] != null) {
      pharmacy = new List<Pharmacy>();
      json['pharmacy'].forEach((v) {
        pharmacy.add(new Pharmacy.fromJson(v));
      });
    }
    if (json['urgentCare'] != null) {
      urgentCare = new List<UrgentCare>();
      json['urgentCare'].forEach((v) {
        urgentCare.add(new UrgentCare.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.hospital != null) {
      data['hospital'] = this.hospital.map((v) => v.toJson()).toList();
    }
    if (this.harmReduction != null) {
      data['harmReduction'] =
          this.harmReduction.map((v) => v.toJson()).toList();
    }
    if (this.pharmacy != null) {
      data['pharmacy'] = this.pharmacy.map((v) => v.toJson()).toList();
    }
    if (this.urgentCare != null) {
      data['urgentCare'] = this.urgentCare.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Hospital {
  int id;
  String name;
  String address1;
  String logo;
  int rating;
  List<String> hours;
  List<String> days;
  String phoneNumber;
  String latlong;
  String distance;
  dynamic waitingTime;

  Hospital(
      {this.id,
      this.name,
      this.address1,
      this.logo,
      this.rating,
      this.hours,
      this.days,
      this.phoneNumber,
      this.latlong,
      this.distance,
      this.waitingTime});

  Hospital.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    address1 = json['address_1'];
    logo = json['logo'];
    rating = json['rating'];
    hours = json['hours'].cast<String>();
    days = json['days'].cast<String>();
    phoneNumber = json['phone_number'];
    latlong = json['latlong'];
    distance = json['distance'];
    waitingTime = json['waiting_time'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['address_1'] = this.address1;
    data['logo'] = this.logo;
    data['rating'] = this.rating;
    data['hours'] = this.hours;
    data['days'] = this.days;
    data['phone_number'] = this.phoneNumber;
    data['latlong'] = this.latlong;
    data['distance'] = this.distance;
    data['waiting_time'] = this.waitingTime;
    return data;
  }
}

class Pharmacy {
  int id;
  String name;
  String address1;
  String logo;
  int rating;
  List<String> hours;
  List<String> days;
  String phoneNumber;
  String latlong;
  String distance;
  List<Packages> packages;

  Pharmacy(
      {this.id,
      this.name,
      this.address1,
      this.logo,
      this.rating,
      this.hours,
      this.days,
      this.phoneNumber,
      this.latlong,
      this.distance,
      this.packages});

  Pharmacy.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    address1 = json['address_1'];
    logo = json['logo'];
    rating = json['rating'];
    hours = json['hours'].cast<String>();
    days = json['days'].cast<String>();
    phoneNumber = json['phone_number'];
    latlong = json['latlong'];
    distance = json['distance'];
    packages = json['packages'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['address_1'] = this.address1;
    data['logo'] = this.logo;
    data['rating'] = this.rating;
    data['hours'] = this.hours;
    data['days'] = this.days;
    data['phone_number'] = this.phoneNumber;
    data['latlong'] = this.latlong;
    data['distance'] = this.distance;
    data['packages'] = this.packages;
    return data;
  }
}

class HarmReduction {
  int id;
  String name;
  String address1;
  String logo;
  int rating;
  List<String> hours;
  List<String> days;
  String phoneNumber;
  String latlong;
  String distance;

  HarmReduction(
      {this.id,
      this.name,
      this.address1,
      this.logo,
      this.rating,
      this.hours,
      this.days,
      this.phoneNumber,
      this.latlong,
      this.distance});

  HarmReduction.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    address1 = json['address_1'];
    logo = json['logo'];
    rating = json['rating'];
    hours = json['hours'].cast<String>();
    days = json['days'].cast<String>();
    phoneNumber = json['phone_number'];
    latlong = json['latlong'];
    distance = json['distance'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['address_1'] = this.address1;
    data['logo'] = this.logo;
    data['rating'] = this.rating;
    data['hours'] = this.hours;
    data['days'] = this.days;
    data['phone_number'] = this.phoneNumber;
    data['latlong'] = this.latlong;
    data['distance'] = this.distance;
    return data;
  }
}

class UrgentCare {
  int id;
  String name;
  String address1;
  String logo;
  int rating;
  List<String> hours;
  List<String> days;
  String phoneNumber;
  String latlong;
  String distance;
  dynamic waitingTime;

  UrgentCare(
      {this.id,
      this.name,
      this.address1,
      this.logo,
      this.rating,
      this.hours,
      this.days,
      this.phoneNumber,
      this.latlong,
      this.distance,
      this.waitingTime});

  UrgentCare.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    address1 = json['address_1'];
    logo = json['logo'];
    rating = json['rating'];
    hours = json['hours'].cast<String>();
    days = json['days'].cast<String>();
    phoneNumber = json['phone_number'];
    latlong = json['latlong'];
    distance = json['distance'];
    waitingTime = json['waiting_time'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['address_1'] = this.address1;
    data['logo'] = this.logo;
    data['rating'] = this.rating;
    data['hours'] = this.hours;
    data['days'] = this.days;
    data['phone_number'] = this.phoneNumber;
    data['latlong'] = this.latlong;
    data['distance'] = this.distance;
    data['waiting_time'] = this.waitingTime;
    return data;
  }
}
