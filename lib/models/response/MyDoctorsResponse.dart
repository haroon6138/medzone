class MyDoctorsResponse {
  String response;
  List<Data> data;

  MyDoctorsResponse({this.response, this.data});

  MyDoctorsResponse.fromJson(Map<String, dynamic> json) {
    response = json['response'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['response'] = this.response;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int id;
  int roleId;
  String firstName;
  String lastName;
  String slug;
  String slugId;
  dynamic videoSupport;
  int rating;
  String email;
  String oneSignalId;
  String contact;
  String gender;
  String avatar;
  String avatarUrl;
  dynamic emailVerifiedAt;
  int video;
  String latlong;
  dynamic settings;
  String createdAt;
  String updatedAt;
  Speciality speciality;
  List<HospitalsData> hospitalsData;

  Data(
      {this.id,
        this.roleId,
        this.firstName,
        this.lastName,
        this.slug,
        this.slugId,
        this.videoSupport,
        this.rating,
        this.email,
        this.oneSignalId,
        this.contact,
        this.gender,
        this.avatar,
        this.avatarUrl,
        this.emailVerifiedAt,
        this.video,
        this.latlong,
        this.settings,
        this.createdAt,
        this.updatedAt,
        this.speciality,
        this.hospitalsData});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    roleId = json['role_id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    slug = json['slug'];
    slugId = json['slug_id'];
    videoSupport = json['video_support'];
    rating = json['rating'];
    email = json['email'];
    oneSignalId = json['one_signal_id'];
    contact = json['contact'];
    gender = json['gender'];
    avatar = json['avatar'];
    avatarUrl = json['avatar_url'];
    emailVerifiedAt = json['email_verified_at'];
    video = json['video'];
    latlong = json['latlong'];
    settings = json['settings'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    speciality = json['speciality'] != null
        ? new Speciality.fromJson(json['speciality'])
        : null;
    if (json['hospitalsData'] != null) {
      hospitalsData = new List<HospitalsData>();
      json['hospitalsData'].forEach((v) {
        hospitalsData.add(new HospitalsData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['role_id'] = this.roleId;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['slug'] = this.slug;
    data['slug_id'] = this.slugId;
    data['video_support'] = this.videoSupport;
    data['rating'] = this.rating;
    data['email'] = this.email;
    data['one_signal_id'] = this.oneSignalId;
    data['contact'] = this.contact;
    data['gender'] = this.gender;
    data['avatar'] = this.avatar;
    data['avatar_url'] = this.avatarUrl;
    data['email_verified_at'] = this.emailVerifiedAt;
    data['video'] = this.video;
    data['latlong'] = this.latlong;
    data['settings'] = this.settings;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.speciality != null) {
      data['speciality'] = this.speciality.toJson();
    }
    if (this.hospitalsData != null) {
      data['hospitalsData'] =
          this.hospitalsData.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Speciality {
  int id;
  String name;
  int doctorTypeId;
  int status;
  String createdAt;
  String updatedAt;

  Speciality(
      {this.id,
        this.name,
        this.doctorTypeId,
        this.status,
        this.createdAt,
        this.updatedAt});

  Speciality.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    doctorTypeId = json['doctor_type_id'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['doctor_type_id'] = this.doctorTypeId;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

class HospitalsData {
  int id;
  String name;
  String address1;
  String latlong;
  String waitingTime;
  String logo;
  int recommended;
  String featuredImage;
  int rating;
  String hours;
  String phoneNumber;
  int hospitalId;
  String hospitalName;
  String hospitalAddress;
  int hospitalRecommended;
  String startTime;
  String endTime;

  HospitalsData(
      {this.id,
        this.name,
        this.address1,
        this.latlong,
        this.waitingTime,
        this.logo,
        this.recommended,
        this.featuredImage,
        this.rating,
        this.hours,
        this.phoneNumber,
        this.hospitalId,
        this.hospitalName,
        this.hospitalAddress,
        this.hospitalRecommended,
        this.startTime,
        this.endTime});

  HospitalsData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    address1 = json['address_1'];
    latlong = json['latlong'];
    waitingTime = json['waiting_time'];
    logo = json['logo'];
    recommended = json['recommended'];
    featuredImage = json['featured_image'];
    rating = json['rating'];
    hours = json['hours'];
    phoneNumber = json['phone_number'];
    hospitalId = json['hospital_id'];
    hospitalName = json['hospital_name'];
    hospitalAddress = json['hospital_address'];
    hospitalRecommended = json['hospital_recommended'];
    startTime = json['start_time'];
    endTime = json['end_time'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['address_1'] = this.address1;
    data['latlong'] = this.latlong;
    data['waiting_time'] = this.waitingTime;
    data['logo'] = this.logo;
    data['recommended'] = this.recommended;
    data['featured_image'] = this.featuredImage;
    data['rating'] = this.rating;
    data['hours'] = this.hours;
    data['phone_number'] = this.phoneNumber;
    data['hospital_id'] = this.hospitalId;
    data['hospital_name'] = this.hospitalName;
    data['hospital_address'] = this.hospitalAddress;
    data['hospital_recommended'] = this.hospitalRecommended;
    data['start_time'] = this.startTime;
    data['end_time'] = this.endTime;
    return data;
  }
}
