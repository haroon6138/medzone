class AuthenticatedUser {
  String token;
  String tokenType;
  int expiresIn;
  User user;

  AuthenticatedUser({this.token, this.tokenType, this.expiresIn, this.user});

  AuthenticatedUser.fromJson(Map<String, dynamic> json) {
    token = json['token'];
    tokenType = json['token_type'];
    expiresIn = json['expires_in'];
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['token'] = this.token;
    data['token_type'] = this.tokenType;
    data['expires_in'] = this.expiresIn;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    return data;
  }
}

class User {
  int id;
  int roleId;
  String fcmId;
  String firstName;
  String lastName;
  String slug;
  String slugId;
  String email;
  Null contact;
  Null gender;
  String avatar;
  String avatarUrl;
  Null emailVerifiedAt;
  Null latlong;
  Null settings;
  String createdAt;
  String updatedAt;

  User(
      {this.id,
        this.roleId,
        this.fcmId,
        this.firstName,
        this.lastName,
        this.slug,
        this.slugId,
        this.email,
        this.contact,
        this.gender,
        this.avatar,
        this.avatarUrl,
        this.emailVerifiedAt,
        this.latlong,
        this.settings,
        this.createdAt,
        this.updatedAt});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    roleId = json['role_id'];
    fcmId = json['fcm_id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    slug = json['slug'];
    slugId = json['slug_id'];
    email = json['email'];
    contact = json['contact'];
    gender = json['gender'];
    avatar = json['avatar'];
    avatarUrl = json['avatar_url'];
    emailVerifiedAt = json['email_verified_at'];
    latlong = json['latlong'];
    settings = json['settings'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['role_id'] = this.roleId;
    data['fcm_id'] = this.fcmId;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['slug'] = this.slug;
    data['slug_id'] = this.slugId;
    data['email'] = this.email;
    data['contact'] = this.contact;
    data['gender'] = this.gender;
    data['avatar'] = this.avatar;
    data['avatar_url'] = this.avatarUrl;
    data['email_verified_at'] = this.emailVerifiedAt;
    data['latlong'] = this.latlong;
    data['settings'] = this.settings;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
