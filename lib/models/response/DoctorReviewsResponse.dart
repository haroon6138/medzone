class DoctorReviewsResponse {
  String response;
  List<Data> data;

  DoctorReviewsResponse({this.response, this.data});

  DoctorReviewsResponse.fromJson(Map<String, dynamic> json) {
    response = json['response'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['response'] = this.response;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int id;
  int doctorId;
  int customerId;
  int appointmentId;
  int rating;
  String review;
  dynamic status;
  String createdAt;
  String updatedAt;
  Customer customer;

  Data(
      {this.id,
        this.doctorId,
        this.customerId,
        this.appointmentId,
        this.rating,
        this.review,
        this.status,
        this.createdAt,
        this.updatedAt,
        this.customer});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    doctorId = json['doctor_id'];
    customerId = json['customer_id'];
    appointmentId = json['appointment_id'];
    rating = json['rating'];
    review = json['review'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    customer = json['customer'] != null
        ? new Customer.fromJson(json['customer'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['doctor_id'] = this.doctorId;
    data['customer_id'] = this.customerId;
    data['appointment_id'] = this.appointmentId;
    data['rating'] = this.rating;
    data['review'] = this.review;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.customer != null) {
      data['customer'] = this.customer.toJson();
    }
    return data;
  }
}

class Customer {
  int id;
  int roleId;
  String fcmId;
  String firstName;
  String lastName;
  String slug;
  String slugId;
  String email;
  String contact;
  String gender;
  String avatar;
  String avatarUrl;
  dynamic emailVerifiedAt;
  dynamic latlong;
  dynamic settings;
  String createdAt;
  String updatedAt;

  Customer(
      {this.id,
        this.roleId,
        this.fcmId,
        this.firstName,
        this.lastName,
        this.slug,
        this.slugId,
        this.email,
        this.contact,
        this.gender,
        this.avatar,
        this.avatarUrl,
        this.emailVerifiedAt,
        this.latlong,
        this.settings,
        this.createdAt,
        this.updatedAt});

  Customer.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    roleId = json['role_id'];
    fcmId = json['fcm_id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    slug = json['slug'];
    slugId = json['slug_id'];
    email = json['email'];
    contact = json['contact'];
    gender = json['gender'];
    avatar = json['avatar'];
    avatarUrl = json['avatar_url'];
    emailVerifiedAt = json['email_verified_at'];
    latlong = json['latlong'];
    settings = json['settings'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['role_id'] = this.roleId;
    data['fcm_id'] = this.fcmId;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['slug'] = this.slug;
    data['slug_id'] = this.slugId;
    data['email'] = this.email;
    data['contact'] = this.contact;
    data['gender'] = this.gender;
    data['avatar'] = this.avatar;
    data['avatar_url'] = this.avatarUrl;
    data['email_verified_at'] = this.emailVerifiedAt;
    data['latlong'] = this.latlong;
    data['settings'] = this.settings;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
