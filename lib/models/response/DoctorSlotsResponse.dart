class DoctorSlotsResponse {
  String response;
  List<String> data;

  DoctorSlotsResponse({this.response, this.data});

  DoctorSlotsResponse.fromJson(Map<String, dynamic> json) {
    response = json['response'];
    data = json['data'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['response'] = this.response;
    data['data'] = this.data;
    return data;
  }
}
