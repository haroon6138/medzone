import 'package:MedZone/models/response/AppointmentsResponse.dart';
import 'package:flutter/cupertino.dart';

class AppointmentsProvider with ChangeNotifier {
  AppointmentsResponse appointmentsResponse;

  List<AppointmentData> pastAppointmentsData = [];
  List<AppointmentData> upcomingAppointmentsData = [];

  AppointmentsResponse getAppointments() => appointmentsResponse;

  List<AppointmentData> getPastAppointments() => pastAppointmentsData;

  List<AppointmentData> getUpcomingAppointments() => upcomingAppointmentsData;

  setAppointmentsResponse(AppointmentsResponse appointmentsResponse) {
    pastAppointmentsData.clear();
    upcomingAppointmentsData.clear();
    this.appointmentsResponse = appointmentsResponse;
    this.appointmentsResponse.data.forEach((element) {
      if (element.expired == 1) pastAppointmentsData.add(element);
      if (element.expired == 0) upcomingAppointmentsData.add(element);
    });
    notifyListeners();
  }
}
