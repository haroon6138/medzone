class IncomingCallData {
  String msgID;
  String notfID;
  String notificationForeground;
  String count;
  String message;
  String appointmentNo;
  String title;
  String projectID;
  String notificationType;
  String background;
  String doctorname;
  String clinicname;
  String speciality;
  String appointmentdate;
  String appointmenttime;
  String type;
  String sessionId;
  String identity;
  String name;
  String videoUrl;
  String picture;
  String isCall;
  String sound;

  IncomingCallData(
      {this.msgID,
        this.notfID,
        this.notificationForeground,
        this.count,
        this.message,
        this.appointmentNo,
        this.title,
        this.projectID,
        this.notificationType,
        this.background,
        this.doctorname,
        this.clinicname,
        this.speciality,
        this.appointmentdate,
        this.appointmenttime,
        this.type,
        this.sessionId,
        this.identity,
        this.name,
        this.videoUrl,
        this.picture,
        this.isCall,
        this.sound});

  IncomingCallData.fromJson(Map<String, dynamic> json) {
    msgID = json['msgID'];
    notfID = json['notfID'];
    notificationForeground = json['notification_foreground'];
    count = json['count'];
    message = json['message'];
    appointmentNo = json['AppointmentNo'];
    title = json['title'];
    projectID = json['ProjectID'];
    notificationType = json['NotificationType'];
    background = json['background'];
    doctorname = json['doctorname'];
    clinicname = json['clinicname'];
    speciality = json['speciality'];
    appointmentdate = json['appointmentdate'];
    appointmenttime = json['appointmenttime'];
    type = json['type'];
    sessionId = json['session_id'];
    identity = json['identity'];
    name = json['name'];
    videoUrl = json['videoUrl'];
    picture = json['picture'];
    isCall = json['is_call'];
    sound = json['sound'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['msgID'] = this.msgID;
    data['notfID'] = this.notfID;
    data['notification_foreground'] = this.notificationForeground;
    data['count'] = this.count;
    data['message'] = this.message;
    data['AppointmentNo'] = this.appointmentNo;
    data['title'] = this.title;
    data['ProjectID'] = this.projectID;
    data['NotificationType'] = this.notificationType;
    data['background'] = this.background;
    data['doctorname'] = this.doctorname;
    data['clinicname'] = this.clinicname;
    data['speciality'] = this.speciality;
    data['appointmentdate'] = this.appointmentdate;
    data['appointmenttime'] = this.appointmenttime;
    data['type'] = this.type;
    data['session_id'] = this.sessionId;
    data['identity'] = this.identity;
    data['name'] = this.name;
    data['videoUrl'] = this.videoUrl;
    data['picture'] = this.picture;
    data['is_call'] = this.isCall;
    data['sound'] = this.sound;
    return data;
  }
}
