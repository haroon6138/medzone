import 'package:MedZone/models/response/ReviewsResponse.dart';

class topFindings {
  final id;
  final type;
  final image;
  final featuredImage;
  final name;
  final address;
  final distance;
  final days;
  final timing;
  final deliveryTiming;
  final double rating;
  final contactNo;
  final webURL;
  dynamic waitingTime;

  final double latitude;
  final double longitude;

  List<String> insurances;
  List<String> services;

  List<ReviewsResponse> reviewsList;

  topFindings(
      {this.id,
      this.type,
      this.address,
      this.image,
      this.featuredImage,
      this.name,
      this.distance,
      this.days,
      this.timing,
      this.deliveryTiming,
      this.contactNo,
      this.webURL,
      this.latitude,
      this.longitude,
      this.rating,
      this.insurances,
      this.services,
      this.waitingTime,
      this.reviewsList});

  getTopFindingsListPhar() {
    List<topFindings> pharList = [];

    var topFindingsPhar1 = new topFindings(
        address: "304 W 46th St, New York, NY 10036",
        image: "assets/images/pharmacy.png",
        name: "Duane Reade Pharmacy",
        distance: "1.70",
        days: "Monday - Friday",
        timing: "11:00 am - 09:00 pm",
        deliveryTiming: "05:00 pm",
        contactNo: "+12127947100",
        latitude: 40.7487573,
        longitude: -73.9998001,
        webURL: "https://walgreens.com/",
        rating: 4.0);

    var topFindingsPhar2 = new topFindings(
        address: "630 Lexington Ave, New York, NY 10022",
        image: "assets/images/cvs_pharm.png",
        name: "CVS Pharmacy",
        distance: "1.34",
        days: "Monday - Sunday",
        timing: "11:00 am - 11:00 pm",
        deliveryTiming: "09:00 pm",
        contactNo: "+12122495198",
        latitude: 40.7488449,
        longitude: -73.9998001,
        webURL: "https://cvs.com/",
        rating: 4.5);

    pharList.add(topFindingsPhar1);
    pharList.add(topFindingsPhar2);

    return pharList;
  }

  getTopFindingsListHosp() {
    List<topFindings> hospList = [];

    var topFindingsHosp1 = new topFindings(
        address: "173 E 88th St, New York, NY 10128",
        image: "assets/images/pharmacy.png",
        name: "NY Hospital",
        distance: "2.32",
        days: "Monday - Sunday",
        timing: "11:00 am - 11:00 pm",
        deliveryTiming: "04:00 pm",
        rating: 4.5,
        latitude: 40.7487573,
        longitude: -73.9998001,
        webURL: "https://nychealthandhospitals.org/",
        contactNo: "+12123052500");

    var topFindingsHosp2 = new topFindings(
        address: "420 E 76th St, New York, NY 10021",
        image: "assets/images/cvs_pharm.png",
        name: "Gracie Square Hospital",
        distance: "3.34",
        days: "Monday - Sunday",
        timing: "11:00 am - 11:00 pm",
        deliveryTiming: "07:00 pm",
        contactNo: "+12124345300",
        latitude: 40.7487573,
        longitude: -73.9998001,
        webURL: "https://nygsh.org/",
        rating: 5.0);

    hospList.add(topFindingsHosp1);
    hospList.add(topFindingsHosp2);

    return hospList;
  }
}
