import 'package:flutter/material.dart';

class BottomNavBarItem {
  const BottomNavBarItem(this.title, this.icon, this.color);

  final String title;
  final IconData icon;
  final MaterialColor color;
}

const List<BottomNavBarItem> allDestinations = <BottomNavBarItem>[
  BottomNavBarItem('Home', Icons.home, Colors.teal),
  BottomNavBarItem('Business', Icons.business, Colors.cyan),
  BottomNavBarItem('School', Icons.school, Colors.orange),
  BottomNavBarItem('Flight', Icons.flight, Colors.blue)
];
