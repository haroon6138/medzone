import 'package:flutter/material.dart';

class CurrentLocation with ChangeNotifier {
  var currentLatitude;
  var currentLongitude;
  var currentAddress;

  CurrentLocation(
      this.currentLatitude, this.currentLongitude, this.currentAddress);

  getCurrentLat() => currentLatitude;

  getCurrentLong() => currentLongitude;

  getCurrentAddress() => currentAddress;

  setCurrentLat(var lat) {
    currentLatitude = lat;
    notifyListeners();
  }

  setCurrentLong(var long) {
    currentLongitude = long;
    notifyListeners();
  }

  setCurrentAddress(var address) {
    currentAddress = address;
    notifyListeners();
  }
}
