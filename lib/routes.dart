import 'package:MedZone/main.dart';
import 'package:MedZone/pages/myAccount.dart';

const String INIT_ROUTE = '/';
const String ROOT = 'root';
const String MY_ACCOUNT = 'my-account';
const String HOME = '/';
var routes = {
  // ROOT: (_) => RootPage(),
  HOME: (_) => MainPage(),
  MY_ACCOUNT: (_) => MyAccount()
};
