import 'package:flutter/material.dart';

class CustomTextStyle {
  static TextStyle display5(BuildContext context) {
    return Theme.of(context).textTheme.display4.copyWith(
          fontSize: 16.0,
          fontWeight: FontWeight.bold,
          letterSpacing: 0.5,
          color: Colors.white,
          fontFamily: 'Open-Sans',
        );
  }
}