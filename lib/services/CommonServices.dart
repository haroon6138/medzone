import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

class CommonService {
  static String currentLat = "current_lat";
  static String currentLong = "current_long";
  static String currentAddress = "current_address";

  static String loadingMessage = tr('loading');

  static String mapStyle =
      '[{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#6195a0"}]},{"featureType":"administrative.province","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"lightness":"0"},{"saturation":"0"},{"color":"#f5f5f2"},{"gamma":"1"}]},{"featureType":"landscape.man_made","elementType":"all","stylers":[{"lightness":"-3"},{"gamma":"1.00"}]},{"featureType":"landscape.natural.terrain","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#bae5ce"},{"visibility":"on"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45},{"visibility":"simplified"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#fac9a9"},{"visibility":"simplified"}]},{"featureType":"road.highway","elementType":"labels.text","stylers":[{"color":"#4e4e4e"}]},{"featureType":"road.arterial","elementType":"labels.text.fill","stylers":[{"color":"#787878"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"transit.station.airport","elementType":"labels.icon","stylers":[{"hue":"#0a00ff"},{"saturation":"-77"},{"gamma":"0.57"},{"lightness":"0"}]},{"featureType":"transit.station.rail","elementType":"labels.text.fill","stylers":[{"color":"#43321e"}]},{"featureType":"transit.station.rail","elementType":"labels.icon","stylers":[{"hue":"#ff6c00"},{"lightness":"4"},{"gamma":"0.75"},{"saturation":"-68"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#eaf6f8"},{"visibility":"on"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#c7eced"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"lightness":"-49"},{"saturation":"-53"},{"gamma":"0.79"}]}]';

  static String en = "assets/json/en.json";
  static String span = "assets/json/spanish.json";

  static setPrefs(String key, String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(key, value);
  }

  static getPrefs(String key, [bool remove = true]) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var strValue = prefs.getString(key);
    if (remove) prefs.remove(key);
    return strValue;
  }

  static setPrefsDouble(String key, double value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setDouble(key, value);
  }

  static getPrefsDouble(String key, [bool remove = true]) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    double strValue = prefs.getDouble(key);
    if (remove) prefs.remove(key);
    return strValue;
  }

  static removePref(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove(key);
  }

  static getLoadingMessage() {
    return tr('loading');
  }

//  static showProgressDialog(context) async {
//    String message = tr('loading');
//    pr = ProgressDialog(context,
//        type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
//    pr.style(
//        message: message,
//        borderRadius: 10.0,
//        backgroundColor: Colors.white,
//        elevation: 10.0,
//        insetAnimCurve: Curves.easeInOut,
//        progress: 0.0,
//        maxProgress: 100.0,
//        progressTextStyle: TextStyle(
//            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
//        messageTextStyle: TextStyle(
//            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600));
//
//    if (!pr.isShowing()) {
//      await pr.show();
//    } else {
//      await pr.hide().then((value) async {
//        await pr.show();
//      });
//    }
//  }

  static String getDays(List<String> days) {
    return days[0] + " - " + days[1];
  }

  static String getTimings(List<String> timings) {
    return timings[0] + " - " + timings[1];
  }

//
//  static hideProgressDialog() async {
//    await pr.hide();
//  }

  static showToast(context, String text) {
    Toast.show(text, context,
        duration: Toast.LENGTH_LONG,
        gravity: Toast.BOTTOM,
        backgroundColor: new Color(0xff17AF6B).withOpacity(1.0),
        textColor: Colors.white,
        backgroundRadius: 5.0);
  }
}
