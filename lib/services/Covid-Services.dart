import 'dart:io';

import 'package:MedZone/config/base_app_client.dart';
import 'package:MedZone/config/config.dart';
import 'package:MedZone/services/CommonServices.dart';
import 'package:flutter/cupertino.dart';

class APIService {
  Future<Map<String, dynamic>> getCovidStatsByCounty(
      String countyName, BuildContext context) async {
    dynamic localRes;

    await BaseAppClient.get(GET_COVID_UPDATES_BY_COUNTY + countyName, context,
        isJWT: false, isAuth: false, onSuccess: (response, statusCode) async {
      localRes = response[0];
    }, onFailure: (String error, int statusCode) {
      throw error;
    });
    return Future.value(localRes);
  }

  Future<Map<String, dynamic>> getTopMedicines(BuildContext context) async {
    dynamic localRes;
    await BaseAppClient.get(BASE_URL + TOP_MEDS, context,
        isAuth: false, isJWT: false, onSuccess: (response, statusCode) async {
      localRes = response;
    }, onFailure: (String error, int statusCode) {
      throw error;
    });
    return Future.value(localRes);
  }

  Future<Map<String, dynamic>> getHomeData(
      String lat, String long, BuildContext context) async {
    Map<String, dynamic> request;
    request = {"lat": lat, "long": long};

    dynamic localRes;

    await BaseAppClient.post(HOME_DATA, context, isJWT: false,
        onSuccess: (response, statusCode) async {
      localRes = response;
    }, onFailure: (String error, int statusCode) {
      throw error;
    }, body: request);
    return Future.value(localRes);
  }

  Future<Map<String, dynamic>> getSearchMedicine(
      String searchQuery, BuildContext context) async {
    Map<String, dynamic> request;
    request = {"search": searchQuery};

    dynamic localRes;

    await BaseAppClient.post(SEARCH_MEDS, context, isJWT: false,
        onSuccess: (response, statusCode) async {
      localRes = response;
    }, onFailure: (String error, int statusCode) {
      throw error;
    }, body: request);
    return Future.value(localRes);
  }

  Future<Map<String, dynamic>> getMedicineDetails(
      String id, BuildContext context) async {
    Map<String, dynamic> request;
    request = {"id": id};

    dynamic localRes;

    await BaseAppClient.post(MED_DETAILS, context, isJWT: false,
        onSuccess: (response, statusCode) async {
      localRes = response;
    }, onFailure: (String error, int statusCode) {
      throw error;
    }, body: request);
    return Future.value(localRes);
  }

  Future<Map<String, dynamic>> getProviderDetails(int id, String type,
      double lat, double long, BuildContext context) async {
    Map<String, dynamic> request;
    request = {"lat": lat, "long": long, "type": type, "id": id};

    dynamic localRes;

    await BaseAppClient.post(PROVIDER_DETAILS, context, isJWT: false,
        onSuccess: (response, statusCode) async {
      localRes = response;
    }, onFailure: (String error, int statusCode) {
      throw error;
    }, body: request);
    return Future.value(localRes);
  }

  Future<Map<String, dynamic>> addReview(int id, String type, String name,
      String email, double rating, String review, BuildContext context) async {
    Map<String, dynamic> request;
    request = {
      "review": review,
      "rating": rating,
      "email": email,
      "name": name,
      "type": type,
      "id": id
    };

    dynamic localRes;

    await BaseAppClient.post(ADD_REVIEW, context, isJWT: false,
        onSuccess: (response, statusCode) async {
      localRes = response;
    }, onFailure: (String error, int statusCode) {
      throw error;
    }, body: request);
    return Future.value(localRes);
  }

  Future<Map<String, dynamic>> getMedicineAvailability(
      int id, String lat, String long, BuildContext context) async {
    Map<String, dynamic> request;
    request = {"lat": lat, "long": long, "id": id};

    dynamic localRes;

    await BaseAppClient.post(MEDICINE_PHARMACY, context, isJWT: false,
        onSuccess: (response, statusCode) async {
      localRes = response;
    }, onFailure: (String error, int statusCode) {
      throw error;
    }, body: request);
    return Future.value(localRes);
  }

  Future<Map<String, dynamic>> getAllNearestProviders(
      String type, String lat, String long, BuildContext context) async {
    Map<String, dynamic> request;
    request = {"lat": lat, "long": long, "type": type};

    dynamic localRes;

    await BaseAppClient.post(ALL_NEAREST, context, isJWT: false,
        onSuccess: (response, statusCode) async {
      localRes = response;
    }, onFailure: (String error, int statusCode) {
      throw error;
    }, body: request);
    return Future.value(localRes);
  }

  Future<Map<String, dynamic>> getCovidStatsByState(
      String stateName, BuildContext context) async {
    dynamic localRes;
    var statesURL = "";

    if (stateName.contains(" ")) {
      statesURL = GET_COVID_UPDATES_BY_STATE +
          stateName.replaceFirst(" ", "%20") +
          "?yesterday=true";
    } else {
      statesURL = GET_COVID_UPDATES_BY_STATE + stateName + "?yesterday=true";
    }

    await BaseAppClient.get(statesURL, context, isAuth: false, isJWT: false,
        onSuccess: (response, statusCode) async {
      localRes = response;
    }, onFailure: (String error, int statusCode) {
      throw error;
    });
    return Future.value(localRes);
  }

  Future<Map<String, dynamic>> getDoctorSpecialities(
      bool isAuth, BuildContext context) async {
    dynamic localRes;

    await BaseAppClient.get(GET_SPECIALITIES, context, isJWT: isAuth,
        onSuccess: (response, statusCode) async {
      localRes = response;
    }, onFailure: (String error, int statusCode) {
      throw error;
    });
    return Future.value(localRes);
  }

  Future<Map<String, dynamic>> getInsuranceCompanies(
      bool isAuth, BuildContext context) async {
    dynamic localRes;

    await BaseAppClient.get(GET_INSURANCES, context, isJWT: isAuth,
        onSuccess: (response, statusCode) async {
      localRes = response;
    }, onFailure: (String error, int statusCode) {
      throw error;
    });
    return Future.value(localRes);
  }

  Future<Map<String, dynamic>> getDoctors(int id, String lat, String long,
      String date, BuildContext context) async {
    Map<String, dynamic> request;
    request = {"lat": lat, "long": long, "special_id": id, "date": date};

    dynamic localRes;

    await BaseAppClient.post(GET_DOCTORS, context, isJWT: false,
        onSuccess: (response, statusCode) async {
      localRes = response;
    }, onFailure: (String error, int statusCode) {
      throw error;
    }, body: request);
    return Future.value(localRes);
  }

  Future<Map<String, dynamic>> getDoctorSlots(
      int doctorID, int hospitalID, String date, BuildContext context) async {
    Map<String, dynamic> request;
    request = {"doctor_id": doctorID, "hospital_id": hospitalID, "date": date};

    dynamic localRes;

    await BaseAppClient.post(GET_DOCTOR_SLOTS, context, isJWT: false,
        onSuccess: (response, statusCode) async {
      localRes = response;
    }, onFailure: (String error, int statusCode) {
      throw error;
    }, body: request);
    return Future.value(localRes);
  }

  Future<Map<String, dynamic>> makeAppointment(
      int doctorID,
      String time,
      String date,
      String type,
      String insuranceID,
      int hospitalID,
      int isVirtual,
      BuildContext context) async {
    var token = await CommonService.getPrefs(JWT_TOKEN, false);

    Map<String, dynamic> request;
    request = {
      "doctor_id": doctorID,
      "time": time,
      "date": date,
      "type": type,
      "insurance_id": insuranceID,
      "hospital_id": hospitalID,
      "is_virtual": isVirtual,
      "platform": Platform.isAndroid ? 1 : 2
    };

    dynamic localRes;

    await BaseAppClient.post(MAKE_APPOINTMENT, context,
        isJWT: true, authToken: token, onSuccess: (response, statusCode) async {
      localRes = response;
    }, onFailure: (String error, int statusCode) {
      throw error;
    }, body: request);
    return Future.value(localRes);
  }

  Future<Map<String, dynamic>> registerUser(
      String firstName,
      String lastName,
      String emailAddress,
      String password,
      String fcmToken,
      BuildContext context) async {
    Map<String, dynamic> request;
    request = {
      "email": emailAddress,
      "password": password,
      "first_name": firstName,
      "last_name": lastName,
      "fcm_id": fcmToken
    };

    dynamic localRes;

    await BaseAppClient.post(REGISTER_USER, context, isJWT: false,
        onSuccess: (response, statusCode) async {
      localRes = response;
    }, onFailure: (String error, int statusCode) {
      throw error;
    }, body: request);
    return Future.value(localRes);
  }

  Future<Map<String, dynamic>> loginUser(String emailAddress, String password,
      String fcmToken, BuildContext context) async {
    Map<String, dynamic> request;
    request = {"email": emailAddress, "password": password, "fcm_id": fcmToken};

    dynamic localRes;

    await BaseAppClient.post(LOGIN_USER, context, isJWT: false,
        onSuccess: (response, statusCode) async {
      localRes = response;
    }, onFailure: (String error, int statusCode) {
      throw error;
    }, body: request);
    return Future.value(localRes);
  }

  Future<Map<String, dynamic>> logoutUser(BuildContext context) async {
    Map<String, dynamic> request;

    var token = await CommonService.getPrefs(JWT_TOKEN, false);

    dynamic localRes;

    await BaseAppClient.post(LOGOUT_USER, context,
        isJWT: true, authToken: token, onSuccess: (response, statusCode) async {
      localRes = response;
    }, onFailure: (String error, int statusCode) {
      throw error;
    }, body: request);
    return Future.value(localRes);
  }

  Future<Map<String, dynamic>> getAppointmentsHistory(
      BuildContext context) async {
    var token = await CommonService.getPrefs(JWT_TOKEN, false);
    dynamic localRes;
    await BaseAppClient.get(APPOINTMENTS_HISTORY, context,
        isAuth: true,
        isJWT: true,
        authToken: token, onSuccess: (response, statusCode) async {
      localRes = response;
    }, onFailure: (String error, int statusCode) {
      throw error;
    });
    return Future.value(localRes);
  }

  Future<Map<String, dynamic>> cancelAppointment(
      int appoID, BuildContext context) async {
    Map<String, dynamic> request;
    var token = await CommonService.getPrefs(JWT_TOKEN, false);
    request = {"id": appoID};

    dynamic localRes;

    await BaseAppClient.post(CANCEL_APPOINTMENT, context,
        isJWT: true, authToken: token, onSuccess: (response, statusCode) async {
      localRes = response;
    }, onFailure: (String error, int statusCode) {
      throw error;
    }, body: request);
    return Future.value(localRes);
  }

  Future<Map<String, dynamic>> addFavouriteDoctor(
      int docID, BuildContext context) async {
    Map<String, dynamic> request;
    var token = await CommonService.getPrefs(JWT_TOKEN, false);
    request = {"id": docID};

    dynamic localRes;

    await BaseAppClient.post(ADD_FAVOURITE_DOCTOR, context,
        isJWT: true, authToken: token, onSuccess: (response, statusCode) async {
      localRes = response;
    }, onFailure: (String error, int statusCode) {
      throw error;
    }, body: request);
    return Future.value(localRes);
  }

  Future<Map<String, dynamic>> getDoctorReviews(
      int docID, BuildContext context) async {
    Map<String, dynamic> request;
    var token = await CommonService.getPrefs(JWT_TOKEN, false);
    request = {"id": docID, "type": "doctors"};

    dynamic localRes;

    print(request);

    await BaseAppClient.post(GET_DOCTOR_REVIEWS, context,
        isJWT: false,
        authToken: token, onSuccess: (response, statusCode) async {
      localRes = response;
    }, onFailure: (String error, int statusCode) {
      throw error;
    }, body: request);
    return Future.value(localRes);
  }

  Future<Map<String, dynamic>> getFavouriteDoctors(BuildContext context) async {
    var token = await CommonService.getPrefs(JWT_TOKEN, false);
    dynamic localRes;

    await BaseAppClient.get(GET_FAVOURITE_DOCTOR, context,
        isJWT: true,
        isAuth: true,
        authToken: token, onSuccess: (response, statusCode) async {
      localRes = response;
    }, onFailure: (String error, int statusCode) {
      throw error;
    });
    return Future.value(localRes);
  }

  Future<Map<String, dynamic>> refreshUserToken(BuildContext context) async {
    var token = await CommonService.getPrefs(JWT_TOKEN, false);
    dynamic localRes;

    await BaseAppClient.get(REFRESH_TOKEN, context,
        isJWT: true,
        isAuth: true,
        authToken: token, onSuccess: (response, statusCode) async {
      localRes = response;
    }, onFailure: (String error, int statusCode) {
      throw error;
    });
    return Future.value(localRes);
  }

  Future<Map<String, dynamic>> removeFavouriteDoctor(
      int docID, BuildContext context) async {
    Map<String, dynamic> request;
    var token = await CommonService.getPrefs(JWT_TOKEN, false);
    request = {"id": docID};

    dynamic localRes;

    await BaseAppClient.post(REMOVE_FAVOURITE_DOCTOR, context,
        isJWT: true, authToken: token, onSuccess: (response, statusCode) async {
      localRes = response;
    }, onFailure: (String error, int statusCode) {
      throw error;
    }, body: request);
    return Future.value(localRes);
  }

  Future<Map<String, dynamic>> sendOTPOnEmail(
      String email, BuildContext context) async {
    Map<String, dynamic> request;
    var token = await CommonService.getPrefs(JWT_TOKEN, false);
    request = {"email": email};

    dynamic localRes;

    await BaseAppClient.post(SEND_OTP_CODE, context,
        isJWT: false,
        authToken: token, onSuccess: (response, statusCode) async {
      localRes = response;
    }, onFailure: (String error, int statusCode) {
      throw error;
    }, body: request);
    return Future.value(localRes);
  }

  Future<Map<String, dynamic>> verifyOTPOnEmail(
      String email, String code, BuildContext context) async {
    Map<String, dynamic> request;
    var token = await CommonService.getPrefs(JWT_TOKEN, false);
    request = {"email": email, "code": code};

    dynamic localRes;

    await BaseAppClient.post(VERIFY_OTP_CODE, context,
        isJWT: false,
        authToken: token, onSuccess: (response, statusCode) async {
      localRes = response;
    }, onFailure: (String error, int statusCode) {
      throw error;
    }, body: request);
    return Future.value(localRes);
  }

  Future<Map<String, dynamic>> updatePassword(
      String email, String password, BuildContext context) async {
    Map<String, dynamic> request;
    var token = await CommonService.getPrefs(JWT_TOKEN, false);
    request = {"email": email, "password": password};

    dynamic localRes;

    await BaseAppClient.post(RESET_PASSWORD, context,
        isJWT: false,
        authToken: token, onSuccess: (response, statusCode) async {
          localRes = response;
        }, onFailure: (String error, int statusCode) {
          throw error;
        }, body: request);
    return Future.value(localRes);
  }

}
