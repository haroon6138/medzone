package io.flutter.plugins.firebasemessaging;

import android.content.Intent;
import java.util.concurrent.TimeUnit;
import com.google.firebase.messaging.RemoteMessage;

// old lines by @Haroon

//public class CustomFlutterFirebaseMessagingService extends FlutterFirebaseMessagingService {
//    @Override
//    public void onMessageReceived(RemoteMessage remoteMessage) {
//        if (remoteMessage.getData().containsKey("is_call")) {
//            Intent intent = getPackageManager().getLaunchIntentForPackage(getPackageName());
//            startActivity(intent);
//            super.onMessageReceived(remoteMessage);
//        } else
//            super.onMessageReceived(remoteMessage);
//    }
//}

public class CustomFlutterFirebaseMessagingService extends FlutterFirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if (remoteMessage.getData().containsKey("is_call")) {
            Intent intent = getPackageManager().getLaunchIntentForPackage(getPackageName());
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT );
            startActivity(intent);
            try {
                TimeUnit.SECONDS.sleep(5);
            }catch (Exception e){
            }
            super.onMessageReceived(remoteMessage);
        } else
            super.onMessageReceived(remoteMessage);
    }
}